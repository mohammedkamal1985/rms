import { Item } from './../Models/Item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemsService } from '../Services/items-service.service';
import { OrderLine } from '../Models/OrderLine';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.css']
})

export class ItemSearchComponent implements OnInit {

  @Input('disabled') disabled: boolean;
  orderline: OrderLine;
  @Output() orderLineCreated = new EventEmitter();
  searchItemsResult: Array<Item>;
  opennedModal: any;

  constructor(private itemService: ItemsService, private modalService: NgbModal) {
    this.orderline = new OrderLine();
  }

  selectItemByName(name, content) {
    this.searchItemsResult = [];
    this.itemService.getItemByName(name)
      .subscribe(result => {
        this.searchItemsResult = result;
        this.opennedModal = this.modalService.open(content, { centered: true });
      });
  }

  selectItemFromSearchResult(item) {
    this.orderline.item = item;
    this.orderline.unit = item.defaultUnit;
    this.orderline.quantity = 1;
    this.opennedModal.dismiss();
  }

  addOrderLine() {
    this.orderLineCreated.emit(this.orderline);
    this.orderline = new OrderLine();
  }

  selectedUnit(event) {
  }

  ngOnInit() {
  }

}

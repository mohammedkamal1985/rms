import { Unit } from './Unit';

export class Item {
    constructor() {
        this.name = '';
    }
    code: string;
    name: string;
    availableUnits: Array<Unit>;
    defaultUnit: Unit;
}

export class CustomerAddress {
    blockNumber: string;
    street: string;
    region: string;
    state: string;
    country: string;
    landmark: string;
}

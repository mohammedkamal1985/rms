import { OrderType } from './Enums';
import { OrderBase } from './OrderBase';
import { Customer } from './Customer';
import { PurchasingOrder } from './PurchasingOrder';
import { CustomerAddress } from './CustomerAddress';

export class DeliveryOrder extends PurchasingOrder {

    deliveryAddress: CustomerAddress;
    deliveryDate: Date;

    constructor(customer: Customer, salesmanId: string, warehousId: string, machineName: string) {
        super(customer, salesmanId, warehousId, machineName);
        this.type = OrderType.Delivery;
    }
}

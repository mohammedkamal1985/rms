export enum OrderType {
    Purchasing= 0,
    Delivery= 1,
    Downpayment= 2,
    online= 3
}

export enum OrderStatus {
    New= 0,
    Submitted= 1,
    Canceled= 2,
    Hold= 3,
    OnWay= 4,
    Delivered= 5,
    Closed= 6
}

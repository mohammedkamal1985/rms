import { Item } from './Item';
import { Unit } from './Unit';

export class OrderLine {

    constructor() {
       this.item = new Item();
        this.quantity = 0;
        this.unitPrice = 0;
        this.unit = new Unit();
        this.total = 0;
    }

    item: Item;
    quantity: number;
    unit: Unit;
    unitPrice: number;
    total: number ;
}

import { CustomerAddress } from './CustomerAddress';

export class Customer {

    constructor() {
        this.addresses = [];
    }
    id: string;
    name: string;
    phoneNumber: string;
    addresses: Array<CustomerAddress>;
    isDefault: boolean;
}

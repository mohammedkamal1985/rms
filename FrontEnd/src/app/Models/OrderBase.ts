import { OrderLine } from './OrderLine';
import { OrderType, OrderStatus } from './Enums';
import { Customer } from './Customer';

export class OrderBase {
    id: string;
    serial: string;
    creationDate: Date;
    closeDate: Date;
    customer: Customer;
    type: OrderType;
    status: OrderStatus;
    total: number;
    net: number;
    paid: number;
    change: number;
    notes: string;
    orderLines: Array<OrderLine>;

    constructor(customer: Customer) {
        this.customer = customer;
        this.status = OrderStatus.New;
        this.total = 0;
        this.net = 0;
        this.paid = 0;
        this.change = 0;
        this.notes = '';
        this.orderLines = new Array<OrderLine>();
    }
}

import { OrderType } from './Enums';
import { OrderBase } from './OrderBase';
import { Customer } from './Customer';

export class PurchasingOrder extends OrderBase {

    salesmanId: string;
    machineName: string;
    warehouseId: string;

    constructor(customer: Customer, salesmanId: string, warehousId: string, machineName: string) {
        super(customer);
        this.type = OrderType.Purchasing;
        this.machineName = machineName;
        this.warehouseId = warehousId;
        this.salesmanId = salesmanId;
    }
}

import { CustomerAddress } from './../Models/CustomerAddress';
import { Customer } from './../Models/Customer';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { OrderCustomerservice } from '../Services/order-customer.service';
import { Component, OnInit, Output, EventEmitter, Input, forwardRef } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '../../../node_modules/@angular/forms';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => OrderCustomerComponent),
  multi: true
};

@Component({
  selector: 'app-order-customer',
  templateUrl: './order-customer.component.html',
  styleUrls: ['./order-customer.component.css'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})

export class OrderCustomerComponent implements OnInit, ControlValueAccessor {
  searchData: string;
  usePhoneInSearch: boolean;
  newOrderCustomer: Customer;
  newAddress: CustomerAddress;
  selectedOrderCustomer: Customer;
  searchCustomerResult: Array<Customer>;
  opennedModal: any;
  @Input('isOrderInProgress') isOrderInProgress: boolean;

  get SelectedOrderCustomer() {
    return this.selectedOrderCustomer;
  }
  set SelectedOrderCustomer(customer: Customer) {
    console.log('Customer property setter value:' + customer);
    if (customer !== this.SelectedOrderCustomer) {
      this.selectedOrderCustomer = customer;
      this.onChangeCallback(customer);
    }
  }

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  constructor(private orderCustomerService: OrderCustomerservice, private modalService: NgbModal) {
    this.usePhoneInSearch = false;
  }

  openNewCustomerModal(content) {
    this.newOrderCustomer = new Customer();
    this.newAddress = new CustomerAddress();
    this.opennedModal = this.modalService.open(content, { centered: true });
  }

  findCustomer(content) {
    this.searchCustomerResult = [];
    // Call the server to filter the customers
    if (this.usePhoneInSearch) {
      this.orderCustomerService.getCustomerByPhone(this.searchData)
        .subscribe(result => {
          if (result != null) {
            this.selectCustomerFromSearchResult(result);
          } else {
            this.opennedModal = this.modalService.open(content, { centered: true });
          }
        });
    } else {
      // FIND BY NAME
      this.orderCustomerService.getCustomersByName(this.searchData)
        .subscribe(result => {
          this.searchCustomerResult = result;
          this.opennedModal = this.modalService.open(content, { centered: true });
        });
    }
  }

  selectCustomerFromSearchResult(customer) {
    // set the selected customer
    this.SelectedOrderCustomer = customer;
    // close the Modal
    if (this.opennedModal) {
      this.opennedModal.dismiss();
    }
    this.searchData = '';
  }

  getDefaultCustomer() {
    this.orderCustomerService.getDefaultCustomer()
      .subscribe(result => {
        if (result != null) {
          this.selectCustomerFromSearchResult(result);
        }
      });
    this.searchData = '';
  }

  SaveNewCustomer() {
    // Empty the newOrderCustomer Address List
    this.newOrderCustomer.addresses = [];
    // Add the newAddress to the newOrderCustomer Address List
    this.newOrderCustomer.addresses.push(this.newAddress);
    // Call the service to save the new customer
    this.orderCustomerService.addNewCustomer(this.newOrderCustomer);
    // set the new customer as the selected customer directly
    this.SelectedOrderCustomer = this.newOrderCustomer;
    this.opennedModal.dismiss();
  }

  changeSelectedustomer(content) {
    if (this.isOrderInProgress) {
      this.opennedModal = this.modalService.open(content, { centered: true });
    } else {
      this.resetSelectedCustomer();
    }
  }

  resetSelectedCustomer() {
    this.SelectedOrderCustomer = undefined;
    if (this.opennedModal) {
      this.opennedModal.dismiss();
    }
  }

  ngOnInit() {
  }

  writeValue(obj: any): void {
    if (obj !== this.selectedOrderCustomer) {
      this.selectedOrderCustomer = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }
}

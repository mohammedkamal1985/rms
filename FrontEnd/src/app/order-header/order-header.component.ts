import { OrderStatus, OrderType } from './../Models/Enums';
import { OrderCustomerservice } from './../Services/order-customer.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomerAddress } from './../Models/CustomerAddress';
import { PurchasingOrder } from './../Models/PurchasingOrder';
import { DeliveryOrder } from './../Models/DeliveryOrder';
import { Customer } from './../Models/Customer';
import { OrderServic } from '../Services/order.service';

@Component({
  selector: 'app-order-header',
  templateUrl: './order-header.component.html',
  styleUrls: ['./order-header.component.css']
})
export class OrderHeaderComponent implements OnInit {

  @Output() orderCreated = new EventEmitter();
  @Output() orderCanceled = new EventEmitter();
  @Output() orderHold = new EventEmitter();
  @Output() orderClosed = new EventEmitter();
  @Input('orderCustomer') orderCustomer: Customer;
  newDeliveryAddress: CustomerAddress;
  opennedModal: any;
  canBeOnHold: boolean;
  @Input('order') order: PurchasingOrder;

  constructor(private orderService: OrderServic, private customerService: OrderCustomerservice, private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  initializeOrder(orderType: OrderType) {
    this.orderService.createNewOrder(orderType, this.orderCustomer,
      '0bf1df29-9a17-4a92-822d-d474f8756641',
      '1cc0bb1b-318e-4160-993a-31a875a9af0a',
      'eeea98d5-05ad-4811-bae8-3464a1febde8')
      .subscribe(result => {
        this.order = result;
        console.log(this.order);
        this.order.type = orderType;
        this.orderCreated.emit(this.order);
        if (orderType === OrderType.Purchasing || orderType === OrderType.Downpayment) {
          this.canBeOnHold = true;
        }
      });
  }

  saveNewAddress() {
    this.orderCustomer = this.customerService.addNewAddress(this.newDeliveryAddress, this.orderCustomer);
    this.opennedModal.dismiss();
  }

  openNewAddressModal(content) {
    this.newDeliveryAddress = new CustomerAddress();
    this.newDeliveryAddress.blockNumber = '10';
    this.opennedModal = this.modalService.open(content, { centered: true });
  }

  cancelOrder() {
    this.order.status = OrderStatus.Canceled;
    if (this.order.type === OrderType.Purchasing) {
      this.orderService.CancelPurchasingOrder(this.order)
        .subscribe(result => {
          if (result === true) {
            this.order = undefined;
            this.orderCanceled.emit();
          }
        });
    }
  }

  onNoteChanged(notes: string) {
    this.order.notes = notes;
  }

  holdOrder() {
    this.order.status = OrderStatus.Hold;
    if (this.order.type === OrderType.Purchasing) {
      this.orderService.HoldPurchasingOrder(this.order)
        .subscribe(result => {
          if (result === true) {
            this.order = undefined;
            this.orderHold.emit();
          }
        });
    }
  }
}

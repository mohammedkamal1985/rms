import { OrderLine } from './../Models/OrderLine';
import { OrderBase } from './../Models/OrderBase';
import { Customer } from './../Models/Customer';
import { Component, OnInit } from '@angular/core';
import { OrderServic } from '../Services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderCustomer: Customer;
  order: OrderBase;

  constructor(private orderService: OrderServic) { }

  ngOnInit() {
  }

  onOrderCustomerSelected(customer) {
    if (!customer) {
      this.order = undefined;
    }
    this.orderCustomer = customer;
  }

  onOrderCreated(order) {
    this.order = order;
  }

  onOrderCanceled() {
    this.order = undefined;
    this.orderCustomer = undefined;
  }

  onOrderHold() {
    this.order = undefined;
    this.orderCustomer = undefined;
  }

  onOrderLineCreated(orderLine) {
    if (!orderLine.item.name) {

      this.orderService.AddOrderLineByCode(orderLine.item.code, this.order.id).subscribe(result => {
        this.order = result as OrderBase;
      });
    } else {
      this.orderService.AddOrderLine(orderLine, this.order.id).subscribe(result => {
        this.order =  result as OrderBase;
      });
    }
  }
}

import { ItemsService } from './Services/items-service.service';
import { OrderServic } from './Services/order.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { OrderCustomerComponent } from './order-customer/order-customer.component';
import { OrderCustomerservice } from './Services/order-customer.service';
import { FormsModule } from '@angular/forms';
import { OrderComponent } from './order/order.component';
import { OrderHeaderComponent } from './order-header/order-header.component';
import { OrderLineComponent } from './order-line/order-line.component';
import { ItemSearchComponent } from './item-search/item-search.component';


@NgModule({
  declarations: [
    AppComponent,
    OrderCustomerComponent,
    OrderComponent,
    OrderHeaderComponent,
    OrderLineComponent,
    ItemSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [
    OrderCustomerservice,
    OrderServic,
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Item } from '../Models/Item';
import { Unit } from '../Models/Unit';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Constants } from '../constant';

@Injectable()
export class ItemsService {

  getUrl: string = Constants.applicationUrl + '/item';

  constructor(private http: HttpClient) {

  }

  getItemByName(itemName: string) {
    return this.http.get<Item[]>(this.getUrl + '/Name/' + itemName);
  }

}

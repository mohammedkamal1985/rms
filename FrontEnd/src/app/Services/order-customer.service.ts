import { CustomerAddress } from './../Models/CustomerAddress';
import { Customer } from './../Models/Customer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Constants } from '../constant';

@Injectable()
export class OrderCustomerservice {

    getUrl: string = Constants.applicationUrl + '/customer';
    private customerList: Array<Customer>;

    constructor(private http: HttpClient) {
        }

    getCustomersByName(name: string): Observable<Customer[]> {
        return this.http.get<Customer[]>(this.getUrl + '/Name/' + name);
    }

    getCustomerByPhone(phone: string) {
        return this.http.get<Customer[]>(this.getUrl + '/Phone/' + phone);
    }

    addNewCustomer(newCustomer: Customer) {
        this.customerList.push(newCustomer);
    }

    getDefaultCustomer() {
        return this.http.get(this.getUrl + '/Default');
    }

    addNewAddress(address: CustomerAddress, customer: Customer) {
        if (!customer.addresses) {
            customer.addresses = new Array<CustomerAddress>();
        }
        customer.addresses.push(address);
        return customer;
    }
}

import { OrderLine } from './../Models/OrderLine';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from '../Models/Customer';
import { OrderType } from '../Models/Enums';
import { DeliveryOrder } from '../Models/DeliveryOrder';
import { PurchasingOrder } from '../Models/PurchasingOrder';
import { Constants } from '../constant';

@Injectable()
export class OrderServic {

  private orderApiUrl: string = Constants.applicationUrl + '/Order';

  constructor(private http: HttpClient) {

  }

  createNewOrder(orderType: OrderType, orderCustomer: Customer, salesmanId: string, warehouseId: string, machineName: string) {
    let order: any;
    if (orderType === OrderType.Delivery) {
      order = new DeliveryOrder(orderCustomer, salesmanId, warehouseId, machineName);
      order.serial = 
      return this.http.get<string>(this.orderApiUrl, order);
    } else if (orderType === OrderType.Purchasing) {
      order = new PurchasingOrder(orderCustomer, salesmanId, warehouseId, machineName);
      return this.http.post<PurchasingOrder>(this.orderApiUrl, order);
    }
  }

  AddOrderLineByCode(itemCode: string, orderId: string) {
    return this.http.post(this.orderApiUrl + '/' + orderId + '/Line/Code/' + itemCode, null);
  }

  AddOrderLine(line: OrderLine, orderId: string) {
    return this.http.post(this.orderApiUrl + '/' + orderId + '/Line', line);
  }

  CancelPurchasingOrder(order: PurchasingOrder) {
    return this.http.put(this.orderApiUrl + '/Purchasing/Cancel', order);
  }

  HoldPurchasingOrder(order: PurchasingOrder) {
    return this.http.put(this.orderApiUrl + '/Purchasing/Hold', order);
  }
}

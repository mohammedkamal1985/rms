﻿using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Customer.Repository.Infrastructure.DataModel
{
    public class AddressDataModel: DBEntityBase
    {
        public int BlockNo { get; set; }
        public string Street { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Landmark { get; set; }
    }
}

﻿using System;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Customer.Repository.Infrastructure.DataModel
{
    public class CustomerGroupDataModel: DBEntityBase<Guid>
    {
        public string Name { get; set; }
    }
}

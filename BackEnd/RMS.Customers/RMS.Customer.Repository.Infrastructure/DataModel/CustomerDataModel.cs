﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.Dapper.Entities;

namespace RMS.Customer.Repository.Infrastructure.DataModel
{
    public class CustomerDataModel:DBEntityBase<Guid>
    {
        public List<AddressDataModel> DeliveryAddresses { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public CustomerGroupDataModel Group { get; set; }

        public bool IsDefault { get; set; }
    }
}

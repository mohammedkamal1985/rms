﻿using Repository.Base.Interfaces;
using RMS.Customer.Repository.Infrastructure.DataModel;

namespace RMS.Customer.Repository.Infrastructure
{
    public interface ICustomerRepository: IWriteRepository<CustomerDataModel>,IQueryRepository//<CustomerDataModel,int>
    {
    }
}

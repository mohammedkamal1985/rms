﻿using System;
using System.Collections.Generic;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Customers.Core.Aggregates
{
    public class CustomerGroup : Entity<Guid>
    {
        public string Name { get;private set; }
        public string Code { get; private set; }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }
    }
}

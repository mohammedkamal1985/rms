﻿using System;
using System.Collections.Generic;
using RMS.Customers.Core.Aggregates;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Core.ValueTypes;

namespace RMS.Customers.Core
{
    public class Customer : AggregateRoot<Guid>
    {
        public List<Address> DeliveryAddresses { get; private set; }
        public List<Order> Orders { get; private set; }

        public string Name { get; private set; }
        public string PhoneNumber { get; set; }

        public DateTime Birthdate { get; private set; }


        public Customer()
        {

        }

        public Customer(List<Address> delieveryAddresses, Guid customerId, string name) : this(customerId,name)
        {
            DeliveryAddresses = delieveryAddresses;
            Name = name;
        }

        public Customer(Guid customerId, string name) : base()
        {
            Name = name;
            base.Id = customerId;
        }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }

        public bool AddAddress(Address newAddress)
        {
            this.DeliveryAddresses.Add(newAddress);
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Customers.Core.Aggregates
{
    public class Order : Entity<Guid>
    {
        public DateTime Date { get; private set; }
        public decimal Total { get; private set; }
        public decimal Discount { get; private set; }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }
    }
}

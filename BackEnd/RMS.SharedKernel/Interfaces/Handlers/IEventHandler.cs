﻿using RMS.SharedKernel.Core.Interfaces.Events;

namespace RMS.SharedKernel.Core.Interfaces.Handlers
{
    public interface IEventHandler<T> where T : IDomainEvent
    {
        void Handle(T domainEvent);
    }
}
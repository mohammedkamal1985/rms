﻿using RMS.SharedKernel.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.SharedKernel.Interfaces.Entities
{
    public interface IValidate
    {
        bool IsValid();
        
        void AddValidationError(string error);

        void AddValidationErrors(List<ValidationError> errors);
    }
}

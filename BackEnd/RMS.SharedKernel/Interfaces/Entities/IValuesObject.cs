﻿namespace RMS.SharedKernel.Interfaces
{
    public interface IValuesObject
    {
        bool IsValid();
    }
}
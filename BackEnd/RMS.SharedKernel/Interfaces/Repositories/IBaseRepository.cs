﻿using RMS.SharedKernel.Core.Interfaces;

namespace RMS.SharedKernel.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity :IEntity
    {
        void Add(TEntity newEntity);

        void GetAll();

        void Pageing(int pageSize, int pageIndex);

        void Delete(TEntity entity);
    }
}
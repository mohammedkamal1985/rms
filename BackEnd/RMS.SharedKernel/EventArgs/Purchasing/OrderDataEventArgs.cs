﻿using System.Collections.Generic;

namespace RMS.SharedKernel.Core.EventArgs.Purchasing
{
    public class OrderDataEventArgs
    {
        public int CustomerId;
        public int CashierId;
        public int SalesmanId;
        public decimal OrderTotal;
    }
}
﻿using System.Collections.Generic;
using RMS.SharedKernel.Core.Interfaces.Events;
using RMS.SharedKernel.Entities.Base;

namespace RMS.SharedKernel.Core.Entities.Base
{
    public abstract class AggregateRoot<T> :Entity<T> 
    {
        private readonly List<IDomainEvent> _domainEvents = new List<IDomainEvent>();
        public virtual IReadOnlyList<IDomainEvent> DomainEvents => _domainEvents;

        protected virtual void AddDomainEvent(IDomainEvent newEvent)
        {
            _domainEvents.Add(newEvent);
        }

        public virtual void ClearEvents()
        {
            _domainEvents.Clear();
        }
    }
}

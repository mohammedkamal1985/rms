﻿using System;
using System.Collections.Generic;
using RMS.SharedKernel.Core.Interfaces;
using RMS.SharedKernel.Interfaces.Entities;

namespace RMS.SharedKernel.Entities.Base
{
    public abstract class Entity<T> : IEntity, IValidate, IEquatable<Entity<T>>
    {
        public override int GetHashCode()
        {
            return EqualityComparer<T>.Default.GetHashCode(Id);
        }
        public T Id { get; protected set; }

        protected Entity(T id)
        {
            Id = id;
            ValidationErrors = new List<ValidationError>();
        }
        protected Entity()
        {
            ValidationErrors = new List<ValidationError>();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Entity<T>) obj);
        }
        public bool Equals(Entity<T> other)
        {
            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != other.GetType())
                return false;

            if (Id == null || other.Id == null)
                return false;

            return Id.Equals(other.Id);
        }
        public static bool operator ==(Entity<T> a, Entity<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }
        public static bool operator !=(Entity<T> a, Entity<T> b)
        {
            return !(a == b);
        }
        public abstract bool IsValid();
        public List<ValidationError> ValidationErrors { get; }

        public void AddValidationError(string error)
        {
            var validationError = new ValidationError(error);
            ValidationErrors.Add(validationError);
        }

        public void AddValidationErrors(List<ValidationError> errors)
        {
            ValidationErrors.AddRange(errors);
        }
    }
}
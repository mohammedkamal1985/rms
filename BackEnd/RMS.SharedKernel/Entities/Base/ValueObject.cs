﻿using System;
using System.Collections.Generic;
using System.Reflection;
using RMS.SharedKernel.Interfaces;
using RMS.SharedKernel.Interfaces.Entities;

namespace RMS.SharedKernel.Entities.Base
{
    public abstract class ValueObject:IValuesObject,IEquatable<ValueObject>,IValidate
    {
        public List<ValidationError> ValidationErrors { get ; protected set; }

        public void AddValidationError(string error)
        {
            ValidationErrors.Add(new ValidationError(error));
        }

        public void AddValidationErrors(List<ValidationError> errors)
        {
            ValidationErrors.AddRange(errors);
        }

        public bool Equals(ValueObject other)
        {
            if (other == null)
                return false;

            
            if (GetType() != other.GetType())
                return false;

            FieldInfo[] fields = other.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            foreach (FieldInfo field in fields)
            {
                object value1 = field.GetValue(other);
                object value2 = field.GetValue(this);

                if (value1 == null)
                {
                    if (value2 != null)
                        return false;
                }
                else if (!value1.Equals(value2))
                    return false;
            }

            return true;
        }

        public abstract bool IsValid();
    }
}
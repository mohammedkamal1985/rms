﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.SharedKernel.Entities.Base
{
    public class ValidationError
    {
        public ValidationError(string message)
        {
            ValidationErrorMessage = message;
        }

        public string ValidationErrorMessage { get; }
    }
}

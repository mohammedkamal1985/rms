﻿namespace RMS.SharedKernel.Core.Entities
{
    public class ApplicationServiceReturnObject<T>
    {
        public T Result;
        public string Message;
    }
}
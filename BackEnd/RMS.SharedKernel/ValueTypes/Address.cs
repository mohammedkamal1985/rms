﻿using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.SharedKernel.Core.ValueTypes
{
    public class Address : ValueObject
    {
        public Address(int blockNo, string street, string city,
            string state, string country, string landmark, string postCode)
        {
            BlockNo = blockNo;
            Street = street;
            City = city;
            State = state;
            Country = country;
            Landmark = landmark;
            PostCode = postCode;
        }

        public int BlockNo { get; }
        public string Street { get; }
        public string City { get; }
        public string State { get; }
        public string Country { get; }
        public string Landmark { get; }
        public string PostCode { get; }

        public override bool IsValid()
        {
            var isValid = true;
            if (string.IsNullOrEmpty(Street))
            {
                isValid = false;
                AddValidationError("Street is required.");
            }

            if (string.IsNullOrEmpty(City))
            {
                isValid = false;
                AddValidationError("Region is required.");
            }

            if (string.IsNullOrEmpty(State))
            {
                isValid = false;
                AddValidationError("State is required.");
            }

            if (string.IsNullOrEmpty(Country))
            {
                isValid = false;
                AddValidationError("Country is required.");
            }

            if (string.IsNullOrEmpty(Landmark))
            {
                isValid = false;
                AddValidationError("Landmark is required.");
            }

            return isValid;
        }
    }
}
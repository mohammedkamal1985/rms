﻿using System;
using System.Linq;
using ExceptionHandling.CustomException;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Core.ValueTypes.StockTransaction;
using RMS.Purchasing.DomainService.Infrastructure;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService
{
    public class StockManager : IStockManager
    {
        private readonly IPurchasingUnitOfWork _unitOfWork;
        private readonly IMappingService _mappingService;
        private readonly ILogger _appLogger;
        private readonly IPurchasingDomainService _domainService;

        public StockManager(IPurchasingUnitOfWork unitOfWork, IMappingService mappingService
            , ILogger appLogger, IPurchasingDomainService domainService)
        {
            _unitOfWork = unitOfWork;
            _mappingService = mappingService;
            _appLogger = appLogger;
            _domainService = domainService;
        }

        public ServiceReadResponse<StockModel> GetStockByItemCode(ServiceRequest<string> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    //TODO log business exception and construct user frindly message
                    throw new BusinessException();
                }

                var stockDataModel = _unitOfWork.StockQueryRepository.GetByItemCode(request.Data);

                if (stockDataModel == null)
                {
                    return new ServiceReadResponse<StockModel>(null);
                }

                var domainStock = _mappingService.Map<StockDataModel, Stock>(stockDataModel);
                var result = _mappingService.Map<Stock, StockModel>(domainStock);
                return new ServiceReadResponse<StockModel>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceWriteResponse<bool> ReserveStock(ServiceRequest<StockTransactionModel> request)
        {
            try
            {
                if (request?.Data == null)
                {
                    //TODO log business exception and construct user friendly message
                    throw new BusinessException();
                }

                var domainTransaction =
                    _mappingService.Map<StockTransactionModel, WithdrawStockTransaction>(request.Data);
                var stockDataModel = _unitOfWork.StockQueryRepository.GetByItemCode(request.Data.Item.Code);
                var domainStock = _mappingService.Map<StockDataModel, Stock>(stockDataModel);

                return ReserveStock(stockDataModel, domainStock, domainTransaction.Quantity.Value,
                    domainTransaction.Quantity.Unit);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceWriteResponse<bool> ReserveStock(ServiceRequest<string> request)
        {
            try
            {
                var response = new ServiceWriteResponse<bool>();
                if (request == null || string.IsNullOrEmpty(request.Data))
                {
                    //TODO log business exception and construct user frindly message
                    throw new BusinessException();
                }

                var stockDataModel = _unitOfWork.StockQueryRepository.GetByItemCode(request.Data);
                var domainStock = _mappingService.Map<StockDataModel, Stock>(stockDataModel);
                return ReserveStock(stockDataModel, domainStock, 1, domainStock.Item.DefaultUnit);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        private ServiceWriteResponse<bool> ReserveStock(StockDataModel stockDataModel, Stock domainStock,
            decimal quantity, Unit transactionUnit)
        {
            var response = new ServiceWriteResponse<bool>();
            var transactionQuantityInStockUnit =
                _domainService.ConvertQuantityToItemDefaultUnitQuantity(quantity, transactionUnit, domainStock.Item);

            if (domainStock.Reserve(transactionQuantityInStockUnit) != null)
            {
                response.Result = false;
                response.ValidationErrors.AddRange(domainStock.ValidationErrors.Select(s => s.ValidationErrorMessage));
                return response;
            }

            stockDataModel.Quantity = domainStock.Quantity;
            stockDataModel.Reserved = domainStock.Reserved;
            stockDataModel.Available = domainStock.Available;

            _unitOfWork.StockWriteRepository.Update(stockDataModel);
            _unitOfWork.SaveChanges();
            _unitOfWork.CloseConnection();

            response.Result = true;
            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using ExceptionHandling.CustomException;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService
{
    public class CustomerManager : ICustomerManager
    {
        private readonly IPurchasingUnitOfWork _unitOfWork;
        private readonly IMappingService _mappingService;
        private ILogger _appLogger;

        public CustomerManager(IPurchasingUnitOfWork unitOfWork,
            IMappingService mappingService,
            ILogger appLogger)
        {
            _unitOfWork = unitOfWork;
            _mappingService = mappingService;
            _appLogger = appLogger;
        }

        public ServiceReadResponse<List<CustomerModel>> GetByName(ServiceRequest<string> request)
        {
            try
            {
                if (request?.Data == null)
                {
                    throw new BusinessException();
                }

                if (string.IsNullOrEmpty(request.Data))
                {
                    return new ServiceReadResponse<List<CustomerModel>>(null);
                }

                _unitOfWork.OpenConnection();
                var customers = _unitOfWork.CustomerRepository.GetByName(request.Data);
                _unitOfWork.CloseConnection();

                if (customers == null)
                {
                    return new ServiceReadResponse<List<CustomerModel>>(null);
                }

                var result = _mappingService.Map<List<Customer>, List<CustomerModel>>(
                    _mappingService.Map<List<CustomerDataModel>, List<Customer>>(customers));
                return new ServiceReadResponse<List<CustomerModel>>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceReadResponse<CustomerModel> GetByPhoneNumber(ServiceRequest<string> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    throw new BusinessException();
                }

                if (string.IsNullOrEmpty(request.Data))
                {
                    return new ServiceReadResponse<CustomerModel>(null);
                }

                _unitOfWork.OpenConnection();
                var customer = _unitOfWork.CustomerRepository.GetByPhoneNumber(request.Data);
                _unitOfWork.CloseConnection();

                if (customer == null)
                {
                    return new ServiceReadResponse<CustomerModel>(null);
                }

                var result = _mappingService.Map<Customer, CustomerModel>(
                    _mappingService.Map<CustomerDataModel, Customer>(customer));
                return new ServiceReadResponse<CustomerModel>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceReadResponse<CustomerModel> GetDefaultCustomer()
        {
            try
            {
                _unitOfWork.OpenConnection();
                var customer = _unitOfWork.CustomerRepository.GetDefaultCustomer();
                _unitOfWork.CloseConnection();

                var result = _mappingService.Map<Customer, CustomerModel>(
                    _mappingService.Map<CustomerDataModel, Customer>(customer));
                return new ServiceReadResponse<CustomerModel>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }
    }
}
﻿using AutoMapper;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Mapping.MappingProfiles;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService.Mapping
{
    public class MappingService : IMappingService
    {
        static MapperConfiguration _configuration;
        private readonly IMapper _mapper;


        public MappingService()
        {
            if (_configuration == null)
            {
                _configuration = new MapperConfiguration(cfg =>
                {
                    cfg.ShouldMapProperty = p => p.SetMethod!=null && ( p.SetMethod.IsPublic || p.SetMethod.IsFamily || p.SetMethod.IsPrivate );
                    cfg.AddProfile<CustomerMappingProfile>();
                    cfg.AddProfile<ItemMappingProfile>();
                    cfg.AddProfile<OrderMappingProfile>();
                    /*cfg.AddProfile<StockMappingProfile>();*/
                });
            }
            _mapper = _configuration.CreateMapper();
        }

        public Destination Map<Source, Destination>(Source sourceObject)
        {
            var des =_mapper.Map<Source, Destination>(sourceObject);
            return des;
        }
    }
}

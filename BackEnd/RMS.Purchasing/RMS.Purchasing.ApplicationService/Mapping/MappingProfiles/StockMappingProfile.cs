﻿using AutoMapper;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Core.ValueTypes.StockTransaction;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService.Mapping.MappingProfiles
{
    public class StockMappingProfile : Profile
    {
        public StockMappingProfile()
        {
            //, Domian to Data Model
            CreateMap<Stock, StockDataModel>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<WithdrawStockTransaction, StockTransactionDataModel>()
                .ForMember(opts => opts.DatabaseId, src => src.Ignore())
                .ForMember(opts => opts.Type, opts => opts.MapFrom(s => (int)TransactionType.Withdraw))
                .ForMember(opt => opt.Quantity, opts => opts.MapFrom(s => s.Quantity.Value))
                .ForMember(opt => opt.Unit, opts => opts.MapFrom(s => s.Quantity.Unit))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //, Data Model to Domian
            CreateMap<StockDataModel, Stock>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<StockTransactionDataModel, WithdrawStockTransaction>()
                .ForMember(opt => opt.Quantity, opts => opts.ResolveUsing((src, des, i, context) =>
                {
                    return new Quantity(context.Mapper.Map<UnitDataModel, Unit>(src.Unit), src.Quantity);
                }))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //, Domian to Model
            CreateMap<Stock, StockModel>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<WithdrawStockTransaction, StockTransactionModel>()
                .ForMember(opt => opt.Quantity, opts => opts.MapFrom(s => s.Quantity.Value))
                .ForMember(opt => opt.Unit, opts => opts.MapFrom(s => s.Quantity.Unit))
                .ForMember(opts => opts.Type, src => src.MapFrom(s => (int)TransactionType.Withdraw))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //, Data Model to Domian
            CreateMap<StockModel, Stock>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<StockTransactionModel, WithdrawStockTransaction>()
                .ForMember(opt => opt.Quantity, opts => opts.ResolveUsing((src, des, i, context) =>
                {
                    return new Quantity(context.Mapper.Map<UnitModel, Unit>(src.Unit), src.Quantity);
                }))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}

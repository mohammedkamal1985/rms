﻿using AutoMapper;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Entities;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Core.ValueTypes.Payment;
using RMS.Purchasing.Core.Enums;
using System.Collections.Generic;

namespace RMS.Purchasing.ApplicationService.Mapping.MappingProfiles
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            #region PAYMENT CLASS MAPPING

            // Domain Entity ==> Business Model
            CreateMap<CashPayment, PaymentModel>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.Ignore())
                .ForMember(d => d.AccountType, opt => opt.Ignore())
                .ForMember(d => d.Type, opt => opt.MapFrom(s => (int) PaymentTypes.Cash));
            CreateMap<CreditCardPayment, PaymentModel>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.MapFrom(s => s.AccountId))
                .ForMember(d => d.AccountType, opt => opt.MapFrom(s => (int) s.AccountType))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => (int) PaymentTypes.Cash));
            CreateMap<PaymentBase, PaymentModel>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.Ignore())
                .ForMember(d => d.AccountType, opt => opt.Ignore())
                .ForMember(d => d.Type, opt => opt.MapFrom(s => (int) s.Type));

            //Business Model ==> Domain Entity
            CreateMap<PaymentModel, CashPayment>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));

            CreateMap<PaymentModel, CreditCardPayment>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.MapFrom(s => s.AccountId))
                .ForMember(d => d.AccountType, opt => opt.MapFrom(s => (CreditCardTypes) s.AccountType));

            //DataModel Model ==>  Domain Entity
            CreateMap<OrderPaymentDataModel, CashPayment>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));
            CreateMap<OrderPaymentDataModel, CreditCardPayment>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.MapFrom(s => s.AccountId))
                .ForMember(d => d.AccountType, opt => opt.MapFrom(s => (CreditCardTypes) s.AccountType));
            CreateMap<OrderPaymentDataModel, PaymentBase>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));


            //Domain Entity ==> DataModel Model 
            CreateMap<CashPayment, OrderPaymentDataModel>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderHeader.Id))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.Ignore())
                .ForMember(d => d.AccountType, opt => opt.Ignore())
                .ForMember(d => d.PaymentType, opt => opt.MapFrom(s => (int) PaymentTypes.Cash));
            CreateMap<CreditCardPayment, OrderPaymentDataModel>()
                //.ForMember(d => d.OrderId, opt => opt.MapFrom(s => s.OrderId))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
                .ForMember(d => d.AccountId, opt => opt.MapFrom(s => s.AccountId))
                .ForMember(d => d.AccountType, opt => opt.MapFrom(s => (int) s.AccountType))
                .ForMember(d => d.PaymentType, opt => opt.MapFrom(s => (int) PaymentTypes.Cash));

            #endregion

            #region UNIT CLASS MAPPING

            // Domain Entity ==> Business Model
            CreateMap<Unit, UnitModel>()
                .ForMember(des => des.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(des => des.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(des => des.Code, opt => opt.MapFrom(src => src.Code));

            //Business Model ==> Domain Entity
            CreateMap<UnitModel, Unit>()
                .ConstructUsing(src => new Unit(src.Id, src.Name, src.Code));

            //DataModel Model ==>  Domain Entity
            CreateMap<UnitDataModel, Unit>()
                .ConstructUsing(src => new Unit(src.Id, src.Name, src.Code));

            //Domain Entity ==> DataModel Model 
            CreateMap<Unit, UnitDataModel>()
                .ForMember(d => d.DatabaseId, opt => opt.Ignore())
                .ForMember(des => des.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(des => des.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(des => des.Code, opt => opt.MapFrom(src => src.Code));

            #endregion

            #region ORDER LINE CALSS MAPPING

            // Domain Entity ==> Business Model
            CreateMap<OrderLine, OrderLineModel>()
                .ForMember(d => d.Quantity, opt => opt.MapFrom(src => src.Quantity.Value))
                .ForMember(d => d.Unit, opt => opt.MapFrom(src => src.Quantity.Unit))
                .ForMember(d => d.Item, opt => opt.MapFrom(src => src.LineItem))
                .ForMember(d => d.UnitPrice, opt => opt.MapFrom(src => src.UnitPrice));

            //Business Model ==> Domain Entity
            CreateMap<OrderLineModel, OrderLine>()
                .ConstructUsing((src, context) => new OrderLine(context.Mapper.Map<ItemModel, Item>(src.Item),
                    new Quantity(context.Mapper.Map<UnitModel, Unit>(src.Unit), src.Quantity), src.UnitPrice, null));
            
            CreateMap<NewOrderLineModel, OrderLine>()
                .ConstructUsing((src, context) => new OrderLine(context.Mapper.Map<ItemModel, Item>(src.Item),
                    new Quantity(context.Mapper.Map<UnitModel, Unit>(src.Unit), src.Quantity), 0, null));


            //DataModel Model ==>  Domain Entity
            CreateMap<OrderLineDataModel, OrderLine>()
                
                .ForMember(d => d.Quantity,
                    opt =>
                        opt.ResolveUsing((src, des, i, context) =>
                            new Quantity(context.Mapper.Map<UnitDataModel, Unit>(src.Unit), src.Quantity)));

            //Domain Entity ==> DataModel Model 
            CreateMap<OrderLine, OrderLineDataModel>()
                .ForMember(d => d.DatabaseId, opt => opt.Ignore())
                .ForMember(d => d.Quantity, opt => opt.MapFrom(src => src.Quantity.Value))
                .ForMember(d => d.Unit, opt => opt.MapFrom(src => src.Quantity.Unit))
                .ForMember(d => d.LineItem, opt => opt.MapFrom(src => src.LineItem))
                .ForMember(d => d.UnitPrice, opt => opt.MapFrom(src => src.UnitPrice));

            #endregion

            /*
            #region ORDER CLASS MAPPING

            // Domain Entity ==> Business Model
            CreateMap<PurchasingOrder, PurchasingOrderModel>()
                .ForMember(s => s.Type, opt => opt.MapFrom(src => (int) src.OrderType))
                .ForMember(d => d.Payments, opt => opt.ResolveUsing((src, des, i, context) =>
                {
                    if (src.Payments == null)
                        return null;
                    var payments = new List<PaymentModel>();
                    foreach (var payment in src.Payments)
                    {
                        if (src.Payments == null)
                            return null;

                        if (payment.Type == PaymentTypes.Cash)
                        {
                            payments.Add(context.Mapper.Map<CashPayment, PaymentModel>((CashPayment) payment));
                        }
                        else if (payment.Type == PaymentTypes.CreditCard)
                        {
                            payments.Add(
                                context.Mapper.Map<CreditCardPayment, PaymentModel>((CreditCardPayment) payment));
                        }
                    }

                    return payments;
                }));
            CreateMap<DeliveryOrder, DeliveryOrderModel>();

            //Business Model ==> Domain Entity
            CreateMap<NewPurchasingOrderModel, PurchasingOrder>();
            CreateMap<PurchasingOrderModel, PurchasingOrder>()
                .ForMember(d => d.Payments, opt => opt.ResolveUsing((src, des, i, context) =>
                {
                    if (src.Payments == null)
                        return null;

                    var payments = new List<PaymentBase>();
                    foreach (var payment in src.Payments)
                    {
                        if (payment.Type == (int) PaymentTypes.Cash)
                        {
                            payments.Add(context.Mapper.Map<PaymentModel, CashPayment>(payment));
                        }
                        else if (payment.Type == (int) PaymentTypes.CreditCard)
                        {
                            payments.Add(context.Mapper.Map<PaymentModel, CreditCardPayment>(payment));
                        }
                    }

                    return payments;
                }));

            //DataModel Model ==>  Domain Entity
            CreateMap<OrderDataModel, PurchasingOrder>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.OrderLines, opt => opt.MapFrom(src => src.OrderLines));

            //Domain Entity ==> DataModel Model 
            CreateMap<PurchasingOrder, OrderDataModel>()
                .ForMember(d => d.DeliveryAddress, opt => opt.Ignore())
                .ForMember(d => d.DeliveryDate, opt => opt.Ignore())
                .ForMember(d => d.PayOnDelivery, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id));

            #endregion
            */
        }
    }
}
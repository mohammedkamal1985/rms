﻿using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.SharedKernel.Core.ValueTypes;

namespace RMS.Purchasing.ApplicationService.Mapping.MappingProfiles
{
    public class CustomerMappingProfile : Profile
    {
        public CustomerMappingProfile()
        {
            #region ADDRESS MAPPING

            //Domain to Business Model
            CreateMap<Address, CustomerAddressModel>()
                .ForMember(d => d.BlockNo, opt => opt.MapFrom(src => src.BlockNo))
                .ForMember(d => d.Street, opt => opt.MapFrom(src => src.Street))
                .ForMember(d => d.City, opt => opt.MapFrom(src => src.City))
                .ForMember(d => d.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(d => d.Landmark, opt => opt.MapFrom(src => src.Landmark))
                .ForMember(d => d.State, opt => opt.MapFrom(src => src.State))
                .ForMember(d => d.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ReverseMap();

            //Domain to Data Model 
            CreateMap<Address, AddressDataModel>()
                .ForMember(d => d.DatabaseId, opt => opt.Ignore())
                .ForMember(d => d.City, opt => opt.MapFrom(src => src.City))
                .ForMember(d => d.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(d => d.Landmark, opt => opt.MapFrom(src => src.Landmark))
                .ForMember(d => d.State, opt => opt.MapFrom(src => src.State))
                .ForMember(d => d.Street, opt => opt.MapFrom(src => src.Street))
                .ForMember(d => d.BlockNo, opt => opt.MapFrom(src => src.BlockNo))
                .ForMember(d => d.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ReverseMap();

            #endregion

            #region CUSTOMER GROUP MAPPING

            //domain to data model
            CreateMap<CustomerGroup, CustomerGroupDataModel>()
                .ForMember(d => d.DatabaseId, s => s.Ignore())
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ReverseMap();

            CreateMap<CustomerGroupDataModel, CustomerGroup>()
                .ConstructUsing(src => new CustomerGroup(src.Id, src.Code, src.Name));

            #endregion

            #region CUSTOMER MAPPING

            //domain to business
            CreateMap<Customer, CustomerModel>()
                .ForMember(d => d.Addresses, opt => opt.MapFrom(src => src.DeliveryAddresses.ToList()))
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(d => d.IsDefault, opt => opt.MapFrom(src => src.IsDefault));

            //business to domain
            CreateMap<CustomerModel, Customer>()
                .ConstructUsing(src =>
                {
                    var addresses = src.Addresses.Select(d =>
                        new Address(d.BlockNo, d.Street, d.City, d.State, d.Country, d.Landmark, d.PostCode));
                    var customer = new Customer(new ReadOnlyCollection<Address>(addresses.ToList()),
                        src.Id, src.Name, src.PhoneNumber, src.IsDefault, null);
                    return customer;
                });

            //data to domain
            CreateMap<CustomerDataModel, Customer>()
                .ConstructUsing(src =>
                {
                    var addresses = src.DeliveryAddresses.Select(d =>
                        new Address(d.BlockNo, d.Street, d.City, d.State, d.Country, d.Landmark, d.PostCode));
                    var customer = new Customer(new ReadOnlyCollection<Address>(addresses.ToList()),
                        src.Id, src.Name, src.PhoneNumber, src.IsDefault, null);
                    return customer;
                });

            //Domain to data
            CreateMap<Customer, CustomerDataModel>()
                .ForMember(d => d.IsDefault, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(d => d.Group, opt => opt.MapFrom(src => src.Group))
                .ForMember(d => d.DeliveryAddresses, opt => opt.MapFrom(src => src.DeliveryAddresses));

            #endregion
        }
    }
}
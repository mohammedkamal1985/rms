﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService.Mapping.MappingProfiles
{
    public class ItemMappingProfile : Profile
    {
        public ItemMappingProfile()
        {
            #region ITEM GROUP

            //Domain Entity ==> Data Model 
            CreateMap<ItemGroup, ItemGroupDataModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.DatabaseId, opt => opt.Ignore());

            //Data Model ==> Domain Entity
            CreateMap<ItemGroupDataModel, ItemGroup>()
                .ConstructUsing(src => new ItemGroup(src.Id, src.Name, src.Code));

            #endregion

            #region ITEM CATEGORY

            //Domain Entity ==> Data Model 
            CreateMap<ItemCategory, ItemCategoryDataModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.DatabaseId, opt => opt.Ignore());

            //Data Model ==> Domain Entity
            CreateMap<ItemCategoryDataModel, ItemCategory>()
                .ConstructUsing(src => new ItemCategory(src.Id, src.Name, src.Code));

            #endregion

            #region ITEM

            //Domain Entity ==> Business Model 
            CreateMap<Item, ItemModel>()
                .ForMember(d=>d.Code, opt=>opt.MapFrom(src=>src.Code))
                .ForMember(d=>d.Id, opt=>opt.MapFrom(src=>src.Id))
                .ForMember(d=>d.Name, opt=>opt.MapFrom(src=>src.Name))
                .ForMember(d=>d.DefaultPrice, opt=>opt.MapFrom(src=>src.DefaultPrice))
                .ForMember(d=>d.DefaultUnit, opt=>opt.MapFrom(src=>src.DefaultUnit))
                .ForMember(d => d.AvailableUnits,
                    opt => opt.ResolveUsing((src, des, i, context) => context.Mapper.Map<List<Unit>, List<UnitModel>>(src.AvailableUnits.Keys.ToList())));

            //Business Model ==> Domain Entity
            CreateMap<ItemModel, Item>()
                .ConstructUsing((src, context) =>
                {
                    var units = new Dictionary<Unit, decimal>();
                    foreach (var unitModel in src.AvailableUnits)
                    {
                        units.Add(context.Mapper.Map<UnitModel, Unit>(unitModel), 0);
                    }

                    return new Item(src.Id, src.Code, src.Name,
                        units, context.Mapper.Map<UnitModel, Unit>(src.DefaultUnit),
                        src.DefaultPrice, null, null);
                });


            CreateMap<Item, ItemDataModel>()
                .ForMember(d => d.DatabaseId, opt => opt.Ignore())
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d=>d.Code, opt=>opt.MapFrom(src=>src.Code))
                .ForMember(d=>d.Name, opt=>opt.MapFrom(src=>src.Name))
                .ForMember(d=>d.DefaultPrice, opt=>opt.MapFrom(src=>src.DefaultPrice))
                .ForMember(d=>d.DefaultUnit, opt=>opt.MapFrom(src=>src.DefaultUnit))
                .ForMember(d => d.AvailableUnits, opt => opt.ResolveUsing((src, des, i, context) =>
                {
                    var units = new List<ItemUnitDataModel>();
                    foreach (var unit in src.AvailableUnits)
                    {
                        units.Add(new ItemUnitDataModel()
                        {
                            Unit = context.Mapper.Map<Unit, UnitDataModel>(unit.Key),
                            Item = des,
                            ConversionFactor = unit.Value
                        });
                    }

                    return units;
                }))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<ItemDataModel, Item>()
                .ConstructUsing((src, context) =>
                {
                    var units = src.AvailableUnits.ToDictionary(
                        dataUnit => context.Mapper.Map<UnitDataModel, Unit>(dataUnit.Unit),
                        dataUnit => dataUnit.ConversionFactor);
                    return new Item(src.Id, src.Code, src.Name,
                        units, context.Mapper.Map<UnitDataModel, Unit>(src.DefaultUnit),
                        src.DefaultPrice, null, null);
                });

            #endregion
        }
    }
}
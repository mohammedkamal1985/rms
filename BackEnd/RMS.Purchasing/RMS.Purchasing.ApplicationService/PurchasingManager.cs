﻿using System;
using System.Linq;
using System.Collections.Generic;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Entities;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.DomainService.Infrastructure;
using RMS.Purchasing.Core.Enums;
using ExceptionHandling.CustomException;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.Core.Interfaces;

namespace RMS.Purchasing.ApplicationService
{
    public class PurchasingManager : IPurchasingManager
    {
        private readonly IPurchasingUnitOfWork _unitOfWork;
        private readonly IMappingService _mappingService;
        private readonly IPurchasingDomainService _domainService;
        private readonly ILogger _appLogger;

        public PurchasingManager(IPurchasingUnitOfWork unitOfWork,
            IMappingService mappingService,
            IPurchasingDomainService domainService,
            ILogger appLogger)
        {
            _unitOfWork = unitOfWork;
            _mappingService = mappingService;
            _domainService = domainService;
            _appLogger = appLogger;
        }

        #region Orders

        public ServiceReadResponse<string> GetNewOrderSerial()
        {
            var serial = _domainService.GenerateOrderSerial();
            return new ServiceReadResponse<string>(serial);
        }

        /// <summary>
        /// Create a new Purchasing Order
        /// </summary>
        /// <param name="request">Order Business Model</param>
        /// <returns>Newly created Purchasing Order</returns>
        public ServiceWriteResponse<PurchasingOrderModel> CreatePurchasingOrder(
            ServiceRequest<NewPurchasingOrderModel> request)
        {
            try
            {
                if (request?.Data == null)
                {
                    //TODO log business exception and construct user friendly message
                    throw new BusinessException();
                }

                var serial = _domainService.GenerateOrderSerial();
                var order = _mappingService.Map<NewPurchasingOrderModel, PurchasingOrder>(request.Data);
                order = order.WithSerialNumber(serial).Create();

                if (order.ValidationErrors.Any())
                {
                    return new ServiceWriteResponse<PurchasingOrderModel>()
                    {
                        Result = null,
                        ValidationErrors = order.ValidationErrors.Select(e => e.ValidationErrorMessage).ToList()
                    };
                }

                if (!SavePurchasingOrder(order))
                {
                    throw new BusinessException();
                    //TODO log business exception and construct user friendly message
                }

                _unitOfWork.SaveChanges();
                _unitOfWork.CloseConnection();
                var businessModel = _mappingService.Map<PurchasingOrder, PurchasingOrderModel>(order);
                return new ServiceWriteResponse<PurchasingOrderModel>()
                {
                    Result = businessModel
                };
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        /* Delivery Order Scenario and API is not implemented yet
         public ServiceWriteResponse<DeliveryOrderModel> CreateDeliveryOrder(
            ServiceRequest<NewPurchasingOrderModel> request)
        {
            try
            {
                var serial = _domainService.GenerateOrderSerial();
                var orderCustomer = _mappingService.Map<CustomerModel, Customer>(request.Data.Customer);
                var deliveryAddress = _mappingService.Map<CustomerAddressModel, Address>(request.Data.newOrder.DeliveryAddress);
                var order = new DeliveryOrder(serial, DateTime.Now, null,
                    orderCustomer, OrderStatus.New, 0, string.Empty,
                    new ReadOnlyCollection<OrderLine>(new List<OrderLine>()),
                    new ReadOnlyCollection<PaymentBase>(new List<PaymentBase>()),
                    request.Data.SalesmanId, request.Data.MachineName, request.Data.WarehouseId,
                    deliveryAddress, null, false);
                order = order.Create();

                if (order.ValidationErrors.Any())
                {
                    var response = new ServiceWriteResponse<DeliveryOrderModel>()
                    {
                        Result = null,
                        ValidationErrors = order.ValidationErrors.Select(e => e.ValidationErrorMessage).ToList()
                    };
                    return response;
                }

                if (!order.ValidationErrors.Any())
                {
                    var orderDataModel = _mappingService.Map<DeliveryOrder, OrderDataModel>(order);

                    _unitOfWork.OrderWriteRepository.Add(orderDataModel);

                    var businessModel = _mappingService.Map<DeliveryOrder, DeliveryOrderModel>(order);
                    var response = new ServiceWriteResponse<DeliveryOrderModel>() {Result = businessModel};
                }

                return null;
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }
*/

        /// <summary>
        /// Add order line
        /// </summary>
        /// <returns></returns>
        public ServiceWriteResponse<PurchasingOrderModel> AddOrderLine(ServiceRequest<OrderLineModel> request)
        {
            try
            {
                if (request?.Data == null || string.IsNullOrEmpty(request.Data.Item.Code))
                {
                    //TODO LOG & Handle DatabaseException
                    //TODO Build friendly user message
                    throw new BusinessException();
                }

                var orderLineBusinessModel = request.Data;
                
                //1.If adding item by scanning code, then you need to fill remaining item data
                if (orderLineBusinessModel.Unit == null || string.IsNullOrEmpty(orderLineBusinessModel.Item.Name))
                {
                    orderLineBusinessModel = FillOrderLineMissingData(orderLineBusinessModel);
                }

                //2.get order from database by Id
                var orderDataModel = _unitOfWork.OrderQueryRepository.FindByDomainId(orderLineBusinessModel.OrderId);
                var order = orderDataModel.Type == (int) OrderTypes.Purchasing
                    ? _mappingService.Map<OrderDataModel, PurchasingOrder>(orderDataModel)
                    : _mappingService.Map<OrderDataModel, DeliveryOrder>(orderDataModel);
                
                //3. convert order line business model to domain model to run business 
                var orderLineDomainModel = _mappingService.Map<OrderLineModel, OrderLine>(orderLineBusinessModel);

                //4.Calculate order line item price per unit
                orderLineDomainModel = CalculateOrderLinePrice(orderLineDomainModel, order);

                
                var response = new ServiceWriteResponse<PurchasingOrderModel>();
                //5.Add order line to order
                order = order.AddOrderLine(orderLineDomainModel);
                if (!order.ValidationErrors.Any())
                {
                    //5.1. Select the Line from the Order Aggregate and did not use the mapped Domain OrderLine [domainOrderLine]
                    //  Because the line may be already exists and the quantity increased
                    var orderLine = order.OrderLines.SingleOrDefault(l =>
                        l.LineItem.Code == orderLineDomainModel.LineItem.Code &&
                        l.Quantity.Unit == orderLineDomainModel.Quantity.Unit);
                    
                    //5.2.Convert domain entity to database entity
                    var lineDataModel = _mappingService.Map<OrderLine, OrderLineDataModel>(orderLine);

                    //5.3. Save order line to database
                    if (SaveOrderLine(orderLineDomainModel, lineDataModel, orderDataModel))
                    {
                        //5.3.1. If order line is saved successfully, update order database entity with the new values and save to data base
                        orderDataModel.Net = order.Net;
                        orderDataModel.Total = order.Total;
                        _unitOfWork.OrderWriteRepository.Update(orderDataModel);

                        _unitOfWork.SaveChanges();
                        _unitOfWork.CloseConnection();
                    }
                    else
                    {
                        //TODO LOG & Handle DatabaseException
                        //TODO Build friendly user message
                        throw new BusinessException();
                    }
                }
                else
                {
                    //5.2. order line is not valid, so add the validation errors to response
                    response.ValidationErrors = order.ValidationErrors.Select(r => r.ValidationErrorMessage).ToList();
                }

                var businessModel = (order.OrderType == OrderTypes.Purchasing)
                    ? _mappingService.Map<PurchasingOrder, PurchasingOrderModel>(order)
                    : _mappingService.Map<DeliveryOrder, DeliveryOrderModel>((DeliveryOrder) order);
                response.Result = businessModel;

                return response;
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        /// <summary>
        /// Cancel Order  
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ServiceWriteResponse<bool> CancelOrder(ServiceRequest<PurchasingOrderModel> request)
        {
            try
            {
                if (request?.Data == null)
                {
                    //TODO log business exception and construct user friendly message
                    throw new BusinessException();
                }

                //get order by Id
                var domainOrder = _mappingService.Map<PurchasingOrderModel, PurchasingOrder>(request.Data);
                var orderDM = _unitOfWork.OrderQueryRepository.FindByDomainId(domainOrder.Id);

                var response = new ServiceWriteResponse<bool>();
                domainOrder = domainOrder.Cancel();
                if (!domainOrder.ValidationErrors.Any())
                {
                    orderDM.Status = (int) OrderStatus.Canceled;
                    orderDM.Notes = domainOrder.Notes;
                    orderDM.CloseDate = DateTime.Now;
                    _unitOfWork.OrderWriteRepository.Update(orderDM);
                    _unitOfWork.SaveChanges();
                    response.Result = true;
                    return response;
                }

                response.ValidationErrors = domainOrder.ValidationErrors.Select(e => e.ValidationErrorMessage).ToList();
                response.Result = false;
                return response;
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        /// <summary>
        /// Cancel Order  
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ServiceWriteResponse<bool> HoldOrder(ServiceRequest<PurchasingOrderModel> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    //TODO log business exception and construct user frindly message
                    throw new BusinessException();
                }

                //get order by Id
                var domainOrder = _mappingService.Map<PurchasingOrderModel, PurchasingOrder>(request.Data);
                var orderDM = _unitOfWork.OrderQueryRepository.FindByDomainId(domainOrder.Id);

                var response = new ServiceWriteResponse<bool>();
                domainOrder = domainOrder.Hold();
                if (!domainOrder.ValidationErrors.Any())
                {
                    orderDM.Status = (int) OrderStatus.Hold;
                    orderDM.Notes = domainOrder.Notes;
                    _unitOfWork.OrderWriteRepository.Update(orderDM);
                    _unitOfWork.SaveChanges();
                    response.Result = true;
                    return response;
                }

                response.ValidationErrors = domainOrder.ValidationErrors.Select(e => e.ValidationErrorMessage).ToList();
                response.Result = false;
                return response;
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceWriteResponse<bool> CloseOrder(ServiceRequest<PurchasingOrderModel> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    //TODO log business exception and construct user frindly message
                    throw new BusinessException();
                }

                //get order by Id
                var domainOrder = _mappingService.Map<PurchasingOrderModel, PurchasingOrder>(request.Data);
                var orderDM = _unitOfWork.OrderQueryRepository.FindByDomainId(domainOrder.Id);

                var response = new ServiceWriteResponse<bool>();
                domainOrder = domainOrder.Close();
                if (!domainOrder.ValidationErrors.Any())
                {
                    orderDM.Status = (int) OrderStatus.Canceled;
                    orderDM.Notes = domainOrder.Notes;
                    orderDM.CloseDate = DateTime.Now;
                    _unitOfWork.OrderWriteRepository.Update(orderDM);
                    _unitOfWork.SaveChanges();
                    response.Result = true;
                    return response;
                }

                response.ValidationErrors = domainOrder.ValidationErrors.Select(e => e.ValidationErrorMessage).ToList();
                response.Result = false;
                return response;
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        #endregion Orders

        #region Private Methods

        private bool SavePurchasingOrder(PurchasingOrder order)
        {
            var orderDataModel = _mappingService.Map<PurchasingOrder, OrderDataModel>(order);
            var customer = _unitOfWork.CustomerRepository.FindByDomainId(order.Customer.Id);
            orderDataModel.CustomerId = customer.DatabaseId;
            orderDataModel = _unitOfWork.OrderWriteRepository.Add(orderDataModel);
            return orderDataModel.DatabaseId != 0;
        }

        private OrderLine CalculateOrderLinePrice(OrderLine domainOrderLine, IPurchasingOrder order)
        {
            //Get PriceLists
            var pricesDataModel = _unitOfWork.ItemRepository.GetPrices(domainOrderLine.LineItem.Code);
            var priceLists = _mappingService.Map<List<PriceListDataModel>, List<PriceList>>(pricesDataModel);

            //Find existing order line to use its Price
            var existingLine = order.OrderLines.SingleOrDefault(line =>
                line.LineItem.Code == domainOrderLine.LineItem.Code &&
                line.Quantity.Unit.Code == domainOrderLine.Quantity.Unit.Code);
            
            decimal price;
            if (existingLine != null)
            {
                price = existingLine.UnitPrice;
            }
            else
            {
                price = _domainService.CalculateItemPrice(domainOrderLine.LineItem, domainOrderLine.Quantity.Unit,
                    priceLists, order.OrderType, order.Customer);
            }
            
            return domainOrderLine.WithPrice(price);
        }

        private bool SaveOrderLine(OrderLine orderLine, OrderLineDataModel lineDataModel, OrderDataModel orderDataModel)
        {
            //Convert Domain Item line to Data base Model
            var lineItem = _unitOfWork.ItemRepository.FindByDomainId(lineDataModel.LineItem.Id);
            lineDataModel.ItemId = lineItem.DatabaseId;
            lineDataModel.LineItem = lineItem;
            lineDataModel.OrderId = orderDataModel.DatabaseId;
            lineDataModel.UnitId = lineItem.AvailableUnits.Single(u => u.Unit.Id == lineDataModel.Unit.Id).Unit.DatabaseId;

            if (orderLine.Quantity.Value == orderLine.Quantity.Value)
            {
                //The order line is first time to be added
                orderDataModel.OrderLines.Add(lineDataModel);
                lineDataModel = _unitOfWork.OrderLineWriteRepository.Add(lineDataModel);
            }
            else
            {
                //Order line already exists and the quantity is increased
                lineDataModel.DatabaseId = orderDataModel.OrderLines.First(l => l.ItemId == lineDataModel.ItemId && l.UnitId == lineDataModel.UnitId).DatabaseId;
                lineDataModel = _unitOfWork.OrderLineWriteRepository.Update(lineDataModel);
            }

            return lineDataModel.DatabaseId != 0;
        }

        private OrderLineModel FillOrderLineMissingData(OrderLineModel orderLine)
        {
            // Get Item by code
            var itemDataModel = _unitOfWork.ItemRepository.GetByCode(orderLine.Item.Code);
            var itemDomainModel = _mappingService.Map<Item, ItemModel>(
                _mappingService.Map<ItemDataModel, Item>(itemDataModel));
            orderLine.Item = itemDomainModel;
            orderLine.Quantity = 1;
            orderLine.Unit = itemDomainModel.DefaultUnit;

            return orderLine;
        }

        #endregion
    }
}
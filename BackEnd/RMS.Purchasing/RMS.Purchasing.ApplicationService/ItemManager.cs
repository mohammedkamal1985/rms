﻿using System;
using System.Collections.Generic;
using ExceptionHandling.CustomException;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.ApplicationService
{
    public class ItemManager : IItemManager
    {
        private readonly IPurchasingUnitOfWork _unitOfWork;
        private readonly IMappingService _mappingService;

        public ItemManager(IPurchasingUnitOfWork unitOfWork, IMappingService mappingService)
        {
            _unitOfWork = unitOfWork;
            _mappingService = mappingService;
        }

        public ServiceReadResponse<ItemModel> GetByCode(ServiceRequest<string> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    throw new BusinessException();
                }

                _unitOfWork.OpenConnection();
                var itemDataModel = _unitOfWork.ItemRepository.GetByCode(request.Data);
                _unitOfWork.CloseConnection();

                var result = _mappingService.Map<Item, ItemModel>(
                    _mappingService.Map<ItemDataModel, Item>(itemDataModel));
                return new ServiceReadResponse<ItemModel>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }

        public ServiceReadResponse<List<ItemModel>> GetByName(ServiceRequest<string> request)
        {
            try
            {
                if (request == null || request.Data == null)
                {
                    throw new BusinessException();
                }

                _unitOfWork.OpenConnection();
                var itemDataModels = _unitOfWork.ItemRepository.GetByName(request.Data);
                _unitOfWork.CloseConnection();

                var result = _mappingService.Map<List<Item>, List<ItemModel>>(
                    _mappingService.Map<List<ItemDataModel>, List<Item>>(itemDataModels));
                return new ServiceReadResponse<List<ItemModel>>(result);
            }
            catch (RepositoryException ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                //TODO Build friendly user message
                var message = "";
                throw new BusinessException(message, ex);
            }
        }
    }
}
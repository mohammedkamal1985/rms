﻿using System.Collections.Generic;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;

namespace RMS.Purchasing.DomainService.Infrastructure
{
    public interface IPurchasingDomainService
    {
        string GenerateOrderSerial();
        decimal ConvertQuantityToItemDefaultUnitQuantity(decimal quantity, Unit unit, Item item);
        decimal CalculateItemPrice(Item item, Unit unit, List<PriceList> priceLists, OrderTypes orderType, Customer orderCustomer, decimal quantity = 1);
    }
}

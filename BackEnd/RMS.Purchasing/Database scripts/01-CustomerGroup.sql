USE [RMS];
GO


IF OBJECT_ID('[POS].[CustomerGroup]') IS NOT NULL
BEGIN 
	print('tabel found')
    DROP table [POS].[CustomerGroup] 
END 
GO
CREATE TABLE [POS].[CustomerGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](20) NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_CustomerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



IF OBJECT_ID('[POS].[CustomerGroupSelect]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerGroupSelect] 
END 
GO
CREATE PROC [POS].[CustomerGroupSelect] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  

	BEGIN TRAN

	SELECT [Id], [Code], [Name] 
	FROM   [POS].[CustomerGroup] 
	WHERE  ([Id] = @Id OR @Id IS NULL) 

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerGroupInsert]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerGroupInsert] 
END 
GO
CREATE PROC [POS].[CustomerGroupInsert] 
    @Code nvarchar(20) = NULL,
    @Name nvarchar(50) = NULL
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [POS].[CustomerGroup] ([Code], [Name])
	SELECT @Code, @Name
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [Code], [Name]
	FROM   [POS].[CustomerGroup]
	WHERE  [Id] = SCOPE_IDENTITY()
	-- End Return Select <- do not remove
               
	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerGroupUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerGroupUpdate] 
END 
GO
CREATE PROC [POS].[CustomerGroupUpdate] 
    @Id int,
    @Code nvarchar(20) = NULL,
    @Name nvarchar(50) = NULL
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [POS].[CustomerGroup]
	SET    [Code] = @Code, [Name] = @Name
	WHERE  [Id] = @Id
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [Code], [Name]
	FROM   [POS].[CustomerGroup]
	WHERE  [Id] = @Id	
	-- End Return Select <- do not remove

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerGroupDelete]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerGroupDelete] 
END 
GO
CREATE PROC [POS].[CustomerGroupDelete] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [POS].[CustomerGroup]
	WHERE  [Id] = @Id

	COMMIT
GO
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


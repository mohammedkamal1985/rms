USE [RMS];
GO

IF OBJECT_ID('[POS].[CustomerAddresses]') IS NOT NULL
BEGIN 
    DROP table [POS].[CustomerAddresses]
END 
GO
CREATE TABLE [POS].[CustomerAddresses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BlockNo] [int] NULL,
	[Street] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Landmark] [nvarchar](50) NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerAddresses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [POS].[CustomerAddresses]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAddresses_Customers] FOREIGN KEY([CustomerId])
REFERENCES [POS].[Customers] ([Id])
GO

ALTER TABLE [POS].[CustomerAddresses] CHECK CONSTRAINT [FK_CustomerAddresses_Customers]
GO



IF OBJECT_ID('[POS].[CustomerAddressesSelect]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerAddressesSelect] 
END 
GO
CREATE PROC [POS].[CustomerAddressesSelect] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  

	BEGIN TRAN

	SELECT [Id], [BlockNo], [Street], [Region], [State], [Country], [Landmark], [CustomerId] 
	FROM   [POS].[CustomerAddresses] 
	WHERE  ([Id] = @Id OR @Id IS NULL) 

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerAddressesInsert]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerAddressesInsert] 
END 
GO
CREATE PROC [POS].[CustomerAddressesInsert] 
    @BlockNo int = NULL,
    @Street nvarchar(50) = NULL,
    @Region nvarchar(50) = NULL,
    @State nvarchar(50) = NULL,
    @Country nvarchar(50) = NULL,
    @Landmark nvarchar(50) = NULL,
    @CustomerId int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [POS].[CustomerAddresses] ([BlockNo], [Street], [Region], [State], [Country], [Landmark], [CustomerId])
	SELECT @BlockNo, @Street, @Region, @State, @Country, @Landmark, @CustomerId
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [BlockNo], [Street], [Region], [State], [Country], [Landmark], [CustomerId]
	FROM   [POS].[CustomerAddresses]
	WHERE  [Id] = SCOPE_IDENTITY()
	-- End Return Select <- do not remove
               
	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerAddressesUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerAddressesUpdate] 
END 
GO
CREATE PROC [POS].[CustomerAddressesUpdate] 
    @Id int,
    @BlockNo int = NULL,
    @Street nvarchar(50) = NULL,
    @Region nvarchar(50) = NULL,
    @State nvarchar(50) = NULL,
    @Country nvarchar(50) = NULL,
    @Landmark nvarchar(50) = NULL,
    @CustomerId int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [POS].[CustomerAddresses]
	SET    [BlockNo] = @BlockNo, [Street] = @Street, [Region] = @Region, [State] = @State, [Country] = @Country, [Landmark] = @Landmark, [CustomerId] = @CustomerId
	WHERE  [Id] = @Id
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [BlockNo], [Street], [Region], [State], [Country], [Landmark], [CustomerId]
	FROM   [POS].[CustomerAddresses]
	WHERE  [Id] = @Id	
	-- End Return Select <- do not remove

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomerAddressesDelete]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomerAddressesDelete] 
END 
GO
CREATE PROC [POS].[CustomerAddressesDelete] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [POS].[CustomerAddresses]
	WHERE  [Id] = @Id

	COMMIT
GO
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


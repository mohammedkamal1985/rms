USE [RMS]
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Vegetables'
           ,'001')
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Fruits'
           ,'002')
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Juices'
           ,'003')
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Sweets'
           ,'004')
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Meat'
           ,'005')
GO

INSERT INTO [POS].[ItemCategories]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Soda'
           ,'006')
GO

INSERT INTO [POS].[ItemGroups]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Group'
           ,'001')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Kilogram'
           ,'Kg')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Gram'
           ,'gm')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Box'
           ,'box')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Unit'
           ,'unit')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Bag'
           ,'bag')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Liter'
           ,'lt')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Bottel'
           ,'Bl')
GO

INSERT INTO [POS].[Units]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newId())
           ,'Dozen'
           ,'Dz')
GO


INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000001'
           ,'Tomato'
           ,1
           ,7
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (1
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (1
           ,2
           ,1000)
GO


INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000002'
           ,'Botato'
           ,1
           ,4.5
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (2
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (2
           ,2
           ,1000)
GO
INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000003'
           ,'Carrot'
           ,1
           ,2.5
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (3
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (3
           ,2
           ,1000)
GO
INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000004'
           ,'Appel'
           ,1
           ,20
           ,2
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (4
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (4
           ,2
           ,1000)
GO
INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000005'
           ,'Banana'
           ,1
           ,11
           ,2
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (5
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (5
           ,2
           ,1000)
GO

INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000006'
           ,'Orang'
           ,1
           ,5.25
           ,2
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (6
           ,1
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (6
           ,2
           ,1000)
GO
INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000007'
           ,'Lamar Orang Juice'
           ,7
           ,15
           ,3
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (7
           ,7
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (7
           ,8
           ,0.083)
GO

INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000008'
           ,'Lamar Apple Juice'
           ,7
           ,15
           ,3
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (8
           ,7
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (8
           ,8
           ,0.083)
GO

INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000009'
           ,'Juhina Apple Juice'
           ,7
           ,12.2
           ,3
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (9
           ,7
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (9
           ,8
           ,0.083)
GO

INSERT INTO [POS].[Items]
           ([DomainId]
           ,[Code]
           ,[Name]
           ,[DefaultUnitId]
           ,[DefaultPrice]
           ,[CategoryId]
           ,[GroupId])
     VALUES
           ((select newId())
           ,'000000010'
           ,'Juhina Orang Juice'
           ,7
           ,12.2
           ,3
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (10
           ,7
           ,1)
GO
INSERT INTO [POS].[ItemUnits]
           ([ItemId]
           ,[UnitId]
           ,[ConversionFactor])
     VALUES
           (10
           ,8
           ,0.083)
GO





USE [RMS]
GO

INSERT INTO [POS].[CustomerGroups]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newID())
           ,'Platinum'
           ,'001')
GO

INSERT INTO [POS].[CustomerGroups]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newID())
           ,'Golden'
           ,'002')
GO

INSERT INTO [POS].[CustomerGroups]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newID())
           ,'Silver'
           ,'003')
GO

INSERT INTO [POS].[CustomerGroups]
           ([DomainId]
           ,[Name]
           ,[Code])
     VALUES
           ((select newID())
           ,'Bronze'
           ,'004')
GO

--//////////////////////////////////////////////////////////////////////////////////

INSERT INTO [POS].[Customers]
           ([DomainId]
           ,[Name]
           ,[PhoneNumber]
           ,[IsDefault]
           ,[GroupId])
     VALUES
           ((select newID())
           ,'Mohamed Kamal'
           ,'01098880614'
           ,0
           ,1)
GO
USE [RMS]
GO

INSERT INTO [POS].[CustomerAddresses]
           ([BlockNo]
           ,[Street]
           ,[Region]
           ,[State]
           ,[Country]
           ,[CustomerId])
     VALUES
           (566
           ,'Plastin St.'
           ,'Maadi'
           ,'Cairo'
           ,'Egypt'
           ,1)
GO



INSERT INTO [POS].[Customers]
           ([DomainId]
           ,[Name]
           ,[PhoneNumber]
           ,[IsDefault]
           ,[GroupId])
     VALUES
           ((select newID())
           ,'Mohamed Kamal'
           ,'01111331118'
           ,0
           ,2)
GO

INSERT INTO [POS].[CustomerAddresses]
           ([BlockNo]
           ,[Street]
           ,[Region]
           ,[State]
           ,[Country]
		   ,[Landmark]
           ,[CustomerId])
     VALUES
           (1
           ,'Plastin St.'
           ,'Maadi'
           ,'Cairo'
           ,'Egypt'
		   ,'Next to Bandar Mall',
		   2)
GO
     

INSERT INTO [POS].[Customers]
           ([DomainId]
           ,[Name]
           ,[PhoneNumber]
           ,[IsDefault]
           ,[GroupId])
     VALUES
           ((select newID())
           ,'Malak M. Kamal'
           ,'01027723371'
           ,0
           ,1)
GO

INSERT INTO [POS].[CustomerAddresses]
           ([BlockNo]
           ,[Street]
           ,[Region]
           ,[State]
           ,[Country]
		   ,[Landmark]
           ,[CustomerId])
     VALUES
           (9
           ,'7oreyah School St.'
           ,'Hehia'
           ,'Sharqyah'
           ,'Egypt'
		   ,'Next to Hehia Chruch',
		   3)
GO
     
	 INSERT INTO [POS].[Customers]
           ([DomainId]
           ,[Name]
           ,[PhoneNumber]
           ,[IsDefault]
           ,[GroupId])
     VALUES
           ((select newID())
           ,'Tak-away Customer'
           ,''
           ,1
           ,4)
GO
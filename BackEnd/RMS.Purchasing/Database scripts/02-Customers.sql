USE [RMS];
GO

IF OBJECT_ID('[POS].[Customers]') IS NOT NULL
BEGIN 
    DROP Table [POS].[Customers] 
END 
GO
CREATE TABLE [POS].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[IsDefault] [bit] NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [POS].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_CustomerGroups] FOREIGN KEY([GroupId])
REFERENCES [POS].[CustomerGroups] ([Id])
GO

ALTER TABLE [POS].[Customers] CHECK CONSTRAINT [FK_Customers_CustomerGroups]
GO

IF OBJECT_ID('[POS].[CustomersSelect]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomersSelect] 
END 
GO
CREATE PROC [POS].[CustomersSelect] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  

	BEGIN TRAN

	SELECT [Id], [Name], [Phone], [IsDefault], [GroupId] 
	FROM   [POS].[Customers] 
	WHERE  ([Id] = @Id OR @Id IS NULL) 

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomersInsert]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomersInsert] 
END 
GO
CREATE PROC [POS].[CustomersInsert] 
    @Name nvarchar(100),
    @Phone nvarchar(20),
    @IsDefault bit = NULL,
    @GroupId int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [POS].[Customers] ([Name], [Phone], [IsDefault], [GroupId])
	SELECT @Name, @Phone, @IsDefault, @GroupId
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [Name], [Phone], [IsDefault], [GroupId]
	FROM   [POS].[Customers]
	WHERE  [Id] = SCOPE_IDENTITY()
	-- End Return Select <- do not remove
               
	COMMIT
GO
IF OBJECT_ID('[POS].[CustomersUpdate]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomersUpdate] 
END 
GO
CREATE PROC [POS].[CustomersUpdate] 
    @Id int,
    @Name nvarchar(100),
    @Phone nvarchar(20),
    @IsDefault bit = NULL,
    @GroupId int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [POS].[Customers]
	SET    [Name] = @Name, [Phone] = @Phone, [IsDefault] = @IsDefault, [GroupId] = @GroupId
	WHERE  [Id] = @Id
	
	-- Begin Return Select <- do not remove
	SELECT [Id], [Name], [Phone], [IsDefault], [GroupId]
	FROM   [POS].[Customers]
	WHERE  [Id] = @Id	
	-- End Return Select <- do not remove

	COMMIT
GO
IF OBJECT_ID('[POS].[CustomersDelete]') IS NOT NULL
BEGIN 
    DROP PROC [POS].[CustomersDelete] 
END 
GO
CREATE PROC [POS].[CustomersDelete] 
    @Id int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [POS].[Customers]
	WHERE  [Id] = @Id

	COMMIT
GO
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


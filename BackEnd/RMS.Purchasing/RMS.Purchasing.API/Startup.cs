﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Logging;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.ApplicationService;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Mapping;
using RMS.Purchasing.DomainService;
using RMS.Purchasing.DomainService.Infrastructure;
using RMS.Purchasing.Repository;
using RMS.Purchasing.Repository.Infrastucture;

namespace RMS.Purchasing.API
{
    public class Startup
    {
        private const string corsPolicyName = "RMSCORS";
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ILogger, Logger>();
            services.AddScoped<IMappingService, MappingService>();
            services.AddScoped<IPurchasingDomainService, PurchasingDomainService>();
            
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IOrderLineWriteRepository, OrderLineWriteRepository>();
            services.AddScoped<IOrderPaymentWriteRepository, OrderPaymentWriteRepository>();
            services.AddScoped<IOrderWriteRepository, OrderWriteRepository>();
            services.AddScoped<IOrderQueryRepository, OrderQueryRepository>();
            services.AddScoped<IStockQueryRepository, StockQueryRepository>();
            services.AddScoped<IStockWriteRepository, StockWriteRepository>();
            services.AddTransient<IPurchasingUnitOfWork, PurchasingUnitOfWork>();

            services.AddScoped<IPurchasingManager, PurchasingManager>();
            services.AddScoped<ICustomerManager, CustomerManager>();
            services.AddScoped<IItemManager, ItemManager>();
            services.AddScoped<IStockManager, StockManager>();

            services.AddMvc();
            services.AddCors(options=>options.AddPolicy(corsPolicyName,builder => {
                builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
            }));
            services.AddDbContext<PurchasingContext>(o => o.UseSqlServer(Environment.GetEnvironmentVariable("ConnectionString"),//"Server=localhost\\\\SQLSERVER2017;Database=RMS;Trusted_Connection=True;MultipleActiveResultSets=true",//
                sqlOptions => sqlOptions.MigrationsAssembly("RMS.Purchasing.Repository")));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ILogger logger)
        {
            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});

            if (!env.IsProduction())
            {
                app.UseCors(corsPolicyName);
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "RMS.Purchasing.API V1");
                });
                //app.ConfigureExceptionHandler();
                app.UseMvc();
            }
        }
    }
}

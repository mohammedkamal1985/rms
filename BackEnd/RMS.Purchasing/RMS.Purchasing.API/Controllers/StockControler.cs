﻿using Microsoft.AspNetCore.Mvc;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.API.Controllers
{
    [Produces("application/json")]
    [Route("api/stock")]
    public class StockControler : Controller
    {
        private IStockManager  _stockManager;
        public StockControler (IStockManager stockManager)
		{
            _stockManager = stockManager;
        }

        [HttpPost]
        [Route("reserve")]
        public IActionResult ReserveStock([FromBody] StockTransactionModel transaction)
        {
            var request = new ServiceRequest<StockTransactionModel>(transaction);
            var resposne = _stockManager.ReserveStock(request);
            return new OkObjectResult(resposne);
        }

        [HttpPost]
        [Route("reserve")]
        public IActionResult ReserveStock(string itemCode)
        {
            var request = new ServiceRequest<string>(itemCode);
            var resposne = _stockManager.ReserveStock(request);
            return new OkObjectResult(resposne);
        }
    }
}
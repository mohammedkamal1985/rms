﻿using Microsoft.AspNetCore.Mvc;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;

namespace RMS.Purchasing.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ICustomerManager _customerManager;
        public CustomerController(ICustomerManager customerManager)
        {
            _customerManager = customerManager;
        }

        [Route("Name/{name}")]
        [HttpGet]
        public IActionResult GetByName(string name)
        {
            var request = new ServiceRequest<string>(name);
            var response = _customerManager.GetByName(request);
            return new OkObjectResult(response.Result);
        }

        [Route("Default")]
        [HttpGet]
        public IActionResult GetDefault()
        {
            var response = _customerManager.GetDefaultCustomer();
            return new OkObjectResult(response.Result);
        }

        [Route("Phone/{phone}")]
        [HttpGet]
        public IActionResult GetByPhone(string phone)
        {
            var request = new ServiceRequest<string>(phone);
            var response = _customerManager.GetByPhoneNumber(request);
            return new OkObjectResult(response.Result);
        }
    }
}
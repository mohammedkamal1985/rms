﻿using System;
using Microsoft.AspNetCore.Mvc;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Order/{orderId}/Line")]
    public class OrderLineController : Controller
    {
        private IPurchasingManager _purchasingManager;
        public OrderLineController(IPurchasingManager purchasinganager)
        {
            _purchasingManager = purchasinganager;
        }

        [HttpPost]
        [Route("Code/{itemCode}")]
        public IActionResult AddOrderLine(string orderId,string itemCode)
        {
            OrderLineModel model = new OrderLineModel()
            {
                OrderId = new Guid(orderId),
                Item = new ItemModel() { Code = itemCode },
                Quantity = 1
            };
            var request = new ServiceRequest<OrderLineModel>(model);

            var result = _purchasingManager.AddOrderLine(request);
            return new OkObjectResult(result);
        }

        [HttpPost]
        public IActionResult AddOrderLine(string orderId, [FromBody]OrderLineModel orderLine)
        {
            orderLine.OrderId = new Guid(orderId);
            var request = new ServiceRequest<OrderLineModel>(orderLine);
            var result = _purchasingManager.AddOrderLine(request);
            return new OkObjectResult(result);
        }
    }
}
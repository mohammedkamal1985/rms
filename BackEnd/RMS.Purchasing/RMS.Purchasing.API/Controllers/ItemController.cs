﻿using Microsoft.AspNetCore.Mvc;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;

namespace RMS.Purchasing.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Item")]
    public class ItemController : Controller
    {
        private IItemManager _itemManager;

        public ItemController(IItemManager itemManager)
        {
            _itemManager = itemManager;
        }

        [HttpGet]
        [Route("Name/{name}")]
        public IActionResult GetByName(string name)
        {
            var request = new ServiceRequest<string>(name);
            var response = _itemManager.GetByName(request);
            return new OkObjectResult(response.Result);
        }

        [HttpGet]
        [Route("Code/{code}")]
        public IActionResult GetByCode(string code)
        {
            var request = new ServiceRequest<string>(code);
            var response = _itemManager.GetByCode(request);
            return new OkObjectResult(response.Result);
        }
    }
}
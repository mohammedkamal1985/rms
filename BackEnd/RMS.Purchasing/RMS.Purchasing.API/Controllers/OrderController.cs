﻿using Microsoft.AspNetCore.Mvc;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class OrderController : Controller
    {
        private IPurchasingManager _purchasingManager;
        public OrderController(IPurchasingManager purchasinganager)
        {
            _purchasingManager = purchasinganager;
        }

        [HttpGet]
        [Route("Purchasing/serial")]
        public IActionResult GetNewOrderSerial()
        {
            var newOrderSerial = _purchasingManager.GetNewOrderSerial();
            return new OkObjectResult(newOrderSerial);
        }

        [HttpPost]
        public IActionResult CreatePurchasingOrder([FromBody]NewPurchasingOrderModel order)
        {
            var request = new ServiceRequest<NewPurchasingOrderModel>(order);
            var newOrder = _purchasingManager.CreatePurchasingOrder(request);
            return new OkObjectResult(newOrder);
        }

        [HttpPut]
        [Route("Purchasing/Cancel")]
        public IActionResult CancelOrder([FromBody]PurchasingOrderModel order)
        {
            var request = new ServiceRequest<PurchasingOrderModel>(order);
            var result = _purchasingManager.CancelOrder(request);
            return new OkObjectResult(result);
        }

        [HttpPut]
        [Route("Purchasing/Hold")]
        public IActionResult HoldOrder([FromBody]PurchasingOrderModel order)
        {
            var request = new ServiceRequest<PurchasingOrderModel>(order);
            var result = _purchasingManager.HoldOrder(request);
            return new OkObjectResult(result);
        }

        [HttpPut]
        [Route("Purchasing/Close")]
        public IActionResult CloseOrder([FromBody]PurchasingOrderModel order)
        {
            var request = new ServiceRequest<PurchasingOrderModel>(order);
            var result = _purchasingManager.CloseOrder(request);
            return new OkObjectResult(result);
        }
    }
}
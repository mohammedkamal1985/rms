﻿using System;
using System.Collections.Generic;
using Repository.Base.Interfaces;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.Repository.Infrastucture
{
    public interface IItemRepository:IQueryRepository
    {
        ItemDataModel GetByCode(string code);

        List<ItemDataModel> GetByName(string name);

        List<PriceListDataModel> GetPrices(string itemCode);
        ItemDataModel FindByDomainId(Guid domainId);
    }
}

﻿using System;
using System.Collections.Generic;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using Repository.Base.Interfaces;

namespace RMS.Purchasing.Repository.Infrastucture
{
    public interface ICustomerRepository:IQueryRepository
    {
        List<CustomerDataModel> GetByName(string name);
        CustomerDataModel GetByPhoneNumber(string phoneNumber);
        CustomerDataModel GetDefaultCustomer();
        CustomerDataModel FindByDomainId(Guid domainId);
    }
}

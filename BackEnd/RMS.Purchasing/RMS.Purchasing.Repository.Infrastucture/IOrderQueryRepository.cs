﻿using System;
using Repository.Base.Interfaces;
using RMS.Purchasing.Repository.Infrastucture.DataModel;

namespace RMS.Purchasing.Repository.Infrastucture
{
    public interface IOrderQueryRepository:IQueryRepository
    {
        OrderDataModel FindByDomainId(Guid domainId);
    }
}

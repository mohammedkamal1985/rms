﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class ItemGroupDataModel:DBEntityBase<Guid>
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public List<ItemDataModel> Items { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class PriceListDataModel:DBEntityBase<Guid>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PriorityIndex { get; set; }
        public int Type { get; set; }
        public List<PriceDataModel> Prices { get; set; }
    }
}

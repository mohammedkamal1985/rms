﻿using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class OrderLineDataModel : DBEntityBase
    {
        public int ItemId{get;set;}
        public ItemDataModel LineItem { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public UnitDataModel Unit { get; set; }
        public int UnitId { get; set; }
        public int OrderId { get; set; }
    }
}

﻿using Repository.Base.Sql.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class OrderPaymentDataModel : DBEntityBase
    {
        public int OrderId { get; set; }
        public int PaymentType { get; set; }
        public decimal Value { get; set; }
        public int? AccountType { get; set; }
        public string AccountId { get; set; }
    }
}

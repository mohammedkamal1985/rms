﻿using System;
using System.ComponentModel.DataAnnotations;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class CustomerGroupDataModel:DBEntityBase<Guid>
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

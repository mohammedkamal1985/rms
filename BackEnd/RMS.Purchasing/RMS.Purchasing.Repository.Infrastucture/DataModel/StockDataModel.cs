﻿using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class StockDataModel : DBEntityBase
    {
        public int ItemId { get; set; }
        public ItemDataModel Item { get; set; }
        public decimal Quantity { get; set; }
        public decimal Reserved { get; set; }
        public decimal Available { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class ItemDataModel:DBEntityBase<Guid>
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public UnitDataModel DefaultUnit { get; set; }
        public int DefaultUnitId { get; set; }
        public List<ItemUnitDataModel> AvailableUnits { get; set; }
        public decimal DefaultPrice { get; set; }
        public int CategoryId { get; set; }
        public ItemCategoryDataModel Category { get; set; }
        public int GroupId { get; set; }
        public ItemGroupDataModel Group { get; set; }
        public StockDataModel Stock { get; set; }
    }
}

﻿using System;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class StockTransactionDataModel: DBEntityBase
    {

        public int ItemId { get; set; }
        public ItemDataModel Item { get; set; }
        public decimal Quantity { get; set; }
        public int UnitId { get; set; }
        public UnitDataModel Unit { get; set; }
        public DateTime TimeStamp { get; set; }
        public int Type { get; set; }
        public Guid LocationID { get; set; }
    }
}

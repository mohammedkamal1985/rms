﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class CustomerDataModel : DBEntityBase<Guid>
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDefault { get; set; }
        
        public int GroupId { get; set; }
        public CustomerGroupDataModel Group { get; set; }
        public List<AddressDataModel> DeliveryAddresses { get; set; }
    }
}
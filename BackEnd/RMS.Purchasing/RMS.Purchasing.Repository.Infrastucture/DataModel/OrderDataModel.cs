﻿using System;
using System.Collections.Generic;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class OrderDataModel : DBEntityBase<Guid>
    {
        public string Serial { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int CustomerId { get; set; }
        public CustomerDataModel Customer { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public decimal Total { get; set; }
        public decimal Net { get; set; }
        public decimal Paid { get; set; }
        public decimal Change { get; set; }
        public string Notes { get; set; }
        public AddressDataModel DeliveryAddress { get; set; }
        public int? DeliveryAddressId { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool? PayOnDelivery { get; set; }
        public Guid? SalesmanId { get; set; }
        public string MachineName { get; set; }
        public Guid? WarehouseId { get; set; }
        public ICollection<OrderLineDataModel> OrderLines { get; set; }
        public ICollection<OrderPaymentDataModel> Payments { get; set; }
    }
}

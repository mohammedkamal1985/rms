﻿using System.ComponentModel.DataAnnotations;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class AddressDataModel : DBEntityBase
    {
        public int BlockNo { get; set; }
        public string Street { get; set; }
        
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Landmark { get; set; }
        
        public string PostCode { get; set; }

        public CustomerDataModel Customer { get; set; }
    }
}

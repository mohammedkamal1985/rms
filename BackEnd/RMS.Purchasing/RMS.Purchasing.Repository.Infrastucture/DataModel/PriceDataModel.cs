﻿using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class PriceDataModel:DBEntityBase
    {
        public int ItemId { get; set; }
        public decimal ItemPrice { get; set; }
        public int PriceListId { get; set; }

        public ItemDataModel Item { get; set; }
    }
}

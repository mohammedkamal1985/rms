﻿using System;
using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class UnitDataModel:DBEntityBase<Guid>
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

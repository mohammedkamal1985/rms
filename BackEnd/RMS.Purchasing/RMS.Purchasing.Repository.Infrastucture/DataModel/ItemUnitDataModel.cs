﻿using Repository.Base.Sql.EntityFramework.Entities;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public class ItemUnitDataModel
    {
        public int ItemId { get; set; }
        public ItemDataModel Item { get; set; }
        public int UnitId { get; set; }
        public UnitDataModel Unit { get; set; }
        public decimal ConversionFactor { get; set; }
    }
}

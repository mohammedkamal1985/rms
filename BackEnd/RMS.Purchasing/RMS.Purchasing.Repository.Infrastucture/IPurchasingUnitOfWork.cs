﻿using Repository.Base.Interfaces;

namespace RMS.Purchasing.Repository.Infrastucture.DataModel
{
    public interface IPurchasingUnitOfWork :IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }
        IItemRepository ItemRepository { get; }
        IOrderQueryRepository OrderQueryRepository { get; }
        IOrderWriteRepository OrderWriteRepository { get; }
        IOrderLineWriteRepository OrderLineWriteRepository { get; }
        IStockQueryRepository StockQueryRepository { get; }
        IStockWriteRepository StockWriteRepository { get; }
    }
}

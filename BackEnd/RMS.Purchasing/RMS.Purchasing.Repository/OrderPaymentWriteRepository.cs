﻿using AutoMapper;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class OrderPaymentWriteRepository : WriteRepositoryBase<PurchasingContext, OrderPaymentMetadata, OrderPaymentDataModel>, IOrderPaymentWriteRepository
    {
        IMapper _mapper;

        public OrderPaymentWriteRepository(PurchasingContext context) : base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
             {
                 cfg.AddProfile<PaymentsProfile>();
             }).CreateMapper();
        }

        protected override OrderPaymentMetadata ConvertDataModelToMetaData(OrderPaymentDataModel entity)
        {
            return _mapper.Map<OrderPaymentMetadata>(entity);
        }

        protected override OrderPaymentDataModel ConvertMetaDataToDataModel(OrderPaymentMetadata metadata)
        {
            return _mapper.Map<OrderPaymentDataModel>(metadata);
        }
    }
}

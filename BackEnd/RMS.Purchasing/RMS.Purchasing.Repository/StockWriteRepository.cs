﻿using AutoMapper;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class StockWriteRepository : WriteRepositoryBase<PurchasingContext, StockMetadata, StockDataModel>, IStockWriteRepository
    {
        private IMapper _mapper;

        public StockWriteRepository(PurchasingContext context):base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ItemsMetadataProfile>();
                cfg.AddProfile<StockMetadataProfile>();
            }).CreateMapper();
        }

        protected override StockMetadata ConvertDataModelToMetaData(StockDataModel entity)
        {
            return _mapper.Map<StockDataModel, StockMetadata>(entity);
        }

        protected override StockDataModel ConvertMetaDataToDataModel(StockMetadata metadata)
        {
            return _mapper.Map<StockMetadata, StockDataModel>(metadata);
        }
    }
}

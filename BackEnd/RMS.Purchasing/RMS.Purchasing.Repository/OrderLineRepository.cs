﻿using System;
using System.Data.Common;
using System.Linq;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class OrderLineRepository : RepositoryBase<PurchasingContextdel,OrderLineMetadata, Guid>, IOrderLineRepository
    {
        
        public OrderLineRepository(PurchasingContext context):base(context)
        {
          
        }
        public override void Delete(OrderLineDataModel entity)
        {
            throw new NotImplementedException();
        }
        
        public override OrderLineDataModel FindByDomainId(Guid domainId)
        {
            throw new NotImplementedException();
        }

        protected override OrderLineMetadata ConvertDataModelToMetaData(OrderLineDataModel metadata)
        {
            throw new NotImplementedException();
        }

        protected override OrderLineDataModel ConvertMetaDataToDataModel(OrderLineMetadata metadata)
        {
            throw new NotImplementedException();
        }
    }
}

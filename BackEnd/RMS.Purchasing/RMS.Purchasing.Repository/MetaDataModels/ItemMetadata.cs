﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("Items")]
    public class ItemMetadata: MetadataBase
    {
        public Guid DomainId { get; set; }
        public string Code { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public UnitMetadata DefaultUnit { get; set; }
        public int DefaultUnitId { get; set; }
        public List<ItemUnitMetadata> AvailableUnits { get; set; }
        public decimal DefaultPrice { get; set; }
        public int CategoryId { get; set; }
        public ItemCategoryMetadata Category { get; set; }
        public int GroupId { get; set; }
        public ItemGroupMetadata Group { get; set; }
        public StockMetadata Stock { get; set; }
    }
}

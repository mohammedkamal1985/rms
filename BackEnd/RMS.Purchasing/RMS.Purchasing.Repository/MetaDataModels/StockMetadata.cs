﻿using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("Stocks")]
    public class StockMetadata:MetadataBase
    {
        public int ItemId { get; set; }
        public ItemMetadata Item { get; set; }
        public decimal Quantity { get; set; }
        public decimal Reserved { get; set; }
        public decimal Available { get; set; }
    }
}

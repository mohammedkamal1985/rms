﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("CustomerAddresses")]
    public class AddressMetadata: MetadataBase
    {
        public int BlockNo { get; set; }
        [StringLength(50)]
        public string Street { get; set; }
        [StringLength(50)]
        public string Region { get; set; }
        [StringLength(50)]
        public string State { get; set; }
        [StringLength(50)]
        public string Country { get; set; }
        public string Landmark { get; set; }
        public int CustomerId { get; set; }
        public CustomerMetadata Customer { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("ItemUnits")]
    public class ItemUnitMetadata: MetadataBase
    {
        public int ItemId { get; set; }
        public ItemMetadata Item { get; set; }
        public int UnitId { get; set; }
        public UnitMetadata Unit { get; set; }
        public decimal ConversionFactor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("Customers")]
    public class CustomerMetadata:MetadataBase
    {
        public Guid DomainId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(20)]
        public string PhoneNumber { get; set; }
        public bool IsDefault { get; set; }
        public int GroupId { get; set; }
        public CustomerGroupMetadata Group { get; set; }
        public List<AddressMetadata> DeliveryAddresses { get; set; }
    }
}

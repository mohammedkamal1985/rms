﻿using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    public class OrderPaymentMetadata : MetadataBase
    {
        public int OrderId { get; set; }
        public int PaymentType { get; set; }
        public decimal Value { get; set; }
        public int? AccountType { get; set; }
        public string AccountId { get; set; }
    }
}

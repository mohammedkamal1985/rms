﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    [Table("ItemCategories")]
    public class ItemCategoryMetadata:MetadataBase
    {
        public Guid DomainId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    public class OrderMetadata:MetadataBase
    {
        public Guid DomainId { get; set; }
        public string Serial { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int CustomerId { get; set; }
        public CustomerMetadata Customer { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public decimal Total { get; set; }
        public decimal Net { get; set; }
        public decimal Paid { get; set; }
        public decimal Change { get; set; }
        public string Notes { get; set; }
        public AddressMetadata DeliveryAddress { get; set; }
        public int? DeliveryAddressId { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool? PayOnDelivery { get; set; }
        public Guid? SalesmanId { get; set; }
        public string MachineName { get; set; }
        public Guid? WarehouseId { get; set; }
        public ICollection<OrderLineMetadata> OrderLines { get; set; }
        public ICollection<OrderPaymentMetadata> Payments { get; set; }
    }
}

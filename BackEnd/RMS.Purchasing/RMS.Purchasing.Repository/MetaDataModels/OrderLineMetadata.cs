﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Abstract;

namespace RMS.Purchasing.Repository.MetaDataModels
{
    public class OrderLineMetadata:MetadataBase
    {
        public int ItemId { get; set; }
        public ItemMetadata LineItem { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public UnitMetadata Unit { get; set; }
        public int UnitId { get; set; }
        public int OrderId { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class PurchasingContext : DbContext
    {
        public DbSet<CustomerMetadata> Customers { get; set; }
        public DbSet<AddressMetadata> CustomerAddresses { get; set; }
        public DbSet<CustomerGroupMetadata> CustomerGroups { get; set; }
        public DbSet<UnitMetadata> Units { get; set; }
        public DbSet<ItemCategoryMetadata> ItemCategories { get; set; }
        public DbSet<ItemGroupMetadata> ItemGroups { get; set; }
        public DbSet<ItemUnitMetadata> ItemUnits { get; set; }
        public DbSet<ItemMetadata> Items { get; set; }
        public DbSet<OrderLineMetadata> OrderLines { get; set; }
        public DbSet<OrderPaymentMetadata> OrderPayments { get; set; }
        public DbSet<OrderMetadata> Orders { get; set; }
        public DbSet<StockMetadata> Stocks { get; set; }

        public PurchasingContext()
        {

        }
        public PurchasingContext(DbContextOptions<PurchasingContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("POS");
            modelBuilder.Entity<CustomerMetadata>().HasOne(c => c.Group).WithMany().HasForeignKey(c => c.GroupId);
            modelBuilder.Entity<AddressMetadata>().HasOne(a => a.Customer).WithMany(c => c.DeliveryAddresses).HasForeignKey(a => a.CustomerId);
            modelBuilder.Entity<ItemMetadata>().HasOne(i => i.Group).WithMany().HasForeignKey(i => i.GroupId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ItemMetadata>().HasOne(i => i.Category).WithMany().HasForeignKey(i => i.CategoryId).OnDelete(DeleteBehavior.Restrict); ;
            modelBuilder.Entity<ItemMetadata>().HasOne(i => i.DefaultUnit).WithMany().HasForeignKey(i => i.DefaultUnitId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ItemMetadata>().HasMany(i => i.AvailableUnits).WithOne(u => u.Item).HasForeignKey(i => i.ItemId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ItemUnitMetadata>().HasOne(iu => iu.Unit).WithMany().HasForeignKey(iu => iu.UnitId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<OrderMetadata>().HasOne(o => o.Customer).WithMany().HasForeignKey(o => o.CustomerId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<OrderMetadata>().HasOne(o => o.DeliveryAddress).WithMany().HasForeignKey(o => o.DeliveryAddressId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<OrderMetadata>().HasMany(o => o.OrderLines).WithOne().HasForeignKey(o => o.OrderId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<OrderLineMetadata>().HasOne(l => l.LineItem).WithMany().HasForeignKey(l => l.ItemId);
            modelBuilder.Entity<OrderLineMetadata>().HasOne(l => l.Unit).WithMany().HasForeignKey(l => l.UnitId);
            modelBuilder.Entity<OrderMetadata>().HasMany(o => o.Payments).WithOne().HasForeignKey(p => p.OrderId).OnDelete(DeleteBehavior.Cascade);
        }
    }

}

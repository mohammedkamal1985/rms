﻿using System;
using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ExceptionHandling.CustomException;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class StockQueryRepository : QueryRepositoryBase<PurchasingContext>, IStockQueryRepository
    {
        private IMapper _mapper;
        public StockQueryRepository(PurchasingContext context) : base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ItemsMetadataProfile>();
                cfg.AddProfile<StockMetadataProfile>();
            }).CreateMapper();
        }

        public StockDataModel GetByItemCode(string code)
        {
            try
            {
                var query = base.GetQueryable<StockMetadata>(i => i.Item.Code.ToLower().Equals(code), null, IncludeProperties);
                StockMetadata result;
                if (query == null || query.Count() == 0)
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<StockMetadata, StockDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        private IQueryable<StockMetadata> IncludeProperties(IQueryable<StockMetadata> query)
        {
            query = query.Include(i => i.Item)
                         .ThenInclude(i => i.AvailableUnits).ThenInclude(iu => iu.Unit).Include(i => i.Item.DefaultUnit);
            return query;
        }
    }
}

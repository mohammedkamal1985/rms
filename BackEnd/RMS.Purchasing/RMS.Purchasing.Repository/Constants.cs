﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.Repository
{
    public class Constants
    {

        //Customers procedures
        public const string CUSTOMER_GET_BY_NAME = "POS.CustomersGetByName";
        public const string CUSTOMER_GET_BY_PHONE = "POS.CustomersGetByPhone";
        public const string CUSTOMER_GET_DEFAULT = "POS.CustomersGetDefaultCustomer";
        public const string CUSTOMER_ADDRESS_GET_BY_CUSTOMER_ID = "POS.CustomerAddressGetByGustomerId";
        public const string CUSTOMER_GET_BY_ID = "POS.CustomersSelect";
        public const string CUSTOMER_GET_BY_DOMAIN_ID = "POS.CustomersSelectByDomainId";
        public const string CUSTOMER_GET_ALL = "POS.CustomersSelect";

        //Items Procedures
        public const string ITEM_GET_BY_NAME = "POS.ItemGetByName";
        public const string ITEM_GET_BY_CODE = "POS.ItemGetByCode";
        public const string ITEM_GET_UNITS = "POS.ItemsGetUnitsByItemId";
        public const string ITEM_CATEGORY_GET = "POS.ItemCategoriesSelect";
        public const string ITEM_GROUP_GET = "POS.ItemGroupsSelect";
        public const string ITEM_GET_BY_ID = "POS.ItemSelect";
        public const string ITEM_GET_BY_DOMAIN_ID = "POS.ItemSelectByDomainId";

        //Orders Procedures
        public const string ORDER_INSERT = "POS.OrdersInsert";
        public const string ORDER_GET_ALL = "POS.OrdersSelect";
        public const string ORDER_GET_BY_ID = "POS.OrdersSelect";
        public const string ORDER_UPDATE = "POS.OrdersUpdate";
        public const string ORDER_DELETE = "POS.OrdersDelete";

        //OrderLine Procedures
        public const string ORDER_LINE_INSERT = "[POS].[OrderLinesInsert]";
        public const string ORDER_LINE_GET_ALL = "[POS].[OrderLinesSelect]";
        public const string ORDER_LINE_GET_BY_ID = "[POS].[OrderLinesSelect]";
        public const string ORDER_LINE_UPDATE = "[POS].[OrderLinesUpdate]";
        public const string ORDER_LINE_DELETE = "[POS].[OrderLinesDelete]";
        public const string ORDER_LINE_GET_BY_ORDER_ID = "[POS].[OrderLinesSelectByOrderId]";
        public const string ORDER_LINE_GET_BY_ITEM_ID = "[POS].[OrderLinesSelectByItemId]";

    }
}

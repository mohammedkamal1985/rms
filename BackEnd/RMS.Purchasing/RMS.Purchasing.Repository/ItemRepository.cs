﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using AutoMapper;
using ExceptionHandling.CustomException;
using Microsoft.EntityFrameworkCore;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class ItemRepository : QueryRepositoryBase<PurchasingContext>, IItemRepository
    {
        IMapper _mapper;
        public ItemRepository(PurchasingContext context) : base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ItemsMetadataProfile>();
                cfg.AddProfile<StockMetadataProfile>();
            }).CreateMapper();
        }

        public ItemDataModel FindByDomainId(Guid domainId)
        {
            try
            {
                var query = base.GetQueryable<ItemMetadata>(i => i.DomainId == domainId, null, IncludeProperties);
                ItemMetadata result;
                if (query == null || !query.Any())
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<ItemDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public ItemDataModel GetByCode(string code)
        {
            try
            {
                var query = base.GetQueryable<ItemMetadata>(i => i.Code.ToLower().Equals(code), null, IncludeProperties);
                ItemMetadata result;
                if (query == null || query.Count() == 0)
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<ItemDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public List<ItemDataModel> GetByName(string name)
        {
            try
            {
                var result = base.GetQueryable<ItemMetadata>(i => i.Name.ToLower().Contains(name), null, IncludeProperties);
                return _mapper.Map<List<ItemDataModel>>(result?.ToList());
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public List<PriceListDataModel> GetPrices(string itemCode)
        {
            return null;
        }

        private IQueryable<ItemMetadata> IncludeProperties(IQueryable<ItemMetadata> query)
        {
            query = query.Include(i => i.Category)
                  .Include(i => i.Group)
                  .Include(i => i.DefaultUnit)

                  .Include(i => i.AvailableUnits).ThenInclude(iu => iu.Unit);
            return query;
        }
    }
}

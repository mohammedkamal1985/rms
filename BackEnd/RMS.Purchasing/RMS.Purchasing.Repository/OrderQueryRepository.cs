﻿using System;
using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ExceptionHandling.CustomException;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class OrderQueryRepository : QueryRepositoryBase<PurchasingContext>,IOrderQueryRepository
    {
        IMapper _mapper;
        public OrderQueryRepository(PurchasingContext context) : base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<OrderMetadataProfile>();
                cfg.AddProfile<CustomerMetadataProfile>();
                cfg.AddProfile<ItemsMetadataProfile>();
            }).CreateMapper();
        }

        public OrderDataModel FindByDomainId(Guid domainId)
        {
            try
            {
                var query = base.GetQueryable<OrderMetadata>(o => o.DomainId == domainId,null, IncludeProperties);
                var result = query.SingleOrDefault(i => i.DomainId.Equals(domainId));
                return _mapper.Map<OrderDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        private IQueryable<OrderMetadata> IncludeProperties(IQueryable<OrderMetadata> query)
        {
            query = query.Include(i => i.Customer)
                   .Include(i => i.DeliveryAddress)
                   .Include(i => i.OrderLines)
                        .ThenInclude(l => l.LineItem)
                   .Include(i => i.OrderLines)
                        .ThenInclude(l => l.Unit);
            return query;
        }
    }
}

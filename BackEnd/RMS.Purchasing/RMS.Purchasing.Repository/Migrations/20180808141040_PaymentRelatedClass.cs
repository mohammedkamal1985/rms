﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RMS.Purchasing.Repository.Migrations
{
    public partial class PaymentRelatedClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderLines_Items_LineItemId",
                schema: "POS",
                table: "OrderLines");

            migrationBuilder.DropIndex(
                name: "IX_OrderLines_LineItemId",
                schema: "POS",
                table: "OrderLines");

            migrationBuilder.DropColumn(
                name: "LineItemId",
                schema: "POS",
                table: "OrderLines");

            migrationBuilder.CreateTable(
                name: "OrderPayments",
                schema: "POS",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OrderId = table.Column<int>(nullable: false),
                    PaymentType = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(nullable: false),
                    AccountType = table.Column<int>(nullable: true),
                    AccountId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderPayments_Orders_OrderId",
                        column: x => x.OrderId,
                        principalSchema: "POS",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_ItemId",
                schema: "POS",
                table: "OrderLines",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderPayments_OrderId",
                schema: "POS",
                table: "OrderPayments",
                column: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLines_Items_ItemId",
                schema: "POS",
                table: "OrderLines",
                column: "ItemId",
                principalSchema: "POS",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderLines_Items_ItemId",
                schema: "POS",
                table: "OrderLines");

            migrationBuilder.DropTable(
                name: "OrderPayments",
                schema: "POS");

            migrationBuilder.DropIndex(
                name: "IX_OrderLines_ItemId",
                schema: "POS",
                table: "OrderLines");

            migrationBuilder.AddColumn<int>(
                name: "LineItemId",
                schema: "POS",
                table: "OrderLines",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_LineItemId",
                schema: "POS",
                table: "OrderLines",
                column: "LineItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLines_Items_LineItemId",
                schema: "POS",
                table: "OrderLines",
                column: "LineItemId",
                principalSchema: "POS",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

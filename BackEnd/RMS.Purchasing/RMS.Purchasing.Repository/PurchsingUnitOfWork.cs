﻿using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using Repo = Repository.Base.Sql.EntityFramework;

namespace RMS.Purchasing.Repository
{
    public class PurchasingUnitOfWork : Repo.Abstract.UnitOfWorkBase<PurchasingContext>, IPurchasingUnitOfWork
    {

        public ICustomerRepository CustomerRepository { get; }
        public IItemRepository ItemRepository { get; }
        public IOrderQueryRepository OrderQueryRepository { get; }
        public IOrderWriteRepository OrderWriteRepository { get; }
        public IOrderLineWriteRepository OrderLineWriteRepository { get; }
        public IStockQueryRepository StockQueryRepository { get; }
        public IStockWriteRepository StockWriteRepository { get; }

        public PurchasingUnitOfWork(PurchasingContext dbContext,
            ICustomerRepository customerRepository,
            IItemRepository itemRepository,
            IOrderQueryRepository orderQueryRepository,
            IOrderWriteRepository orderWriteRepository,
            IOrderLineWriteRepository orderLineWriteRepository,
            IStockQueryRepository stockQueryRepository,
            IStockWriteRepository stockWriteRepository
            ) : base(dbContext)
        {
            CustomerRepository = customerRepository;
            ItemRepository = itemRepository;
            OrderQueryRepository = orderQueryRepository;
            OrderWriteRepository = orderWriteRepository;
            OrderLineWriteRepository = orderLineWriteRepository;
            StockQueryRepository = stockQueryRepository;
            StockWriteRepository = stockWriteRepository;
        }
    }
}

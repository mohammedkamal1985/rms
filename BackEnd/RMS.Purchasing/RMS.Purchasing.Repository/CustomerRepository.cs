﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Logging.Infrastructure.Interfaces;
using ExceptionHandling.CustomException;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class CustomerRepository : QueryRepositoryBase<PurchasingContext>, ICustomerRepository
    {
        IMapper _mapper;
        //ILogger _logger;
        public CustomerRepository(PurchasingContext context) : base(context)
        {
            //_logger = logger;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CustomerMetadataProfile>();
            }).CreateMapper();
        }

        public CustomerDataModel FindByDomainId(Guid domainId)
        {
            try
            {
                var query = base.GetQueryable<CustomerMetadata>(c => c.DomainId == domainId, null, IncludeProperties);
                CustomerMetadata result;
                if (query == null || query.Count() == 0)
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<CustomerMetadata, CustomerDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public List<CustomerDataModel> GetByName(string name)
        {
            try
            {
                var result = base.GetQueryable<CustomerMetadata>(c => c.Name.ToLower().Contains(name) && !c.IsDefault, null, IncludeProperties);
                return _mapper.Map<List<CustomerMetadata>, List<CustomerDataModel>>(result.ToList());
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public CustomerDataModel GetByPhoneNumber(string phoneNumber)
        {
            try
            {
                var query = base.GetQueryable<CustomerMetadata>(c => c.PhoneNumber.ToLower().Equals(phoneNumber) && !c.IsDefault, null, IncludeProperties);
                CustomerMetadata result;
                if (query == null || query.Count() ==0)
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<CustomerMetadata, CustomerDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public CustomerDataModel GetDefaultCustomer()
        {
            try
            {
                var query = base.GetQueryable<CustomerMetadata>(c => c.IsDefault, null, IncludeProperties);
                CustomerMetadata result;
                if (query == null || query.Count() == 0)
                    result = null;
                else
                    result = query.First();
                return _mapper.Map<CustomerMetadata, CustomerDataModel>(result);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        private IQueryable<CustomerMetadata> IncludeProperties(IQueryable<CustomerMetadata> query)
        {
            query = query.Include(c => c.DeliveryAddresses).Include(c => c.Group);
            return query;
        }
    }
}

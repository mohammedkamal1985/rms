﻿using AutoMapper;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository.MappingProfiles
{
    public class OrderMetadataProfile:Profile
    {
        public OrderMetadataProfile()
        {
            CreateMap<OrderMetadata, OrderDataModel>().ReverseMap();
            CreateMap<OrderLineMetadata, OrderLineDataModel>().ReverseMap();
        }
    }
}

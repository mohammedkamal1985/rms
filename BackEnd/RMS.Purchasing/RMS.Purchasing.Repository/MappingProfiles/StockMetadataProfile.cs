﻿using AutoMapper;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository.MappingProfiles
{
    public class StockMetadataProfile : Profile
    {
        public StockMetadataProfile()
        {
            CreateMap<StockDataModel, StockMetadata>().ReverseMap();
        }
    }
}

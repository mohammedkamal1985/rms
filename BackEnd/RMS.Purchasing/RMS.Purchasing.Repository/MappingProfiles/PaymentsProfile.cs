﻿using AutoMapper;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository.MappingProfiles
{
    public class PaymentsProfile:Profile
    {
        public PaymentsProfile()
        {
            CreateMap<OrderPaymentDataModel, OrderPaymentMetadata>().ReverseMap();
        }
    }
}

﻿using AutoMapper;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository.MappingProfiles
{
    public class CustomerMetadataProfile:Profile
    {
        public CustomerMetadataProfile()
        {
            //Business Model ==> Domain Entity
            CreateMap<AddressMetadata, AddressDataModel>().ReverseMap();

            CreateMap<CustomerMetadata, CustomerDataModel>().ReverseMap();

            CreateMap<CustomerGroupMetadata, CustomerGroupDataModel>().ReverseMap(); 
        }
    }
}

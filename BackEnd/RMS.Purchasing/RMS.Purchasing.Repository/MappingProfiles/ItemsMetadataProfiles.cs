﻿using AutoMapper;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository.MappingProfiles
{
    public class ItemsMetadataProfile:Profile
    {
        public ItemsMetadataProfile()
        {
            CreateMap<ItemGroupMetadata, ItemGroupDataModel>().ReverseMap();
            CreateMap<ItemCategoryMetadata, ItemCategoryDataModel>().ReverseMap();
            CreateMap<UnitMetadata, UnitDataModel>().ReverseMap();
            CreateMap<ItemUnitMetadata, ItemUnitDataModel>().ReverseMap();
            CreateMap<ItemMetadata, ItemDataModel>().ReverseMap();
        }
    }
}

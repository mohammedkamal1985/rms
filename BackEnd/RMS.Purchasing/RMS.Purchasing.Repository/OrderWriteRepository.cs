﻿using System;
using System.Data.Common;
using AutoMapper;
using ExceptionHandling.CustomException;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class OrderWriteRepository : WriteRepositoryBase<PurchasingContext, OrderMetadata,OrderDataModel>, IOrderWriteRepository
    {
        IMapper _mapper;
        public OrderWriteRepository(PurchasingContext context):base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<OrderMetadataProfile>();
                cfg.AddProfile<CustomerMetadataProfile>();
                cfg.AddProfile<ItemsMetadataProfile>();
            }).CreateMapper();
        }

        public override OrderDataModel Add(OrderDataModel entity)
        {
            try
            { 
            return base.Add(entity);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public override OrderDataModel Update(OrderDataModel entity)
        {
            try
            {
                return base.Update(entity);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        protected override OrderMetadata ConvertDataModelToMetaData(OrderDataModel entity)
        {
           return _mapper.Map<OrderMetadata>(entity);
        }

        protected override OrderDataModel ConvertMetaDataToDataModel(OrderMetadata metadata)
        {
            return _mapper.Map<OrderDataModel>(metadata);
        }
    }
}

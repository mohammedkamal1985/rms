﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RMS.Purchasing.Repository
{
    public class PurchasingContextFactory : IDesignTimeDbContextFactory<PurchasingContext>
    {
        public PurchasingContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<PurchasingContext>();
            
            optionsBuilder.UseSqlServer("Server=localhost\\SQLSERVER2017;Database=RMS;Trusted_Connection=True;MultipleActiveResultSets=true");

            return new PurchasingContext(optionsBuilder.Options);
        }
    }
}

﻿using System;
using System.Data.Common;
using AutoMapper;
using ExceptionHandling.CustomException;
using Repository.Base.Sql.EntityFramework.Abstract;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.Repository.MappingProfiles;
using RMS.Purchasing.Repository.MetaDataModels;

namespace RMS.Purchasing.Repository
{
    public class OrderLineWriteRepository : WriteRepositoryBase<PurchasingContext, OrderLineMetadata, OrderLineDataModel>,IOrderLineWriteRepository
    {
        IMapper _mapper;

        public OrderLineWriteRepository(PurchasingContext context) : base(context)
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<OrderMetadataProfile>();
                cfg.AddProfile<CustomerMetadataProfile>();
                cfg.AddProfile<ItemsMetadataProfile>();
            }).CreateMapper();
        }

        public override OrderLineDataModel Add(OrderLineDataModel entity)
        {
            try
            {
                return base.Add(entity);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        public override OrderLineDataModel Update(OrderLineDataModel entity)
        {
            try
            {
                return base.Update(entity);
            }
            catch (DbException dbException)
            {
                throw new RepositoryException(dbException.Message, dbException);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }
        protected override OrderLineMetadata ConvertDataModelToMetaData(OrderLineDataModel entity)
        {
            return _mapper.Map<OrderLineMetadata>(entity);
        }

        protected override OrderLineDataModel ConvertMetaDataToDataModel(OrderLineMetadata metadata)
        {
            return _mapper.Map<OrderLineDataModel>(metadata);
        }
    }
}

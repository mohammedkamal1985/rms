﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExceptionHandling.CustomException;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.DomainService.Infrastructure;

namespace RMS.Purchasing.DomainService
{
    public class PurchasingDomainService : IPurchasingDomainService
    {
        private decimal ConvertUnits(decimal fromQuantity, decimal factor)
        {
            return fromQuantity * factor;
        }

        public decimal ConvertQuantityToItemDefaultUnitQuantity(decimal quantity, Unit unit, Item item)
        {
            if (unit == null || item == null)
            {
                //TODO LOG & Handle DatabaseException
                throw new BusinessException();
            }

            if (item.AvailableUnits.ContainsKey(unit) && item.AvailableUnits[unit] > 0)
            {
                //Convert From Unit to Default Unit and Calculate Price
                return ConvertUnits(quantity, item.AvailableUnits[unit]);
            }

            //TODO LOG & Handle DatabaseException
            throw new BusinessException();
        }

        /// <summary>
        ///This method to calculate the price of a single unit of the Unit in the parameter (not the order line price)
        /// </summary>
        /// <param name="item">item to calculate price for</param>
        /// <param name="unit">unit to calculate price for</param>
        /// <param name="priceLists">List of prices used in calculations</param>
        /// <param name="orderType"></param>
        /// <param name="orderCustomer"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        /// <exception cref="BusinessException"></exception>
        public decimal CalculateItemPrice(Item item, Unit unit, List<PriceList> priceLists,
            OrderTypes orderType, Customer orderCustomer, decimal quantity = 1)
        {
            if (item == null || unit == null || orderCustomer == null || !item.AvailableUnits.ContainsKey(unit))
            {
                //TODO LOG & Handle DatabaseException
                throw new BusinessException();
            }

            //If no available price lists
            if (priceLists == null || priceLists.Count == 0)
            {
                //If used unit is the item's default unit, then use the default price
                if (unit == item.DefaultUnit)
                    return item.DefaultPrice;

                //otherwise convert used unit to item's default unit and multiply by default price 
                return ConvertQuantityToItemDefaultUnitQuantity(quantity, unit, item) * item.DefaultPrice;
            }

            //Get valid price lists with active date range ordered by PriorityIndex
            priceLists = GetValidPriceLists(priceLists, orderCustomer, item, orderType).ToList();

            if (priceLists.Count == 0)
            {
                //If no valid price lists with active date range, do recursion call with the price list list == null
                //This will execute code in the upper lines and use default item price 
                return CalculateItemPrice(item, unit, null, orderType, orderCustomer, quantity);
            }

            //select price from lists that has the correct item and the item unit, order them by the priority index and then select the first price
             var targetPrice = priceLists
                 .SelectMany(pl => pl.Prices)
                 .FirstOrDefault(p => p.ItemId == item.Id && p.UnitId == unit.Id);
             if (targetPrice != null)
                 return targetPrice.Price;

            //Loop Lists to Get Price for Item with different Unit
            foreach (var price in priceLists.SelectMany(pl => pl.Prices).Where(p => p.ItemId == item.Id ))
            {
                //Get the unit used in the price list
                var priceListUnitId = price.UnitId;

                //If the unit used in the price list is NOT one of the Item unit list
                if (item.AvailableUnits.Keys.All(u => u.Id != priceListUnitId))
                {
                    continue;
                }
                
                var priceListUnit = item.AvailableUnits.SingleOrDefault(e => e.Key.Id == priceListUnitId).Key;
               
                //Price of item's default unit according to the price list
                var itemDefaultUnitPriceList = price.Price *
                                               (1 / item.AvailableUnits[priceListUnit]);

                //Convert used unit to item's default unit
                var quantityInDefaultUnit = ConvertUnits(quantity, item.AvailableUnits[unit]);
                
                return quantityInDefaultUnit * itemDefaultUnitPriceList;
            }

            //TODO LOG & Handle DatabaseException
            throw new BusinessException();
        }

        private IEnumerable<PriceList> GetValidPriceLists(IEnumerable<PriceList> lists, Customer orderCustomer, Item item,
            OrderTypes orderType)
        {
            //validate Price Lists against its type
            var validLists = lists.Where(list => IsValidPriceList(list, orderCustomer, item, orderType)).ToList();
           
            //validate Price Lists against date
            validLists = validLists
                .Where(l => l.StartDate.Date <= DateTime.Now.Date && l.EndDate.Date >= DateTime.Now.Date).ToList();
            return validLists.OrderByDescending(pl=>pl.PriorityIndex);
        }

        private bool IsValidPriceList(PriceList list, Customer orderCustomer, Item item, OrderTypes orderType)
        {
            switch (list.Type)
            {
                //If a Price List set for specific customer which is not the current customer go for the next one
                case PriceListTypes.Customer when !list.TypeValue.Equals(orderCustomer.Id.ToString()):
                    return false;
                //If a price List set for specific Customer Group and the Customer does not belong to the Group go to the next one
                case PriceListTypes.CustomerGroup when !list.TypeValue.Equals(orderCustomer.Group.Id.ToString()):
                    return false;
                //If a price list set for specific Item Group and the Item is not in the Group Go to next one 
                case PriceListTypes.ItemGroup when !list.TypeValue.Equals(item.Group.Id.ToString()):
                    return false;
                //If a price list set for specific Order Type and the order type is different Go to next one 
                case PriceListTypes.OrderType when !list.TypeValue.Equals(orderType.ToString()):
                    return false;
                case PriceListTypes.All:
                    return true;
                default:
                    return true;
            }
        }

        public string GenerateOrderSerial()
        {
            var sb = new StringBuilder(DateTime.Now.DayOfYear.ToString("D3"));
            sb.Append(DateTime.Now.Year.ToString());
            sb.Append(new Random().Next(1000000).ToString("D6"));
            return sb.ToString();
        }
    }
}
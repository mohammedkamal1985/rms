﻿using System;
using RMS.SharedKernel.Core.ValueTypes;

namespace RMS.Purchasing.Core.Interfaces
{
    public interface IDeliveryOrder:IPurchasingOrder
    {
        Address DeliveryAddress { get; }
        DateTime? DeliveryDate { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Core.ValueTypes.Payment;

namespace RMS.Purchasing.Core.Interfaces
{
    public interface IPurchasingOrder
    {
         string Serial { get; }
         DateTime CreateDate { get; }
         DateTime? CloseDate { get; }
         Customer Customer { get; }
         OrderStatus Status { get; }
         decimal Total { get; }
         decimal Discount { get; }
         decimal Net { get; }
         decimal Paid { get; }
         decimal Change { get; }
         string Notes { get; }
         IReadOnlyCollection<OrderLine> OrderLines { get; }
         IReadOnlyCollection<PaymentBase> Payments { get; }
         Guid SalesmanId { get; }
         string MachineName { get; }
         Guid WarehouseId { get; }
         OrderTypes OrderType { get; }
    }
}

﻿using System;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class ItemCategory : Entity<Guid>
    {
        #region CONSTRUCTORS

        public ItemCategory(Guid categoryId, string name, string code) : base(categoryId)
        {
            Name = name;
            Code = code;
        }

        #endregion

        #region PROPERTIES

        public string Name { get; }
        public string Code { get; }

        #endregion

        #region FUNCTIONS

        public override bool IsValid()
        {
            return true;
        }

        #endregion
    }
}
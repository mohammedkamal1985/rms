﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RMS.Purchasing.Core.Enums;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class PriceList : Entity<Guid>
    {
        public PriceList(DateTime startDate, DateTime endDate, int priority,
            PriceListTypes type, string typeValue, ReadOnlyCollection<ItemPrice> prices)
        {
            StartDate = startDate;
            EndDate = endDate;
            PriorityIndex = priority;
            Type = type;
            Prices = prices;
            TypeValue = typeValue;
        }

        #region PROPERTIES

        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
        public int PriorityIndex { get; }
        public PriceListTypes Type { get; }
        public string TypeValue { get; }
        public ReadOnlyCollection<ItemPrice> Prices { get; }

        #endregion

        #region FUNCTIONS

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }

        #endregion
    }
}
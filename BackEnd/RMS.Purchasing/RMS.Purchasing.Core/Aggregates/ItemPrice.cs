﻿using System;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class ItemPrice : Entity<Guid>
    {
        public ItemPrice(Guid itemId,Guid unitId,decimal price)
        {
            ItemId = itemId;
            UnitId = unitId;
            Price = price;
        }

        #region PROPERTIES
        public Guid ItemId { get; }
        public Guid UnitId { get; }
        public decimal Price { get; }
        #endregion
        
        #region FUNCTIONS
        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }
        #endregion
    }
}

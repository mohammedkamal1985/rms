﻿using System;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class CustomerGroup : Entity<Guid>
    {
        #region COUNSTRUCTORS

        public CustomerGroup(Guid domainId, string code, string name) : base(domainId)
        {
            Code = code;
            Name = name;
        }

        #endregion

        #region PROPERTIES

        public string Name { get; }
        public string Code { get; }

        #endregion

        #region FUNCTIONS

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }

        #endregion
    }
}
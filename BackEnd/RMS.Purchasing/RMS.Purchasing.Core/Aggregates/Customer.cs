﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RMS.SharedKernel.Core.ValueTypes;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class Customer : Entity<Guid>
    {
        #region PROPERTIES
        public ReadOnlyCollection<Address> DeliveryAddresses { get; }
        public string Name { get; }
        public string PhoneNumber { get; }
        public bool IsDefault { get; }
        public CustomerGroup Group { get; }
        #endregion

        #region CONSTRUCTORS

        public Customer(ReadOnlyCollection<Address> deliveryAddresses, Guid customerId,
            string name, string phoneNumber, bool isDefault, CustomerGroup group) : base(customerId)
        {
            DeliveryAddresses = deliveryAddresses;
            Name = name;
            PhoneNumber = phoneNumber;
            IsDefault = isDefault;
            Group = group;
        }

        #endregion

        #region FUNCTIONS
        public override bool IsValid()
        {
            ValidationErrors.Clear();
            var isValid = true;
            if (string.IsNullOrEmpty(Name))
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidCustomerName);
            }

            if (string.IsNullOrEmpty(PhoneNumber) && !IsDefault)
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidCustomerPhone);
            }

            //TODO Validate the Phone Number Fomate
            return isValid;
        }
        public Customer AddAddress(Address newAddress)
        {
            var newAddressList = new ReadOnlyCollection<Address>(new List<Address>(DeliveryAddresses) {newAddress});
            return new Customer(newAddressList, Id, Name, PhoneNumber, IsDefault, Group);
        }
        #endregion
    }
}
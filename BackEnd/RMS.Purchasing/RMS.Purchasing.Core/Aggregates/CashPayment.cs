﻿using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Interfaces;

namespace RMS.Purchasing.Core.ValueTypes.Payment
{
    public class CashPayment : PaymentBase
    {
        public CashPayment(decimal value, IPurchasingOrder order) : base(value, order)
        {
            Type = Enums.PaymentTypes.Cash;
        }

        public override bool IsValid()
        {
            if (Value != 0)
                return false;
            AddValidationError(Resources.Msg_InvalidPaymentValue);

            return false;
        }
    }
}

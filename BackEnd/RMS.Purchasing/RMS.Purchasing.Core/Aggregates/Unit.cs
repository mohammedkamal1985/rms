﻿using System;
using System.Collections.Generic;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class Unit: Entity<Guid>
    {
        public Unit(Guid unitId, string name,string code):base(unitId)
        {
            Name = name;
            Code = code;
        }
        public string Name { get; }
        public string Code { get; }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.Interfaces;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Core.ValueTypes.Payment;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class PurchasingOrder : AggregateRoot<Guid>, IPurchasingOrder
    {
        #region PROPERTIES

        public string Serial { get; }
        public DateTime CreateDate { get; }
        public DateTime? CloseDate { get; }
        public Customer Customer { get; }
        public OrderStatus Status { get; }
        public decimal Total => OrderLines.Sum(e => e.Quantity.Value * e.UnitPrice);
        public decimal Discount { get; }
        public decimal Net => Total - Discount;
        public decimal Paid => Payments.Sum(p => p.Value);
        public decimal Change => Paid - Net;
        public string Notes { get; }
        public IReadOnlyCollection<OrderLine> OrderLines { get; }
        public IReadOnlyCollection<PaymentBase> Payments { get; }
        public Guid SalesmanId { get; }
        public string MachineName { get; }
        public Guid WarehouseId { get; }
        public virtual OrderTypes OrderType => OrderTypes.Purchasing;
        #endregion

        #region CONSTRUCTOR

        public PurchasingOrder(string serial, DateTime createDate, DateTime? closeDate,
            Customer customer, OrderStatus status, decimal discount, string notes,
            IReadOnlyCollection<OrderLine> orderLines, IReadOnlyCollection<PaymentBase> orderPayments,
            Guid salesmanId, string machineName, Guid warehouseId)
        {
            Serial = serial;
            CreateDate = createDate;
            CloseDate = closeDate;
            Customer = customer;
            Status = status;
            Discount = discount;
            Notes = notes;
            OrderLines = orderLines;
            MachineName = machineName;
            WarehouseId = warehouseId;
            Payments = orderPayments;
            SalesmanId = salesmanId;
        }

        #endregion

        #region VALIDATIONS

        protected bool IsValidForCreation()
        {
            ValidationErrors.Clear();
            var isValid = true;

            if (Customer == null || !Customer.IsValid())
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderCustomerData);
            }

            if (string.IsNullOrEmpty(Serial))
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderSerialNumber);
            }

            if (string.IsNullOrEmpty(MachineName))
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderMachineName);
            }

            if (SalesmanId == Guid.Empty)
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderSalesmanId);
            }

            if (WarehouseId == Guid.Empty)
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderWarehouseId);
            }

            return isValid;
        }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            var isValid = IsValidForCreation();

            if (OrderLines == null || OrderLines.Count == 0)
            {
                isValid = false;
                AddValidationError(Resources.Msg_InvalidOrderLine);
            }

            return isValid;
        }

        protected bool IsValidForHold()
        {
            ValidationErrors.Clear();
            var isValid = IsValid();
            return isValid;
        }

        protected bool IsValidForClose()
        {
            ValidationErrors.Clear();
            var isValid = IsValid();
            
            if (Payments.Sum(p => p.Value) != Net)
            {
                isValid = false;
                ValidationErrors.Add(new ValidationError(Resources.Msg_InvalidOrderPaymentNotEqualsToNet));
            }
            return isValid;
        }

        #endregion

        #region FUNCTIONS

        public PurchasingOrder WithSerialNumber(string serial)
        {
            return new PurchasingOrder(serial, CreateDate, CloseDate, Customer, Status,
                Discount, Notes, OrderLines, Payments, SalesmanId, MachineName, WarehouseId);
        }

        public PurchasingOrder Create()
        {
            if (IsValidForCreation())
            {
                return new PurchasingOrder(Serial, DateTime.Now, CloseDate, Customer, OrderStatus.New, Discount, Notes,
                    new ReadOnlyCollection<OrderLine>(new List<OrderLine>()),
                    new ReadOnlyCollection<PaymentBase>(new List<PaymentBase>()),
                    SalesmanId, MachineName, WarehouseId);
            }

            return this;
        }

        public PurchasingOrder Close()
        {
            if (IsValidForClose())
            {
                return new PurchasingOrder(Serial, CreateDate, DateTime.Now, Customer,
                    OrderStatus.Closed, Discount, Notes, OrderLines, Payments, SalesmanId,
                    MachineName, WarehouseId);
            }

            return this;
        }

        public PurchasingOrder Cancel()
        {
            return new PurchasingOrder(Serial, CreateDate, DateTime.Now, Customer,
                OrderStatus.Canceled, Discount, Notes, OrderLines, Payments, SalesmanId,
                MachineName, WarehouseId);
        }

        public PurchasingOrder Hold()
        {
            if (IsValidForHold())
            {
                return new PurchasingOrder(Serial, CreateDate, CloseDate, Customer,
                    OrderStatus.Hold, Discount, Notes, OrderLines, Payments, SalesmanId,
                    MachineName, WarehouseId);
            }

            return this;
        }

        #endregion

        #region ORDER LINES ACTIONS

        public virtual PurchasingOrder AddOrderLine(OrderLine newOrderLine)
        {
            var orderLines = OrderLines.ToList();
            //1. Check if the order already has an order line with the same item and unit
            var orderLine = orderLines.SingleOrDefault(l =>l.LineItem.Equals(newOrderLine.LineItem) && l.Quantity.Unit.Equals(newOrderLine.Quantity.Unit));
            
            if (orderLine != null)
            {
                //1.1. if order already has order line with same item and unit, then increase this order line quantity 
                orderLine.IncreaseQuantity(newOrderLine.Quantity.Value);
            }
            else
            {
                //1.2. if order line is newly added, then validate and add.
                if (!newOrderLine.IsValid())
                {
                    AddValidationErrors(newOrderLine.ValidationErrors);
                    return this;
                }

                orderLines.Add(newOrderLine);
            }

            return new PurchasingOrder(Serial, CreateDate, CloseDate, Customer, Status, Discount, Notes,
                new ReadOnlyCollection<OrderLine>(orderLines), Payments, SalesmanId,MachineName,WarehouseId);
        }

        public virtual PurchasingOrder UpdateOrderLine(OrderLine modifiedOrderLine)
        {
            var orderLines = OrderLines.ToList();
            var orderLine = orderLines.SingleOrDefault(l =>
                l.LineItem.Equals(modifiedOrderLine.LineItem) &&
                l.Quantity.Unit.Equals(modifiedOrderLine.Quantity.Unit));
            if (orderLine != null)
            {
                //orderLines.IncreaseQuantity(newOrderLine.Quantity.Value);
            }

            return new PurchasingOrder(Serial, CreateDate, CloseDate, Customer, Status, Discount, Notes,
                new ReadOnlyCollection<OrderLine>(orderLines), Payments, SalesmanId,MachineName,WarehouseId);
        }

        public virtual PurchasingOrder RemoveOrderLine(OrderLine line)
        {
            var orderLines = OrderLines.ToList();
            var orderLine = orderLines.SingleOrDefault(l =>
                l.LineItem.Equals(line.LineItem) && l.Quantity.Unit.Equals(line.Quantity.Unit));
            if (orderLine != null)
            {
                orderLines.Remove(orderLine);
            }

            return new PurchasingOrder(Serial, CreateDate, CloseDate, Customer, Status, Discount, Notes,
                new ReadOnlyCollection<OrderLine>(orderLines), Payments, SalesmanId,MachineName,WarehouseId);
        }

        #endregion

        #region PAYMENTS

        public PurchasingOrder AddPayment(PaymentBase newPayment)
        {
            if (!newPayment.IsValid())
            {
                AddValidationErrors(newPayment.ValidationErrors);
                return this;
            }

            var payments = Payments.ToList();
            payments.Add(newPayment);
            return new PurchasingOrder(Serial, CreateDate, CloseDate, Customer, Status, Discount, Notes,
                OrderLines, new ReadOnlyCollection<PaymentBase>(payments), SalesmanId,MachineName,WarehouseId);
        }

        #endregion Payments

        #region Events

        #endregion
    }
}
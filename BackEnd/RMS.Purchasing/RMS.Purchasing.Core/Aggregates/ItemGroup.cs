﻿using System;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class ItemGroup : Entity<Guid>
    {
        public ItemGroup()
        {

        }
        public ItemGroup(Guid groupId, string name, string code) : base(groupId)
        {
            Name = name;
            Code = code;
        }
        public string Name { get; }
        public string Code { get; }

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }
    }
}

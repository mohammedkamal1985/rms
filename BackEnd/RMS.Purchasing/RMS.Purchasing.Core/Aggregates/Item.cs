﻿using System;
using System.Collections.Generic;
using System.Linq;
using RMS.Purchasing.Core.ValueTypes;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Aggregates
{
    public class Item : Entity<Guid>
    {
        #region CONSTRUCTORS

        public Item(Guid id, string code, string name,
            Dictionary<Unit, decimal> itemUnits,
            Unit defaultUnit, decimal defaultPrice,
            ItemCategory category, ItemGroup group,
            Stock stock = null)
            : base(id)
        {
            Code = code;
            Name = name;
            AvailableUnits = itemUnits;
            DefaultUnit = defaultUnit;
            DefaultPrice = defaultPrice;
            Stock = stock;
            Category = category;
            Group = group;
        }

        #endregion

        #region PROPERTIES

        public string Code { get; }
        public string Name { get; }
        public Unit DefaultUnit { get; }
       
        //TO DO, replace with Custom Type Unit and conversion factor
        public Dictionary<Unit, decimal> AvailableUnits { get; }
        public ItemCategory Category { get; }
        public ItemGroup Group { get; }
        public decimal DefaultPrice { get; }
        public Stock Stock { get; }

        #endregion

        #region FUNCTIONS

        public override bool IsValid()
        {
            ValidationErrors.Clear();
            return true;
        }

        #endregion
    }
}
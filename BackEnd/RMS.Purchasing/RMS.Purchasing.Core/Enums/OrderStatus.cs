﻿namespace RMS.Purchasing.Core.Enums
{
    public enum OrderStatus
    {
        New,
        Submitted,
        Canceled,
        Hold,
        OnWay,
        Delivered,
        Closed
    }
}
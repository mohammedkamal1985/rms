﻿namespace RMS.Purchasing.Core.Enums
{
    public enum CreditCardTypes
    {
        MasterCard = 1,
        VisaCard = 2
    }
}

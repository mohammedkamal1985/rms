﻿namespace RMS.Purchasing.Core.Enums
{
    public enum TransactionType
    {
        Withdraw,
        Return,
        Deposit,
        Adjustment
    }
}

﻿namespace RMS.Purchasing.Core.Enums
{
    public enum PriceListTypes
    {
        Customer = 1,
        CustomerGroup = 2,
        ItemGroup = 3,
        OrderType = 4,
        All = 5
    }
}
﻿namespace RMS.Purchasing.Core.Enums
{
    public enum PaymentTypes
    {
        Cash=1,
        Voucher=2,
        CreditCard=3,
        GiftCard=4
    }
}

﻿namespace RMS.Purchasing.Core.Enums
{
    public enum OrderTypes
    {
        Purchasing,
        DownPayment,
        Online,
        Delivery
    }
}
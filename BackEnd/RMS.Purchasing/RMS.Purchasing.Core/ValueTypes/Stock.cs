﻿using System.Collections.Generic;
using RMS.Purchasing.Core.Aggregates;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.ValueTypes
{
    public class Stock : ValueObject
    {
        public Stock()
        {

        }
        public Stock(Item item, decimal qunatity, decimal available, decimal reserved)
        {
            Item = item;
            Available = available;
            Reserved = reserved;
            Quantity = qunatity;
        }

        public Item Item { get;}
        public decimal Available { get; }
        public decimal Reserved { get; }
        public decimal Quantity { get;}

        public override bool IsValid()
        {
            return Available > 0;
        }

        public bool IsValidForTransaction(decimal transactionQuantity)
        {
            ValidationErrors = new List<ValidationError>();

            if (Available >= transactionQuantity)
            {
                return true;
            }
            ValidationErrors.Add(new ValidationError(Resources.Msg_NotEnoughQuantity));
            return false;
        }

        public Stock Reserve(decimal transactionQuantity)
        {
            ValidationErrors = new List<ValidationError>();

            if (!IsValid()||!IsValidForTransaction(transactionQuantity))
            {
                return null;
            }

            return new Stock(Item,Quantity,Available- transactionQuantity,Reserved+ transactionQuantity);
        }
    }
}

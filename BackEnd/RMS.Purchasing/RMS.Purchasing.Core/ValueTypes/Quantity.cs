﻿using RMS.Purchasing.Core.Aggregates;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.ValueTypes
{
    public class Quantity : ValueObject
    {
        public Quantity(Unit unit, decimal value)
        {
            Unit = unit;
            Value = value;
        }

        #region PROPERTIES

        public decimal Value { get; }
        public Unit Unit { get; }

        #endregion

        #region FUNCTIONS

        public Quantity IncreaseQuantity(decimal incrementValue)
        {
            return new Quantity(Unit, Value + incrementValue);
        }

        public Quantity DecreaseQuantity(decimal decrementValue)
        {
            return new Quantity(Unit, Value + decrementValue);
        }

        public override bool IsValid()
        {
            var isvalid = true;
            if (Value == 0)
            {
                AddValidationError(Resources.Msg_InvalidQuantity);
                isvalid = false;
            }

            if (Unit == null)
            {
                AddValidationError(Resources.Msg_InvalidUnit);
                isvalid = false;
            }

            return isvalid;
        }

        #endregion
    }
}
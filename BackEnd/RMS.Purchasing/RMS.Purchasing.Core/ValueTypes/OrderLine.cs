﻿using System;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Interfaces;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.ValueTypes
{
    public class OrderLine : ValueObject
    {
        public OrderLine(Item lineItem, Quantity lineQuantity, decimal unitPrice,IPurchasingOrder parentOrder)
        {
            LineItem = lineItem;
            Quantity = lineQuantity;
            UnitPrice = unitPrice;
            ParentOrder = parentOrder;
        }

        #region PROPERTIES

        public Item LineItem { get; }
        public Quantity Quantity { get; }
        public decimal UnitPrice { get; }
        public virtual IPurchasingOrder ParentOrder { get; }

        #endregion

        #region FUNCTIONS
        public OrderLine IncreaseQuantity(decimal incrementValue)
        {
            return new OrderLine(LineItem, Quantity.IncreaseQuantity(incrementValue), UnitPrice, ParentOrder);
        }
        public OrderLine DecreaseQuantity(decimal decrementValue)
        {
            return new OrderLine(LineItem, Quantity.IncreaseQuantity(decrementValue), UnitPrice, ParentOrder);
         }
        public OrderLine WithOrderHeader(IPurchasingOrder order)
        {
            return new OrderLine(LineItem, Quantity, UnitPrice, order);
        }
        public OrderLine WithPrice(decimal price)
        {
            return new OrderLine(LineItem, Quantity, price,ParentOrder);
        }

        public override bool IsValid()
        {
            var isValid = true;
            if (!Quantity.IsValid())
            {
                AddValidationErrors(Quantity.ValidationErrors);
                isValid = false;
            }

            if (this.LineItem == null)
            {
                AddValidationError(Resources.Msg_InvalidLineItem);
                isValid = false;
            }

            return isValid;
        }
        #endregion
    }
}
﻿using System;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.ValueTypes.StockTransaction
{
    public class WithdrawStockTransaction : ValueObject
    {
        public WithdrawStockTransaction()
        {

        }
        public WithdrawStockTransaction(Item item, decimal quantity,
            Unit unit, Guid locationId, TransactionType type)
        {
            Item = item;
            Quantity = new Quantity(unit, quantity);
            TimeStamp = DateTime.Now;
            LocationId = locationId;
        }

        public Item Item { get; }
        public Quantity Quantity { get; }
        public Guid LocationId { get; }
        public DateTime TimeStamp { get; }

        public override bool IsValid()
        {
            var isValid = true;
            if (Item == null)
            {
                isValid = false;
                ValidationErrors.Add(new ValidationError(Resources.Msg_InvalidTransactionItem));
            }
            if (Quantity.Value == 0)
            {
                isValid = false;
                ValidationErrors.Add(new ValidationError(Resources.Msg_InvalidTransactionQuantity));
            }
            if (LocationId != Guid.Empty)
                return isValid;

            ValidationErrors.Add(new ValidationError(Resources.Msg_InvalidTransactionLocation));
            return false;
        }
    }
}

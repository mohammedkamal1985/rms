﻿using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.Interfaces;

namespace RMS.Purchasing.Core.ValueTypes.Payment
{
    public class CreditCardPayment:PaymentBase
    {
        public CreditCardPayment(decimal value,IPurchasingOrder order,string accountId,CreditCardTypes accountType):
            base(value,order)
        {
            AccountId = accountId;
            AccountType = accountType;
            Type = PaymentTypes.CreditCard;
        }

        public string AccountId { get; }
        public CreditCardTypes AccountType { get; }

        public override bool IsValid()
        {
            return true;
        }
    }
}

﻿using System;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.Interfaces;
using RMS.SharedKernel.Core.Entities.Base;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.ValueTypes.Payment
{
    public abstract class PaymentBase : ValueObject
    {
        public PaymentBase(decimal value, IPurchasingOrder order)
        {
            Value = value;
            OrderHeader = order;
        }
        public IPurchasingOrder OrderHeader { get; }
        public decimal Value { get; }
        public PaymentTypes Type { get; protected set; }
    }
}
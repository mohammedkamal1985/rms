﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.Core.Interfaces;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Core.ValueTypes.Payment;
using RMS.SharedKernel.Core.ValueTypes;
using RMS.SharedKernel.Entities.Base;

namespace RMS.Purchasing.Core.Entities
{
    public class DeliveryOrder : PurchasingOrder, IDeliveryOrder
    {
        #region PROPERTIES

        public Address DeliveryAddress { get; }
        public DateTime? DeliveryDate { get; }
        public override OrderTypes OrderType => OrderTypes.Delivery;

        #endregion

        #region CONSTRUCTORS

        public DeliveryOrder(string serial, DateTime createDate, DateTime? closeDate,
            Customer customer, OrderStatus status, decimal discount, string notes,
            IReadOnlyCollection<OrderLine> orderLines, IReadOnlyCollection<PaymentBase> orderPayments,
            Guid salesmanId, string machineName, Guid warehouseId,
            Address deliveryAddress, DateTime? deliveryDate) :
            base(serial, createDate, closeDate, customer, status,
                discount, notes, orderLines, orderPayments,
                salesmanId, machineName, warehouseId)
        {
            DeliveryAddress = deliveryAddress;
            DeliveryDate = deliveryDate;
            
        }

        #endregion

        #region FUNCTIONS

        public new DeliveryOrder Create()
        {
            if (IsValidForDelivery())
            {
                return new DeliveryOrder(Serial, DateTime.Now, CloseDate, Customer, OrderStatus.New, Discount, Notes,
                    new ReadOnlyCollection<OrderLine>(new List<OrderLine>()),
                    new ReadOnlyCollection<PaymentBase>(new List<PaymentBase>()),
                    SalesmanId, MachineName, WarehouseId, null, null);
            }

            return this;
        }

        /*public DeliveryOrder Submit(decimal paymentValue, bool payOnDelivery)
        {
            PayOnDelivery = payOnDelivery;
            if (IsValidForDelivery())
            {
                var tmpOrder = base.Create();
                return new DeliveryOrder(tmpOrder.Serial, tmpOrder.Cre);
            }

            return this;
        }*/

        public new DeliveryOrder Close()
        {
            if (IsValidForClose())
            {
                return new DeliveryOrder(Serial, CreateDate, DateTime.Now, Customer,
                    OrderStatus.Closed, Discount, Notes, OrderLines, Payments, SalesmanId,
                    MachineName, WarehouseId, DeliveryAddress, DeliveryDate);
            }

            return this;
        }

        private bool IsValidForDelivery()
        {
            var isValid = IsValidForCreation();

            if (DeliveryAddress == null)
            {
                isValid = false;
                ValidationErrors.Add(new ValidationError(""));
            }

            return isValid;
        }

        #endregion
    }
}
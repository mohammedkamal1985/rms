using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExceptionHandling.CustomException;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.Enums;
using RMS.Purchasing.DomainService.Infrastructure;

namespace RMS.Purchasing.DomainService.Test
{
    [TestClass]
    public class DomainServiceTest
    {
        private IPurchasingDomainService _domainService;
        private Item _item;
        private Unit _defaultUnit;
        private Unit _alternativeUnit;
        private Unit _thiredUnit;
        private Customer _orderCustomer;

        [TestInitialize]
        public void Initialize()
        {
            _domainService = new PurchasingDomainService();
            _defaultUnit = new Unit(Guid.NewGuid(), "kg", "Kilo");
            _alternativeUnit = new Unit(Guid.NewGuid(), "gm", "Gram");
            _thiredUnit = new Unit(Guid.NewGuid(), "hk", "Half Kilo");
            var units = new Dictionary<Unit, decimal>();
            units.Add(_defaultUnit, 1);
            units.Add(_alternativeUnit, 0.001M);
            units.Add(_thiredUnit, 0.5M);
            var category = new ItemCategory(Guid.NewGuid(), "Test Category", "ITC");
            var group = new ItemGroup(Guid.NewGuid(), "Test Item Group", "TIG");
            _item = new Item(Guid.NewGuid(), "Test Code", "Test Name", units, _defaultUnit, 20, category, group);
            _orderCustomer = new Customer(null,Guid.NewGuid(), "Test Customer","+31626223555",false,
                new CustomerGroup(Guid.NewGuid(),"CGT","Customer Group Test"));
            
        }

        [TestMethod]
        public void CalculatePrice_NullItem()
        {
            //Assert
            Assert.ThrowsException<BusinessException>(() =>
                _domainService.CalculateItemPrice(null, _item.DefaultUnit, null,
                    OrderTypes.Purchasing, _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_NullUnit()
        {
            //Assert
            Assert.ThrowsException<BusinessException>(() =>
                _domainService.CalculateItemPrice(_item, null, null, OrderTypes.Purchasing, _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_NullCustomer()
        {
            //Assert
            Assert.ThrowsException<BusinessException>(() =>
                _domainService.CalculateItemPrice(_item, _item.DefaultUnit, null,
                    OrderTypes.Purchasing, null));
        }

        [TestMethod]
        public void CalculatePrice_UnitIsNotInItemsAvailableUnits()
        {
            //Arrange
            var tmpUit = new Unit(Guid.NewGuid(), "Tmp Unit", "tmp");

            //Assert
            Assert.ThrowsException<BusinessException>(() =>
                _domainService.CalculateItemPrice(_item, tmpUit, null, OrderTypes.Purchasing,
                    _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_NoPriceList_UnitUsedIsDefaultUnitTest()
        {
            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit, null,
                OrderTypes.Purchasing, _orderCustomer);

            //Assert
            Assert.AreEqual(_item.DefaultPrice, result);
        }

        [TestMethod]
        public void CalculatePrice_NoPriceList_UnitUsedExistsInAvailableUnits_HasConversionFactorTest()
        {
            //Action
            var result = _domainService.CalculateItemPrice(_item, _alternativeUnit, null,
                OrderTypes.Purchasing, _orderCustomer);

            //Assert
            Assert.AreEqual(_item.AvailableUnits[_alternativeUnit] * _item.DefaultPrice, result);
        }

        [TestMethod]
        public void CalculatePrice_NoPriceList_UnitUsedExistsInAvailableUnits_NoConversionFactorTest()
        {
            //Arrange
            var tmpUnit = new Unit(Guid.NewGuid(), "Test Unit", "testCode");
            _item.AvailableUnits.Add(tmpUnit, 0);

            //Assert
            Assert.ThrowsException<BusinessException>(() =>
                _domainService.CalculateItemPrice(_item, tmpUnit, null, OrderTypes.Purchasing,
                    _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_NoPriceList_UnitUsedDoesNotExistsInAvailableUnitsTest()
        {
            //Arrange
            var tmpUnit = new Unit(Guid.NewGuid(), "Test Unit", "testCode");

            //Assert
            Assert.ThrowsException<BusinessException>(() => _domainService.CalculateItemPrice(_item, tmpUnit, null,
                OrderTypes.Purchasing, _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_PriceListDateOutOfRange()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(
                new List<ItemPrice>
                {
                    new ItemPrice(_item.Id, _item.DefaultUnit.Id, 20)
                }
            );
            var priceList = new PriceList(DateTime.Now.AddDays(-70), DateTime.Now.AddDays(-5), 0,
                PriceListTypes.All, string.Empty, prices);

            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit, new List<PriceList> {priceList},
                OrderTypes.Purchasing, _orderCustomer);

            //Assert
            Assert.AreEqual(_item.DefaultPrice, result);
        }

        [TestMethod]
        public void CalculatePrice_PriceListDateInRange_ItemAndUnitPriceExists()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(
                new List<ItemPrice>
                {
                    new ItemPrice(_item.Id, _item.DefaultUnit.Id, 20)
                }
            );
            var priceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.All, string.Empty, prices);

            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit, new List<PriceList> {priceList},
                OrderTypes.Purchasing, _orderCustomer);

            //Assert
            Assert.AreEqual(20, result);
        }

        [TestMethod]
        public void CalculatePrice_PriceListDateInRange_ItemPriceExists_PriceListUnitIsNotAvailableUnit()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, Guid.NewGuid(), 20)
            });
            var priceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.All, string.Empty, prices);

            //Assert
            Assert.ThrowsException<BusinessException>(() => _domainService.CalculateItemPrice(_item,
                _item.AvailableUnits.Keys.ToList()[1],
                new List<PriceList> {priceList}, OrderTypes.Purchasing, _orderCustomer));
        }

        [TestMethod]
        public void CalculatePrice_PriceListDateInRange_ItemPriceExists_PriceListUnitIsAvailableUnit()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _thiredUnit.Id, 10)
            });
            var priceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.All, string.Empty, prices);

            //Action
            var result = _domainService.CalculateItemPrice(_item, _alternativeUnit, new List<PriceList> {priceList},
                OrderTypes.Purchasing, _orderCustomer, 250);

            //Assert
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void CalculatePrice_MultiPriceLists_SomePriceListsNotValid()
        {
            //Arrange
            var prices1 =  new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, Guid.NewGuid(), 20)
            });
            var invalidPriceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.Customer, Guid.NewGuid().ToString(), prices1);
            var prices2 =  new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 100)
            });
            var validPriceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 1,
                PriceListTypes.All, string.Empty, prices2);

            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit,
                new List<PriceList> {validPriceList, invalidPriceList}, OrderTypes.Delivery, _orderCustomer);

            //Assert 
            Assert.AreEqual(prices2[0].Price, result);
        }

        [TestMethod]
        public void CalculatePrice_MultiPriceLists_AllListsValid_DifferentPriorityIndex()
        {
            //Arrange
            var prices1 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 20)
            });
            var validPriceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 1,
                PriceListTypes.Customer, _orderCustomer.Id.ToString(), prices1);
            var prices2 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 100)
            });
            var validPriceList1 = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.All, string.Empty, prices2);

            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit,
                new List<PriceList> {validPriceList, validPriceList1}, OrderTypes.Delivery, _orderCustomer);

            //Assert 
            Assert.AreEqual(prices1[0].Price, result);
        }

        [TestMethod]
        public void CalculatePrice_MultiPriceLists_AllListsValid_OnDontHaveItemUnitPrice()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 20)
            });
            var validPriceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.Customer, _orderCustomer.Id.ToString(), prices);

            var prices1 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, Guid.NewGuid(), 100)
            });
            var validPriceList1 = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 2,
                PriceListTypes.All, string.Empty, prices1);

            var prices2 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 50)
            });
            var validPriceList2 = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 1,
                PriceListTypes.OrderType, OrderTypes.Delivery.ToString(), prices2);
            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit,
                new List<PriceList> {validPriceList, validPriceList1, validPriceList2}, OrderTypes.Delivery,
                _orderCustomer);

            //Assert 
            Assert.AreEqual(prices2[0].Price, result);
        }

        [TestMethod]
        public void CaluclatePrice_MultiPriceLists_AllListsValid_SamePriorityIndex()
        {
            //Arrange
            var prices = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 20)
            });
            var validPriceList = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.Customer, _orderCustomer.Id.ToString(), prices);

            var prices1 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 100)
            });
            var validPriceList1 = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.All, string.Empty, prices1);

            var prices2 = new ReadOnlyCollection<ItemPrice>(new List<ItemPrice>()
            {
                new ItemPrice(_item.Id, _defaultUnit.Id, 50)
            });
            var validPriceList2 = new PriceList(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(60), 0,
                PriceListTypes.OrderType, OrderTypes.Delivery.ToString(), prices2);
            //Action
            var result = _domainService.CalculateItemPrice(_item, _item.DefaultUnit,
                new List<PriceList> {validPriceList, validPriceList1, validPriceList2}, OrderTypes.Delivery,
                _orderCustomer);

            //Assert 
            Assert.AreEqual(prices[0].Price, result);
        }
    }
}
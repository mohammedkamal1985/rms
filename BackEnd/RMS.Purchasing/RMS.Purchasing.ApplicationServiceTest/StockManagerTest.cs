﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExceptionHandling.CustomException;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.ApplicationService;
using RMS.Purchasing.ApplicationService.Mapping;
using RMS.Purchasing.DomainService.Infrastructure;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Repository;
using RMS.Purchasing.Repository.Infrastucture;

namespace ApplicationServiceTest
{
    /*[TestClass]
    public class StockManagerTest
    {
        private IStockManager _stockManager;
        private IMappingService _mappingService;
        private Mock<ILogger> _mockLogger;
        private Mock<IPurchasingDomainService> _domainService;

        private StockDataModel BuildStockDataModel()
        {
            var unitDM = new UnitDataModel()
            {
                DomainId = Guid.NewGuid(),
                Name = "Test Unit",
                Code = "testCode"
            };

            var itemDM = new ItemDataModel()
            {
                DomainId = Guid.NewGuid(),
                Code = "123456",
                Name = "Test Item",
                DefaultUnit = unitDM,
                DefaultPrice = 10
            };
            var units = new List<ItemUnitDataModel> {new ItemUnitDataModel() {Unit = unitDM, Item = itemDM}};
            itemDM.AvailableUnits = units;
            var stockDM = new StockDataModel()
            {
                Item = itemDM,
                Quantity = 100,
                Available = 30,
                Reserved = 70
            };
            return stockDM;
        }

        private void MockDomainService(decimal returnedValue)
        {
            if (_domainService == null)
                _domainService = new Mock<IPurchasingDomainService>();
            _domainService.Setup(s => s.ConvertQuantityToItemDefaultUnitQuantity(It.IsAny<decimal>(),
                It.IsAny<Unit>(), It.IsAny<Item>())).Returns(returnedValue);
        }

        [TestInitialize]
        public void Initialize()
        {
            _mappingService = new MappingService();
            _mockLogger = new Mock<ILogger>();
        }

        [TestMethod]
        public void GetStockByItemCode_NullRequest()
        {
            //Arranger
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryWithNoData().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Assert && Action
            Assert.ThrowsException<BusinessException>(() => _stockManager.GetStockByItemCode(null));
        }

        [TestMethod]
        public void GetStockByItemCode_NullRequestData()
        {
            //Arranger
            var request = new ServiceRequest<string>(null);
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryWithNoData().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Assert && Action
            Assert.ThrowsException<BusinessException>(() => _stockManager.GetStockByItemCode(request));
        }

        [TestMethod]
        public void GetStockByItemCode_StockQueryRepositoryThrowException()
        {
            //Arranger
            var request = new ServiceRequest<string>("testCode");
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryThatThrowException().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Assert && Action
            Assert.ThrowsException<BusinessException>(() => _stockManager.GetStockByItemCode(request));
        }

        [TestMethod]
        public void GetStockByItemCode_StockQueryRepositoryReturnNull()
        {
            //Arranger
            var request = new ServiceRequest<string>("testCode");
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryThatReturnNull().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action
            var response = _stockManager.GetStockByItemCode(request);

            //Assert
            Assert.IsNull(response.Result);
        }

        [TestMethod]
        public void GetStockByItemCode_StockQueryRepositoryReturnData()
        {
            //Arranger
            var stockDataModel = BuildStockDataModel();
            var request = new ServiceRequest<string>("testCode");
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryThatReturnData(stockDataModel).Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action
            var response = _stockManager.GetStockByItemCode(request);

            //Assert
            Assert.AreEqual(stockDataModel.Item.Code, response.Result.Item.Code);
            Assert.AreEqual(stockDataModel.Item.DefaultUnit.Code, response.Result.Item.DefaultUnit.Code);
            Assert.AreEqual(stockDataModel.Available, response.Result.Available);
            Assert.AreEqual(stockDataModel.Quantity, response.Result.Quantity);
            Assert.AreEqual(stockDataModel.Reserved, response.Result.Reserved);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ReserveStock_NullRequest()
        {
            //Arranger
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryWithNoData().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);
            //Action
            Assert.ThrowsException<BusinessException>(() =>
                _stockManager.ReserveStock((ServiceRequest<StockTransactionModel>) null));
        }

        [TestMethod]
        public void ReserveStock_NullRequestData()
        {
            //Arranger
            var request = new ServiceRequest<StockTransactionModel>(null);
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryWithNoData().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action
            Assert.ThrowsException<BusinessException>(() => _stockManager.ReserveStock(request));
        }

        [TestMethod]
        public void ReserveStock_GetStockByItemThrowsException()
        {
            //Arranger
            var request = new ServiceRequest<StockTransactionModel>(new StockTransactionModel
            {
                Quantity = 1,
                Type = 1
            });
            var uoW = new StockRepositoryBuilder().BuildReadRepositoryThatThrowException().Build();
            MockDomainService(5);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action && Assert
            Assert.ThrowsException<BusinessException>(() => _stockManager.ReserveStock(request));
        }

        [TestMethod]
        public void ReserveStock_NotEnoughQuantity()
        {
            //Arranger
            var stockDataModel = BuildStockDataModel();
            stockDataModel.Quantity = 20;
            stockDataModel.Available = 1;
            stockDataModel.Reserved = 9;

            var request = new ServiceRequest<StockTransactionModel>(new StockTransactionModel()
            {
                Quantity = 3,
                Type = 1,
                Item = new ItemModel()
                {
                    Code = stockDataModel.Item.Code,
                }
            });

            var uoW = new StockRepositoryBuilder().BuildReadRepositoryThatReturnData(stockDataModel).Build();
            MockDomainService(3);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action
            var response = _stockManager.ReserveStock(request);

            //Assert
            Assert.IsFalse(response.Result);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ReserveStock_EnoughQuantity_SaveTransactionThroughException()
        {
            //Arranger
            var stockDataModel = BuildStockDataModel();
            stockDataModel.Quantity = 20;
            stockDataModel.Available = 5;
            stockDataModel.Reserved = 9;

            var request = new ServiceRequest<StockTransactionModel>(new StockTransactionModel()
            {
                Quantity = 3,
                Type = 1,
                Item = new ItemModel()
                {
                    Code = stockDataModel.Item.Code,
                }
            });

            var uoW = new StockRepositoryBuilder()
                .BuildReadRepositoryThatReturnData(stockDataModel)
                .BuildWriteRepositoryThatThrowException().Build();
            MockDomainService(3);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action && Assert
            Assert.ThrowsException<BusinessException>(() => _stockManager.ReserveStock(request));
        }

        [TestMethod]
        public void ReserveStock_NormalScenario()
        {
            //Arranger
            var stockDataModel = BuildStockDataModel();
            stockDataModel.Quantity = 20;
            stockDataModel.Available = 5;
            stockDataModel.Reserved = 9;

            var request = new ServiceRequest<StockTransactionModel>(new StockTransactionModel()
            {
                Quantity = 3,
                Type = 1,
                Item = new ItemModel()
                {
                    Code = stockDataModel.Item.Code,
                }
            });

            var uoW = new StockRepositoryBuilder()
                .BuildReadRepositoryThatReturnData(stockDataModel)
                .BuildWriteRepositoryThatSaveSuccessfully(stockDataModel).Build();
            
            MockDomainService(3);
            _stockManager =
                new StockManager(uoW, _mappingService, _mockLogger.Object, _domainService.Object);

            //Action
            var response = _stockManager.ReserveStock(request);

            //Assert
            Assert.IsTrue(response.Result);
        }
    }

    public class StockRepositoryBuilder
    {
        private IStockQueryRepository _stockQueryRepository;
        private IStockWriteRepository _stockWriteRepository;

        public StockRepositoryBuilder BuildReadRepositoryWithNoData()
        {
            _stockQueryRepository = new Mock<IStockQueryRepository>().Object;
            return this;
        }

        public StockRepositoryBuilder BuildReadRepositoryThatThrowException()
        {
            var mock = new Mock<IStockQueryRepository>();
            mock.Setup(repo => repo.GetByItemCode(It.IsAny<string>()))
                .Throws(new RepositoryException());
            _stockQueryRepository = mock.Object;
            return this;
        }

        public StockRepositoryBuilder BuildReadRepositoryThatReturnNull()
        {
            var mock = new Mock<IStockQueryRepository>();
            mock.Setup(repo => repo.GetByItemCode(It.IsAny<string>()))
                .Returns<StockDataModel>(null);
            _stockQueryRepository = mock.Object;
            return this;
        }

        public StockRepositoryBuilder BuildReadRepositoryThatReturnData(StockDataModel retrievedDataModel)
        {
            var mock = new Mock<IStockQueryRepository>();
            mock.Setup(repo => repo.GetByItemCode(It.IsAny<string>())).Returns(retrievedDataModel);
            _stockQueryRepository = mock.Object;
            return this;
        }

        public StockRepositoryBuilder BuildWriteRepositoryThatThrowException()
        {
            var mock = new Mock<IStockWriteRepository>();
            mock.Setup(repo => repo.Update(It.IsAny<StockDataModel>()))
                .Throws(new RepositoryException());
            _stockWriteRepository = mock.Object;
            return this;
        }

        public StockRepositoryBuilder BuildWriteRepositoryThatSaveSuccessfully(StockDataModel stockDataModel)
        {
            var mock = new Mock<IStockWriteRepository>();
            mock.Setup(repo => repo.Update(It.IsAny<StockDataModel>()))
                .Returns(stockDataModel);
            _stockWriteRepository = mock.Object;
            return this;
        }

        public IPurchasingUnitOfWork Build()
        {
            return new PurchasingUnitOfWork(null, null, null,
                null, null, null,
                _stockQueryRepository, _stockWriteRepository);
        }
    }*/
}
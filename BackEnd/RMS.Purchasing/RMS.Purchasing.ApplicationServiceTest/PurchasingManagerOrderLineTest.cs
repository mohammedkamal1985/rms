﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationServiceTest
{
    [TestClass]
    public class PurchasingManagerOrderLineTest
    {
        /*
         * [TestMethod]
        public void AddOrderLine_ByItemCode_FirstTimeAdd()
        {
            //Arrange
            var orderId = Guid.NewGuid();
            var itemCode = "012345";
            var order = SetupOrderRepositoryGetOrderByIdEmptyOrderMock(orderId);
            var item = SetupItemRepositoryGetItemByCodeAndPriceListsMock(itemCode);
            var price = SetupDomainServiceCalculatePriceMock();
            SetupOrderLineRepositoryEmptyMock();
            SetupLoggerMockup();
            InitializePurchasingManager();

            //Action 
            var result = _purchasingManager.AddOrderLine(orderId, itemCode, 1);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.OrderLines.Count == 1);
            Assert.AreEqual(itemCode, result.OrderLines[0].Item.Code);
            Assert.AreEqual(1, result.OrderLines[0].Quantity);
        }

        [TestMethod]
        public void AddOrderLine_ByItemCode_EMptyItemCode()
        {
            //Arrange
            var orderId = Guid.NewGuid();
            var itemCode = "";
            var order = SetupOrderRepositoryGetOrderByIdEmptyOrderMock(orderId);
            var item = SetupItemRepositoryGetItemByCodeAndPriceListsMock(itemCode);
            var price = SetupDomainServiceCalculatePriceMock();
            SetupOrderLineRepositoryEmptyMock();
            SetupLoggerMockup();
            InitializePurchasingManager();

            //Action 
            var result = _purchasingManager.AddOrderLine(orderId, itemCode, 1);

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void AddOrderLine_ByItemCode_EMptyOrderId()
        {
            //Arrange
            Guid orderId = Guid.Empty;
            var itemCode = "012345";
            var order = SetupOrderRepositoryGetOrderByIdEmptyOrderMock(orderId);
            var item = SetupItemRepositoryGetItemByCodeAndPriceListsMock(itemCode);
            var price = SetupDomainServiceCalculatePriceMock();
            SetupOrderLineRepositoryEmptyMock();
            SetupLoggerMockup();
            InitializePurchasingManager();

            //Action 
            var result = _purchasingManager.AddOrderLine(orderId, itemCode, 1);

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void AddOrderLine_ByItemCode_ItemWasAddBefore()
        {
            //Arrange
            var orderId = Guid.NewGuid();
            var itemCode = "012345";
            var item = SetupItemRepositoryGetItemByCodeAndPriceListsMock(itemCode);
            var order = SetupOrderRepositoryGetOrderByIdOrderWithOrderLinesMock(orderId, item);
            var price = SetupDomainServiceCalculatePriceMock();
            SetupOrderLineRepositoryEmptyMock();
            SetupLoggerMockup();
            InitializePurchasingManager();

            //Action 
            var result = _purchasingManager.AddOrderLine(orderId, itemCode, 1);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.OrderLines.Count == 1);
            Assert.AreEqual(itemCode, result.OrderLines[0].Item.Code);
            Assert.AreEqual(2, result.OrderLines[0].Quantity);
        }
        */
    }
}

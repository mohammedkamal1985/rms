using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.ApplicationService.Mapping;
using RMS.Purchasing.Core.Aggregates;
using RMS.Purchasing.Core.ValueTypes;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.SharedKernel.Core.ValueTypes;

namespace ApplicationServiceTest
{
    [TestClass]
    public class MappingServiceTesting
    {
        private MappingService _mappingService = new MappingService();

        [TestInitialize]
        public void Initialize()
        {
            //_mappingService = new MappingService();
        }

        #region ADDRESS MAPPING TEST

        [TestMethod]
        public void ConvertDomainAddressToBusinessModelAddressTest()
        {
            //Arrange
            var domainAddress = new Address(10, "Plastine St.", "New Maadi", "Cairo", "Egypt", "Near Court Yard","");

            //Action
            var businessModel = _mappingService.Map<Address, CustomerAddressModel>(domainAddress);

            //Assert
            Assert.AreEqual(domainAddress.BlockNo, businessModel.BlockNo);
            Assert.AreEqual(domainAddress.Street, businessModel.Street);
            Assert.AreEqual(domainAddress.City, businessModel.City);
            Assert.AreEqual(domainAddress.Street, businessModel.Street);
            Assert.AreEqual(domainAddress.Country, businessModel.Country);
            Assert.AreEqual(domainAddress.Landmark, businessModel.Landmark); 
            Assert.AreEqual(domainAddress.PostCode, businessModel.PostCode);
        }

        [TestMethod]
        public void ConvertBusinessModelAddressToDomainAddressTest()
        {
            //Arrange
            var businessModel = new CustomerAddressModel
            {
                BlockNo = 10,
                Street = "Plastine St.",
                City = "New Maadi",
                State = "Cairo",
                Country = "Egypt",
                Landmark = "Near Cpirt Yard"
            };

            //Action
            var domainAddress = _mappingService.Map<CustomerAddressModel, Address>(businessModel);

            //Assert
            Assert.AreEqual(businessModel.BlockNo, domainAddress.BlockNo);
            Assert.AreEqual(businessModel.Street, domainAddress.Street);
            Assert.AreEqual(businessModel.City, domainAddress.City);
            Assert.AreEqual(businessModel.Street, domainAddress.Street);
            Assert.AreEqual(businessModel.Country, domainAddress.Country);
            Assert.AreEqual(businessModel.Landmark, domainAddress.Landmark);
        }

        [TestMethod]
        public void ConvertDomainAddressToDataModelAddressTest()
        {
            //Arrange
            var domainAddress = new Address(10, "Plastine St.", "New Maadi", "Cairo", "Egypt", "Near Court Yard","");

            //Action
            var dataModel = _mappingService.Map<Address, AddressDataModel>(domainAddress);

            //Assert
            Assert.AreEqual(domainAddress.BlockNo, dataModel.BlockNo);
            Assert.AreEqual(domainAddress.Street, dataModel.Street);
            Assert.AreEqual(domainAddress.City, dataModel.City);
            Assert.AreEqual(domainAddress.Street, dataModel.Street);
            Assert.AreEqual(domainAddress.Country, dataModel.Country);
            Assert.AreEqual(domainAddress.Landmark, dataModel.Landmark);
        }

        [TestMethod]
        public void ConvertDataModelAddressToDomainAddressTest()
        {
            //Arrange
            var dataModel = new AddressDataModel
            {
                BlockNo = 10,
                Street = "Plastine St.",
                City = "New Maadi",
                State = "Cairo",
                Country = "Egypt",
                Landmark = "Near Cpirt Yard"
            };

            //Action
            var domainAddress = _mappingService.Map<AddressDataModel, Address>(dataModel);

            //Assert
            Assert.AreEqual(dataModel.BlockNo, domainAddress.BlockNo);
            Assert.AreEqual(dataModel.Street, domainAddress.Street);
            Assert.AreEqual(dataModel.City, domainAddress.City);
            Assert.AreEqual(dataModel.Street, domainAddress.Street);
            Assert.AreEqual(dataModel.Country, domainAddress.Country);
            Assert.AreEqual(dataModel.Landmark, domainAddress.Landmark);
        }

        #endregion ADDRESS MAPPING TEST

        #region CUSTOMER GROUP MAPPING

        [TestMethod]
        public void ConvertDomainCustomerGroupToDataModelCustomerGroupTest()
        {
            //Arrange
            var domainEntity = new CustomerGroup(Guid.NewGuid(), "test name", "tets code");

            //Action
            var dataModel = _mappingService.Map<CustomerGroup, CustomerGroupDataModel>(domainEntity);

            //Assert
            Assert.AreEqual(domainEntity.Id, dataModel.Id);
            Assert.AreEqual(domainEntity.Code, dataModel.Code);
            Assert.AreEqual(domainEntity.Name, dataModel.Name);
        }

        [TestMethod]
        public void ConvertDataModelCustomerGroupToDomainCustomerGroupTest()
        {
            //Arrange
            var dataModel = new CustomerGroupDataModel
            {
                Code = "test code",
                Id = Guid.NewGuid(),
                Name = "test name"
            };

            //Action
            var domainEntity = _mappingService.Map<CustomerGroupDataModel, CustomerGroup>(dataModel);

            //Assert
            Assert.AreEqual(dataModel.Code, domainEntity.Code);
            Assert.AreEqual(dataModel.Id, domainEntity.Id);
            Assert.AreEqual(dataModel.Name, domainEntity.Name);
        }

        #endregion CUSTOMER GROUP MAPPING

        #region CUSTOMER MAPPING TEST

        [TestMethod]
        public void ConvertDomainCustomerToBusinessModelCustomerTest()
        {
            //Arrange
            var domainAddress = new Address(10, "Plastine St.", "New Maadi", "Cairo", "Egypt", "Near court Yard","");
            var domainCustomer = new Customer(new ReadOnlyCollection<Address>(
                    new List<Address>() {domainAddress}
                ),
                Guid.NewGuid(), "Testing Customer", "010123456",
                false, new CustomerGroup(Guid.NewGuid(), "TCG", "Test Customer Group"));

            //Action
            var businessModel = _mappingService.Map<Customer, CustomerModel>(domainCustomer);

            //Assert
            Assert.AreEqual(domainCustomer.DeliveryAddresses.Count, businessModel.Addresses.Count);
            Assert.AreEqual(domainCustomer.Id, businessModel.Id);
            Assert.AreEqual(domainCustomer.Name, businessModel.Name);
            Assert.AreEqual(domainCustomer.PhoneNumber, businessModel.PhoneNumber);
            Assert.AreEqual(domainCustomer.IsDefault, businessModel.IsDefault);
        }

        [TestMethod]
        public void ConvertBusinessModelCustomerToDomainCustomerTest()
        {
            //Arrange
            var businessModel = new CustomerModel
            {
                Addresses = new List<CustomerAddressModel>()
                {
                    new CustomerAddressModel
                    {
                        BlockNo = 10,
                        Street = "Plastine St.",
                        City = "New Maadi",
                        State = "Cairo",
                        Country = "Egypt",
                        Landmark = "Near Court Yard"
                    }
                },
                Id = Guid.NewGuid(),
                Name = "Testing Customer"
            };

            //Action
            var domainCustomer = _mappingService.Map<CustomerModel, Customer>(businessModel);

            //Assert
            Assert.AreEqual(businessModel.Addresses.Count, domainCustomer.DeliveryAddresses.Count);
            Assert.AreEqual(businessModel.Id, domainCustomer.Id);
            Assert.AreEqual(businessModel.Name, domainCustomer.Name);
        }

        [TestMethod]
        public void ConvertDomainCustomerToDataModelCustomerTest()
        {
            //Arrange
            var domainAddress = new Address(10, "Plastine St.", "New Maadi", "Cairo", "Egypt", "Near Court Yard","");
            var domainCustomer = new Customer(new ReadOnlyCollection<Address>(
                    new List<Address>() {domainAddress}
                ),
                Guid.NewGuid(), "Testing Customer", "010123456",
                false, new CustomerGroup(Guid.NewGuid(), "TCG", "Test Customer Group"));

            //Action
            var dataModel = _mappingService.Map<Customer, CustomerDataModel>(domainCustomer);

            //Assert
            Assert.AreEqual(domainCustomer.DeliveryAddresses.Count, dataModel.DeliveryAddresses.Count);
            Assert.AreEqual(domainCustomer.Id, dataModel.Id);
            Assert.AreEqual(domainCustomer.Name, dataModel.Name);
            Assert.AreEqual(domainCustomer.PhoneNumber, dataModel.PhoneNumber);
            Assert.AreEqual(domainCustomer.IsDefault, dataModel.IsDefault);
            Assert.AreEqual(domainCustomer.Group.Code, dataModel.Group.Code);
            Assert.AreEqual(domainCustomer.Group.Name, dataModel.Group.Name);
            Assert.AreEqual(domainCustomer.Group.Id, dataModel.Group.Id);
        }

        [TestMethod]
        public void ConvertDataModelCustomerToDomainCustomerTest()
        {
            //Arrange
            var dataModel = new CustomerDataModel
            {
                DeliveryAddresses = new List<AddressDataModel>()
                {
                    new AddressDataModel
                    {
                        BlockNo = 10,
                        Street = "Plastine St.",
                        City = "New Maadi",
                        State = "Cairo",
                        Country = "Egypt",
                        Landmark = "Near Court Yard"
                    }
                },
                Id = Guid.NewGuid(),
                Name = "Testing Customer",
                PhoneNumber = "123456789"
            };

            //Action
            var domainCustomer = _mappingService.Map<CustomerDataModel, Customer>(dataModel);

            //Assert
            Assert.AreEqual(dataModel.DeliveryAddresses.Count, domainCustomer.DeliveryAddresses.Count);
            Assert.AreEqual(dataModel.Id, domainCustomer.Id);
            Assert.AreEqual(dataModel.Name, domainCustomer.Name);
            Assert.AreEqual(dataModel.PhoneNumber, domainCustomer.PhoneNumber);
        }

        #endregion CUSTOMER MAPPING TEST

        #region Item GROUP MAPPING

        [TestMethod]
        public void ConvertDomainItemGroupToDataModelItemGroupTest()
        {
            //Arrange
            var domainEntity = new ItemGroup(Guid.NewGuid(), "test name", "tets code");

            //Action
            var dataModel = _mappingService.Map<ItemGroup, ItemGroupDataModel>(domainEntity);

            //Assert
            Assert.AreEqual(domainEntity.Id, dataModel.Id);
            Assert.AreEqual(domainEntity.Code, dataModel.Code);
            Assert.AreEqual(domainEntity.Name, dataModel.Name);
        }

        [TestMethod]
        public void ConvertDataModelItemGroupToDomainItemGroupTest()
        {
            //Arrange
            var dataModel = new ItemGroupDataModel
            {
                Code = "test code",
                Id = Guid.NewGuid(),
                Name = "test name"
            };

            //Action
            var domainEntity = _mappingService.Map<ItemGroupDataModel, ItemGroup>(dataModel);

            //Assert
            Assert.AreEqual(dataModel.Code, domainEntity.Code);
            Assert.AreEqual(dataModel.Id, domainEntity.Id);
            Assert.AreEqual(dataModel.Name, domainEntity.Name);
        }

        #endregion ITEM GROUP MAPPING

        #region Item CATEGORY MAPPING

        [TestMethod]
        public void ConvertDomainItemCategoryToDataModelItemCategoryTest()
        {
            //Arrange
            var domainEntity = new ItemCategory(Guid.NewGuid(), "test name", "tets code");

            //Action
            var dataModel = _mappingService.Map<ItemCategory, ItemCategoryDataModel>(domainEntity);

            //Assert
            Assert.AreEqual(domainEntity.Id, dataModel.Id);
            Assert.AreEqual(domainEntity.Code, dataModel.Code);
            Assert.AreEqual(domainEntity.Name, dataModel.Name);
        }

        [TestMethod]
        public void ConvertDataModelItemCategoryToDomainItemCategoryTest()
        {
            //Arrange
            var dataModel = new ItemCategoryDataModel
            {
                Code = "test code",
                Id = Guid.NewGuid(),
                Name = "test name"
            };

            //Action
            var domainEntity = _mappingService.Map<ItemCategoryDataModel, ItemCategory>(dataModel);

            //Assert
            Assert.AreEqual(dataModel.Code, domainEntity.Code);
            Assert.AreEqual(dataModel.Id, domainEntity.Id);
            Assert.AreEqual(dataModel.Name, domainEntity.Name);
        }

        #endregion ITEM CATEGORY MAPPING

        #region UNIT MAPPING TEST

        [TestMethod]
        public void ConvertFromDomainUnitToBusinessModelUnitTest()
        {
            //Arrange
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "TU");

            //Action
            var businessUnit = _mappingService.Map<Unit, UnitModel>(domainUnit);

            //Assert
            Assert.AreEqual(domainUnit.Id, businessUnit.Id);
            Assert.AreEqual(domainUnit.Name, businessUnit.Name);
            Assert.AreEqual(domainUnit.Code, businessUnit.Code);
        }

        [TestMethod]
        public void ConvertFromBusinessModelUnitToDomainUnitTest()
        {
            //Arrange
            var businessUnit = new UnitModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };

            //Action
            var domainUnit = _mappingService.Map<UnitModel, Unit>(businessUnit);

            //Assert
            Assert.AreEqual(businessUnit.Id, domainUnit.Id);
            Assert.AreEqual(businessUnit.Name, domainUnit.Name);
            Assert.AreEqual(businessUnit.Code, domainUnit.Code);
        }

        [TestMethod]
        public void ConvertFromDomainUnitToDataModelUnitTest()
        {
            //Arrange
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "TU");

            //Action
            var dataUnit = _mappingService.Map<Unit, UnitDataModel>(domainUnit);

            //Assert
            Assert.AreEqual(domainUnit.Id, dataUnit.Id);
            Assert.AreEqual(domainUnit.Name, dataUnit.Name);
            Assert.AreEqual(domainUnit.Code, dataUnit.Code);
        }

        [TestMethod]
        public void ConvertFromDataModelUnitToDomainUnitTest()
        {
            //Arrange
            var dataUnit = new UnitDataModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };

            //Action
            var domainUnit = _mappingService.Map<UnitDataModel, Unit>(dataUnit);

            //Assert
            Assert.AreEqual(dataUnit.Id, domainUnit.Id);
            Assert.AreEqual(dataUnit.Name, domainUnit.Name);
            Assert.AreEqual(dataUnit.Code, domainUnit.Code);
        }

        #endregion UNIT MAPPING TEST

        #region ITEM MAPPING TEST

        private Item BuildItemDomainModel()
        {
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "testCode");
            var itemCategory = new ItemCategory(Guid.NewGuid(), "ICT", "Item Category Test");
            var itemGroup = new ItemGroup(Guid.NewGuid(), "IGT", "Item Group Test");
            var units = new Dictionary<Unit, decimal> {{domainUnit, 0}};
            return  new Item(Guid.NewGuid(), "123456", "Test Item",
                units, domainUnit, 10, itemCategory, itemGroup);
        }
        [TestMethod]
        public void ConvertFromDomainItemToBusinessModelItemTest()
        {
            //Arrange
            var domainItem = BuildItemDomainModel();

            //Action
            var businessItem = _mappingService.Map<Item, ItemModel>(domainItem);

            //Assert
            Assert.AreEqual(domainItem.Id, businessItem.Id);
            Assert.AreEqual(domainItem.Name, businessItem.Name);
            Assert.AreEqual(domainItem.Code, businessItem.Code);
            Assert.AreEqual(domainItem.DefaultPrice, businessItem.DefaultPrice);
            Assert.AreEqual(domainItem.DefaultUnit.Code, businessItem.DefaultUnit.Code);
            Assert.AreEqual(domainItem.DefaultUnit.Name, businessItem.DefaultUnit.Name);
        }

        [TestMethod]
        public void ConvertFromBusinessModelItemToDomainItemTest()
        {
            //Arrange
            var businessItem = new ItemModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Item",
                DefaultPrice = 10,
                DefaultUnit = new UnitModel()
                {
                    Code = "Unit Code Test",
                    Id = Guid.NewGuid(),
                    Name = "Unit Name Test"
                }
            };

            //Action
            var domainItem = _mappingService.Map<ItemModel, Item>(businessItem);

            //Assert
            Assert.AreEqual(businessItem.Id, domainItem.Id);
            Assert.AreEqual(businessItem.Name, domainItem.Name);
            Assert.AreEqual(businessItem.Code, domainItem.Code);
            Assert.AreEqual(businessItem.DefaultPrice, domainItem.DefaultPrice);
            Assert.AreEqual(businessItem.DefaultUnit.Code, domainItem.DefaultUnit.Code);
            Assert.AreEqual(businessItem.DefaultUnit.Name, domainItem.DefaultUnit.Name);
            Assert.AreEqual(businessItem.DefaultUnit.Id, domainItem.DefaultUnit.Id);
        }

        [TestMethod]
        public void ConvertFromDomainItemToDataModelItemTest()
        {
            //Arrange
            var domainItem = BuildItemDomainModel();

            //Action
            var dataItem = _mappingService.Map<Item, ItemDataModel>(domainItem);

            //Assert
            Assert.AreEqual(domainItem.Id, dataItem.Id);
            Assert.AreEqual(domainItem.Name, dataItem.Name);
            Assert.AreEqual(domainItem.Code, dataItem.Code);
            Assert.AreEqual(domainItem.DefaultUnit.Code, dataItem.DefaultUnit.Code);
        }

        [TestMethod]
        public void ConvertFromDataModelItemToDomainItemTest()
        {
            //Arrange
            var dataUnit = new UnitDataModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };
            var dataItem = new ItemDataModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Item",
                DefaultUnit = dataUnit,
            };
            dataItem.AvailableUnits = new List<ItemUnitDataModel>
                {new ItemUnitDataModel {Unit = dataUnit, Item = dataItem}};

            //Action
            var domainItem = _mappingService.Map<ItemDataModel, Item>(dataItem);

            //Assert
            Assert.AreEqual(dataItem.Id, domainItem.Id);
            Assert.AreEqual(dataItem.Name, domainItem.Name);
            Assert.AreEqual(dataItem.Code, domainItem.Code);
            Assert.AreEqual(dataItem.DefaultUnit.Code, domainItem.DefaultUnit.Code);
        }

        #endregion ITEM MAPPING TEST

        #region ORDER LINE MAPPING TEST

        private OrderLine BuildOrderLineDomainModel()
        {
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "TU");
            var domainItem = BuildItemDomainModel();
            return new OrderLine(domainItem, new Quantity(domainUnit, 10), 20,null);
        }
        
        [TestMethod]
        public void ConvertFromDomainOrderLineToBusinessModelOrderLine()
        {
            //Arrange
            var domainOrderLine = BuildOrderLineDomainModel();

            //Action
            var businessOrderLine = _mappingService.Map<OrderLine, OrderLineModel>(domainOrderLine);

            //Assert
            Assert.AreEqual(domainOrderLine.LineItem.Id, businessOrderLine.Item.Id);
            Assert.AreEqual(domainOrderLine.Quantity.Value, businessOrderLine.Quantity);
            Assert.AreEqual(domainOrderLine.Quantity.Unit.Id, businessOrderLine.Unit.Id);
            Assert.AreEqual(domainOrderLine.UnitPrice, businessOrderLine.UnitPrice);
        }

        [TestMethod]
        public void ConvertFromBusinessModelNewOrderLineToDomainOrderLineTest()
        {
            //Arrange
            var businessItem = new ItemModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Item"
            };
            var businessUnit = new UnitModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };
            var businessOrderLine = new NewOrderLineModel()
            {
                Item = businessItem,
                Unit = businessUnit,
                Quantity = 10
            };

            //Action
            var domainOrderLine = _mappingService.Map<NewOrderLineModel, OrderLine>(businessOrderLine);

            //Assert
            Assert.AreEqual(businessOrderLine.Item.Id, domainOrderLine.LineItem.Id);
            Assert.AreEqual(businessOrderLine.Quantity, domainOrderLine.Quantity.Value);
            Assert.AreEqual(businessOrderLine.Unit.Id, domainOrderLine.Quantity.Unit.Id);
        }

        [TestMethod]
        public void ConvertFromBusinessModelOrderLineToDomainOrderLineTest()
        {
            //Arrange
            var businessItem = new ItemModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Item"
            };
            var businessUnit = new UnitModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };
            var businessOrderLine = new OrderLineModel()
            {
                Item = businessItem,
                Unit = businessUnit,
                Quantity = 10,
                UnitPrice = 5
            };

            //Action
            var domainOrderLine = _mappingService.Map<NewOrderLineModel, OrderLine>(businessOrderLine);

            //Assert
            Assert.AreEqual(businessOrderLine.Item.Id, domainOrderLine.LineItem.Id);
            Assert.AreEqual(businessOrderLine.Quantity, domainOrderLine.Quantity.Value);
            Assert.AreEqual(businessOrderLine.Unit.Id, domainOrderLine.Quantity.Unit.Id);
        }

        [TestMethod]
        public void ConvertFromDomainOrderLineToDataModelOrderLine()
        {
            //Arrange
            var domainOrderLine = BuildOrderLineDomainModel();

            //Action
            var dataOrderLine = _mappingService.Map<OrderLine, OrderLineDataModel>(domainOrderLine);

            //Assert
            Assert.AreEqual(domainOrderLine.LineItem.Id, dataOrderLine.LineItem.Id);
            Assert.AreEqual(domainOrderLine.Quantity.Value, dataOrderLine.Quantity);
            Assert.AreEqual(domainOrderLine.Quantity.Unit.Id, dataOrderLine.Unit.Id);
            Assert.AreEqual(domainOrderLine.UnitPrice, dataOrderLine.UnitPrice);
        }

        #endregion ORDER LINE MAPPING TEST

        /*
        #region PAYMENT MAPPING TEST

        [TestMethod]
        public void ConvertFromDomainCashPaymentToBusinessModelPaymentTest()
        {
            //Arrange
            var cashPayment =
                new CashPayment(10, new Order("123456879", new Customer(Guid.NewGuid(), "Test Customer")));

            //Action 
            var paymentModel = _mappingService.Map<CashPayment, PaymentModel>(cashPayment);

            //Assert
            Assert.AreEqual(cashPayment.Value, paymentModel.Value);
            Assert.AreEqual((int) PaymentTypes.Cash, paymentModel.Type);
        }

        [TestMethod]
        public void ConvertFromDomainCreditCardPaymentToBusinessModelPaymentTest()
        {
            //Arrange
            var ccPayment = new CreditCardPayment(10,
                new Order("123456879", new Customer(Guid.NewGuid(), "Test Customer")), "151617",
                CreditCardTypes.VisaCard);

            //Action 
            var paymentModel = _mappingService.Map<CreditCardPayment, PaymentModel>(ccPayment);

            //Assert
            Assert.AreEqual(paymentModel.Value, paymentModel.Value);
            Assert.AreEqual((int) PaymentTypes.Cash, paymentModel.Type);
            Assert.AreEqual(paymentModel.AccountId, paymentModel.AccountId);
        }

        [TestMethod]
        public void ConvertFromPaymentBusinessModelToDomainCashPaymentTest()
        {
            //Arrange
            var paymentModel = new PaymentModel()
            {
                Value = 10,
                Type = 1,
                OrderId = Guid.NewGuid()
            };

            //Action 
            var cashPayment = _mappingService.Map<PaymentModel, CashPayment>(paymentModel);

            //Assert
            Assert.AreEqual(paymentModel.Value, cashPayment.Value);
            Assert.AreEqual(paymentModel.OrderId, cashPayment.OrderId);
        }

        [TestMethod]
        public void ConvertFromPaymentBusinessModelToDomainCreditCardPaymentTest()
        {
            //Arrange
            var paymentModel = new PaymentModel()
            {
                Value = 10,
                Type = 3,
                OrderId = Guid.NewGuid(),
                AccountId = Guid.NewGuid().ToString(),
                AccountType = 2
            };

            //Action 
            var creditCardPayment = _mappingService.Map<PaymentModel, CreditCardPayment>(paymentModel);

            //Assert
            Assert.AreEqual(paymentModel.Value, paymentModel.Value);
            Assert.AreEqual((int) PaymentTypes.CreditCard, paymentModel.Type);
            Assert.AreEqual(paymentModel.AccountId, paymentModel.AccountId);
            Assert.AreEqual(CreditCardTypes.VisaCard, (CreditCardTypes) paymentModel.AccountType);
        }

        #endregion PAYMENT MAPPING TEST

        #region STOK MAPPING TEST

        [TestMethod]
        public void ConvertStockDomainEntityToDataModel()
        {
            //Arrange
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "testCode");
            var units = new Dictionary<Unit, decimal>();
            units.Add(domainUnit, 0);
            var item = new Item(Guid.NewGuid(), "123456", "Test Item", units, domainUnit, 10);
            var domainStock = new Stock(item, 100, 30, 70);

            //Action
            var stockDM = _mappingService.Map<Stock, StockDataModel>(domainStock);

            //Assert
            Assert.AreEqual(item.Code, stockDM.Item.Code);
            Assert.AreEqual(item.Id, stockDM.Item.DomainId);
            Assert.AreEqual(100, stockDM.Quantity);
            Assert.AreEqual(30, stockDM.Available);
            Assert.AreEqual(70, stockDM.Reserved);
        }

        [TestMethod]
        public void ConvertStockDataModelToDomainEntity()
        {
            //Arrange
            var unitDM = new UnitDataModel()
            {
                DomainId = Guid.NewGuid(),
                Name = "Test Unit",
                Code = "testCode"
            };

            var itemDM = new ItemDataModel()
            {
                DomainId = Guid.NewGuid(),
                Code = "123456",
                Name = "Test Item",
                DefaultUnit = unitDM,
                DefaultPrice = 10
            };
            var units = new List<ItemUnitDataModel>();
            units.Add(new ItemUnitDataModel() {Unit = unitDM, Item = itemDM});
            itemDM.AvailableUnits = units;
            var stockDM = new StockDataModel()
            {
                Item = itemDM,
                Quantity = 100,
                Available = 30,
                Reserved = 70
            };

            //Action
            var domainStock = _mappingService.Map<StockDataModel, Stock>(stockDM);

            //Assert
            Assert.AreEqual(itemDM.Code, domainStock.Item.Code);
            Assert.AreEqual(itemDM.DomainId, domainStock.Item.Id);
            Assert.AreEqual(100, domainStock.Quantity);
            Assert.AreEqual(30, domainStock.Available);
            Assert.AreEqual(70, domainStock.Reserved);
        }

        [TestMethod]
        public void ConvertStockDomainEntityToModel()
        {
            //Arrange
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "testCode");
            var units = new Dictionary<Unit, decimal>();
            units.Add(domainUnit, 0);
            var item = new Item(Guid.NewGuid(), "123456", "Test Item", units, domainUnit, 10);
            var domainStock = new Stock(item, 100, 30, 70);

            //Action
            var stockDM = _mappingService.Map<Stock, StockModel>(domainStock);

            //Assert
            Assert.AreEqual(item.Code, stockDM.Item.Code);
            Assert.AreEqual(item.Id, stockDM.Item.Id);
            Assert.AreEqual(100, stockDM.Quantity);
            Assert.AreEqual(30, stockDM.Available);
            Assert.AreEqual(70, stockDM.Reserved);
        }

        [TestMethod]
        public void ConvertStockModelToDomainEntity()
        {
            //Arrange
            var unit = new UnitModel()
            {
                Id = Guid.NewGuid(),
                Name = "Test Unit",
                Code = "testCode"
            };

            var item = new ItemModel()
            {
                Id = Guid.NewGuid(),
                Code = "123456",
                Name = "Test Item",
                DefaultUnit = unit,
                DefaultPrice = 10,
                AvailableUnits = new List<UnitModel>() {unit}
            };
            var stock = new StockModel()
            {
                Item = item,
                Quantity = 100,
                Available = 30,
                Reserved = 70
            };

            //Action
            var domainStock = _mappingService.Map<StockModel, Stock>(stock);

            //Assert
            Assert.AreEqual(item.Code, domainStock.Item.Code);
            Assert.AreEqual(item.Id, domainStock.Item.Id);
            Assert.AreEqual(100, domainStock.Quantity);
            Assert.AreEqual(30, domainStock.Available);
            Assert.AreEqual(70, domainStock.Reserved);
        }

        #endregion STOK MAPPING TEST
*/
        /*
        [TestMethod]
        public void ConvertFromDomainPurchasingOrderToBusinessModelPurchasingOrderTest()
        {
            //Arrange
            var domainUnit = new Unit(Guid.NewGuid(), "Test Unit", "TU");
            var units = new Dictionary<Unit, decimal>();
            units.Add(domainUnit, 0);
            var domainOrder = new PurchasingOrder("0111011", new Customer(Guid.NewGuid(), "Test Customer"),
                Guid.NewGuid(), "Test Machine", Guid.NewGuid());
            var domainItem = new Item(Guid.NewGuid(), "123456", "Test Item", units, domainUnit, 10);
            var domainOrderLine = new OrderLine(domainItem, new Quantity(domainUnit, 10), 20);
            domainOrder.AddOrderLine(domainOrderLine);
            //Action 
            var businesOrder = _mappingService.Map<PurchasingOrder, PurchasingOrderModel>(domainOrder);

            //Assert
            Assert.AreEqual(domainOrder.Customer.Id, businesOrder.Customer.Id);
            Assert.AreEqual(domainOrder.Customer.Name, businesOrder.Customer.Name);
            Assert.AreEqual(domainOrder.MachineName, businesOrder.MachineName);
            Assert.AreEqual(domainOrder.SalesmanId, businesOrder.SalesmanId);
            Assert.AreEqual(domainOrder.WarehouseId, businesOrder.WarehouseId);
            Assert.AreEqual(domainOrder.OrderLines.Count, businesOrder.OrderLines.Count);
            Assert.IsTrue(businesOrder.OrderLines.Count == 1);
        }

        [TestMethod]
        public void ConvertFromBusinessModelNewPurchasingOrderTestToDomainPurchasingOrder()
        {
            //Arrange
            var businessOrder = new NewPurchasingOrderModel()
            {
                SalesmanId = Guid.NewGuid(),
                MachineName = "Test Machine",
                WarehouseId = Guid.NewGuid(),
                Customer = new CustomerModel()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test Customer",
                    Addresses = new List<CustomerAddressModel>
                    {
                        new CustomerAddressModel
                        {
                            BlockNo = 10,
                            Country = "Egypt",
                            Landmark = "Land mark Test",
                            City = "Region test",
                            State = "State test",
                            Street = "Street Test"
                        }
                    }
                }
            };

            //Action 
            var domainOrder = _mappingService.Map<NewPurchasingOrderModel, PurchasingOrder>(businessOrder);

            //Assert
            Assert.AreEqual(businessOrder.Customer.Id, domainOrder.Customer.Id);
            Assert.AreEqual(businessOrder.Customer.Name, domainOrder.Customer.Name);
            Assert.AreEqual(businessOrder.MachineName, domainOrder.MachineName);
            Assert.AreEqual(businessOrder.SalesmanId, domainOrder.SalesmanId);
            Assert.AreEqual(businessOrder.WarehouseId, domainOrder.WarehouseId);
        }

        [TestMethod]
        public void ConvertFromBusinessModelPurchasingOrderTestToDomainPurchasingOrder()
        {
            //Arrange
            var businessOrder = new PurchasingOrderModel()
            {
                SalesmanId = Guid.NewGuid(),
                MachineName = "Test Machine",
                WarehouseId = Guid.NewGuid(),
                Customer = new CustomerModel()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test Customer",
                    Addresses = new List<CustomerAddressModel>
                    {
                        new CustomerAddressModel
                        {
                            BlockNo = 10,
                            Country = "Egypt",
                            Landmark = "Land mark Test",
                            City = "Region test",
                            State = "State test",
                            Street = "Street Test"
                        }
                    }
                }
            };
            var businessItem = new ItemModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Item"
            };
            var businessUnit = new UnitModel
            {
                Id = Guid.NewGuid(),
                Code = "1234",
                Name = "Testing Unit"
            };
            businessOrder.OrderLines.Add(new OrderLineModel()
            {
                Item = businessItem,
                Unit = businessUnit,
                Quantity = 10,
                UnitPrice = 5
            });

            //Action 
            var domainOrder = _mappingService.Map<PurchasingOrderModel, PurchasingOrder>(businessOrder);

            //Assert
            Assert.AreEqual(businessOrder.Customer.Id, domainOrder.Customer.Id);
            Assert.AreEqual(businessOrder.Customer.Name, domainOrder.Customer.Name);
            Assert.AreEqual(businessOrder.MachineName, domainOrder.MachineName);
            Assert.AreEqual(domainOrder.SalesmanId, businessOrder.SalesmanId);
            Assert.AreEqual(domainOrder.WarehouseId, businessOrder.WarehouseId);
            Assert.AreEqual(domainOrder.OrderLines.Count, businessOrder.OrderLines.Count);
        }
        */
        
    }
}
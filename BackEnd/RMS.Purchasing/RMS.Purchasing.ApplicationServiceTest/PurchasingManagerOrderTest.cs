﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExceptionHandling.CustomException;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.ApplicationService.Mapping;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using RMS.Purchasing.DomainService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;


namespace ApplicationServiceTest
{
    /*
    [TestClass]
    public class PurchasingManagerOrderTest
    {
        private IPurchasingManager _purchasingManager;
        private Mock<IPurchasingUnitOfWork> _unitOfWork;
        private IMappingService _mappingService;
        private Mock<IPurchasingDomainService> _domainService;

        private Mock<ILogger> _logger;

        private CustomerModel BuildCustomerModel(string name, string phone)
        {
            return new CustomerModel
            {
                Id = Guid.NewGuid(),
                Name = name,
                PhoneNumber = phone,
                Addresses = new List<CustomerAddressModel>
                {
                    new CustomerAddressModel
                    {
                        BlockNo=1,
                        Country="Country",
                        Landmark="Landmark",
                        City="Region",
                        State="State",
                        Street="Street"
                    }
                }
            };
        }

        private PurchasingOrderModel BuildPurchasingOrderModel_WithOrderLines_WithoutPayment()
        {
            return new PurchasingOrderModel
            {
                Id = Guid.NewGuid().ToString(),
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Payments = null,
                OrderLines = new List<OrderLineModel>
                {
                    new OrderLineModel
                    {
                        Item= new ItemModel
                        {
                            Code="00001",
                            Name="Test Item",
                            Id=Guid.NewGuid(),
                            AvailableUnits=new List<UnitModel>
                            {
                                new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                }
                            },
                            DefaultUnit= new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                }
                        },
                        Quantity=1,
                        Unit= new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                },
                        UnitPrice=10,
                        Total=10
                    }
                },
                Serial = Guid.NewGuid().ToString(),
                Total = 10,
                Change = 0,
                Net = 10,
                Paid = 0
            };
        }

        private PurchasingOrderModel BuildPurchasingOrderModel_WithOrderLines_WithPayment()
        {
            return new PurchasingOrderModel
            {
                Id = Guid.NewGuid().ToString(),
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Payments = new List<PaymentModel>
                {
                    new PaymentModel
                    {
                        Type=1,
                        Value=10
                    }
                },
                OrderLines = new List<OrderLineModel>
                {
                    new OrderLineModel
                    {
                        Item= new ItemModel
                        {
                            Code="00001",
                            Name="Test Item",
                            Id=Guid.NewGuid(),
                            AvailableUnits=new List<UnitModel>
                            {
                                new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                }
                            },
                            DefaultUnit= new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                }
                        },
                        Quantity=1,
                        Unit= new UnitModel
                                {
                                    Code="Kg",
                                    Id=Guid.NewGuid(),
                                    Name="Kilogram"
                                },
                        UnitPrice=10,
                        Total=10
                    }
                },
                Serial = Guid.NewGuid().ToString(),
                Total = 10,
                Change = 0,
                Net = 10,
                Paid = 0
            };
        }

        private PurchasingOrderModel BuildPurchasingOrderModel_WithoutOrderLines_WithoutPayment()
        {
            return new PurchasingOrderModel()
            {
                Id = Guid.NewGuid().ToString(),
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Payments = null,
                OrderLines = null,
                Serial = Guid.NewGuid().ToString(),
                Total = 0,
                Change = 0,
                Net = 0,
                Paid = 0
            };
        }

        private OrderDataModel BuildPurchasingOrderDataModel_WithOutOrderLines_WithoutPayments(Guid orderDomainId)
        {
            return new OrderDataModel()
            {
                Change = 0,
                CreateDate = DateTime.Now,
                Customer = new CustomerDataModel()
                {
                    DomainId = Guid.NewGuid(),
                    Name = "Test Customer",
                    PhoneNumber = "123456789",
                    DeliveryAddresses = new List<AddressDataModel>(),
                    Group = new CustomerGroupDataModel()
                    {
                        Code = "A+",
                        DomainId = Guid.NewGuid(),
                        Name = "A Plus Group"
                    }
                },
                DomainId = orderDomainId,
                MachineName = "Test Machine",
                SalesmanId = Guid.NewGuid(),
                Status = 0,
                WarehouseId = Guid.NewGuid(),
                Type = 0,
                OrderLines = new List<OrderLineDataModel>()
            };
        }

        private OrderDataModel BuildPurchasingOrderDataModel_WithOrderLines_WithoutPayments(Guid orderDomainId)
        {
            var tmpItem = new ItemDataModel()
            {
                Code = "0001",
                DefaultPrice = 10,
                DefaultUnit = new UnitDataModel
                {
                    Code = "Ut",
                    DomainId = Guid.NewGuid(),
                    Name = "Unit"
                },
                DomainId = Guid.NewGuid(),
                Name = "Test Item"
            };
            tmpItem.AvailableUnits = new List<ItemUnitDataModel>()
            {
                new ItemUnitDataModel
                {
                    Item=tmpItem,
                    Unit = tmpItem.DefaultUnit
                }
            };
            return new OrderDataModel()
            {
                Change = 0,
                CreateDate = DateTime.Now,
                Customer = new CustomerDataModel()
                {
                    DomainId = Guid.NewGuid(),
                    Name = "Test Customer",
                    PhoneNumber = "123456789",
                    DeliveryAddresses = new List<AddressDataModel>(),
                    Group = new CustomerGroupDataModel()
                    {
                        Code = "A+",
                        DomainId = Guid.NewGuid(),
                        Name = "A Plus Group"
                    }
                },
                DomainId = orderDomainId,
                MachineName = "Test Machine",
                SalesmanId = Guid.NewGuid(),
                Status = 0,
                WarehouseId = Guid.NewGuid(),
                Type = 0,
                OrderLines = new List<OrderLineDataModel>()
                {
                    new OrderLineDataModel
                    {
                        LineItem = tmpItem,
                        Quantity=1,
                        Unit=tmpItem.DefaultUnit,
                        UnitPrice = 10
                    }
                }
            };
        }

        private OrderDataModel BuildPurchasingOrderDataModel_FromOrderDTO(PurchasingOrderModel model)
        {
            var datamodel = new OrderDataModel()
            {
                Customer = new CustomerDataModel()
                {
                    Name = model.Customer.Name,
                    PhoneNumber = model.Customer.PhoneNumber
                },
                MachineName = model.MachineName,
                SalesmanId = model.SalesmanId,
                WarehouseId = model.WarehouseId,
                CreateDate = model.CreateDate,
                Serial = model.Serial,
                Total = model.Total,
                Change = model.Change,
                Net = model.Net,
                Paid = model.Paid
            };

            if (model.OrderLines != null)
            {
                datamodel.OrderLines = new List<OrderLineDataModel>();
                foreach (var line in model.OrderLines)
                {
                    datamodel.OrderLines.Add(new OrderLineDataModel()
                    {
                        LineItem = new ItemDataModel()
                        {
                            Code = line.Item.Code,
                            Name = line.Item.Name,
                            DomainId = line.Item.Id
                        },
                        Quantity = 1,
                        Unit = new UnitDataModel()
                        {
                            Code = line.Unit.Code,
                            DomainId = line.Unit.Id,
                            Name = line.Unit.Name
                        },
                        UnitPrice = line.UnitPrice
                    });
                }
            }
            else
            {
                datamodel.OrderLines = null;
            }

            if (model.Payments != null)
            {
                datamodel.Payments = new List<OrderPaymentDataModel>();
                foreach (var payment in model.Payments)
                {
                    datamodel.Payments.Add(new OrderPaymentDataModel()
                    {
                        Value = payment.Value,
                        AccountId = payment.AccountId,
                        AccountType = payment.AccountType,
                        PaymentType = payment.Type
                    });
                }
            }
            else
            {
                datamodel.Payments = null;
            }

            return datamodel;
        }

        private OrderDataModel BuildPurchasingOrderDataModel_FromOrderDTO(NewPurchasingOrderModel model)
        {
            var datamodel = new OrderDataModel()
            {
                Customer = new CustomerDataModel()
                {
                    Name = model.Customer.Name,
                    PhoneNumber = model.Customer.PhoneNumber
                },
                MachineName = model.MachineName,
                SalesmanId = model.SalesmanId,
                WarehouseId = model.WarehouseId
            };
            return datamodel;
        }

        private void InitializePurchasingManager()
        {
            _purchasingManager = new PurchasingManager(_unitOfWork.Object,
                _mappingService, _domainService.Object, _logger.Object);
        }

        #region MOCK SETUP
        private void SetupDomainService_GeneratOrderSerialMock()
        {
            _domainService = new Mock<IPurchasingDomainService>();
            _domainService.Setup(service => service.GenerateOrderSerial()).Returns("012346678974512103432");
        }

        private void SetupDomainService_GeneratOrderSerial_ThrowExceptionMock()
        {
            _domainService = new Mock<IPurchasingDomainService>();
            _domainService.Setup(service => service.GenerateOrderSerial()).Throws(new BusinessException());
        }

        private decimal SetupDomainService_CalculatePriceMock()
        {
            SetupDomainService_GeneratOrderSerialMock();
            _domainService.SetReturnsDefault<decimal>(10);
            return 10;
        }

        private void SetupQueryOrderRepository_EmptyMock()
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            var orderId = Guid.NewGuid();
            _unitOfWork.Setup(uow => uow.OrderQueryRepository.FindByDomainId(orderId)).Returns(BuildPurchasingOrderDataModel_WithOutOrderLines_WithoutPayments(Guid.NewGuid()));
        }

        private void SetupWriteOrderRepository_EmptyMock()
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
        }

        private void SetupWriteOrderRepository_SaveSuccessfullyMock(NewPurchasingOrderModel order)
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            var dataModel = BuildPurchasingOrderDataModel_FromOrderDTO(order);
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Add(It.IsAny<OrderDataModel>())).Returns<OrderDataModel>(o => { dataModel.Id = 1; return dataModel; });
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Update(It.IsAny<OrderDataModel>())).Returns<OrderDataModel>(o => { return dataModel; });
            _unitOfWork.Setup(uof => uof.SaveChanges());
        }

        private void SetupWriteOrderRepository_SaveWithExceptionMock()
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Add(It.IsAny<OrderDataModel>())).Throws(new RepositoryException());
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Update(It.IsAny<OrderDataModel>())).Throws(new RepositoryException());
        }

        private void SetupWriteOrderRepository_SaveFailMock(NewPurchasingOrderModel order)
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            var dataModel = new OrderDataModel()
            {
                Change = 0,
                CreateDate = DateTime.Now,
                Customer = new CustomerDataModel()
                {
                    Name = order.Customer.Name,
                    PhoneNumber = order.Customer.PhoneNumber,
                    DomainId = order.Customer.Id
                },
                MachineName = order.MachineName,
                Paid = 0,
                SalesmanId = order.SalesmanId,
                WarehouseId = order.WarehouseId
            };
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Add(It.IsAny<OrderDataModel>())).Returns(dataModel);
            _unitOfWork.Setup(uow => uow.OrderWriteRepository.Update(It.IsAny<OrderDataModel>())).Returns(dataModel);
        }

        private void SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(PurchasingOrderModel model)
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();

            _unitOfWork.Setup(uow => uow.OrderQueryRepository.FindByDomainId(new Guid(model.Id))).Returns(BuildPurchasingOrderDataModel_FromOrderDTO(model));
        }

        private void SetupCustomerRepository_FindByDomainIdMock(Guid customerId)
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            _unitOfWork.Setup(u => u.CustomerRepository.FindByDomainId(customerId)).Returns(new CustomerDataModel
            {
                DomainId = Guid.NewGuid(),
                Id = 1,
                Name = "Customer 1",
                PhoneNumber = "01098880614",
                DeliveryAddresses = new List<AddressDataModel>()
                    {
                        new AddressDataModel()
                        {
                            BlockNo=1,
                            Id=1,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        },
                        new AddressDataModel()
                        {
                            BlockNo=566,
                            Id=2,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        }
                    }
            });
        }

        private ItemDataModel SetupItemRepository_GetItemByCodeAndPriceListsMock(string code)
        {
            if (_unitOfWork == null)
                _unitOfWork = new Mock<IPurchasingUnitOfWork>();
            var tmpItem = new ItemDataModel()
            {
                Code = code,
                DefaultPrice = 10,
                DefaultUnit = new UnitDataModel
                {
                    Code = "Ut",
                    DomainId = Guid.NewGuid(),
                    Name = "Unit"
                },
                DomainId = Guid.NewGuid(),
                Name = "Test Item"
            };
            tmpItem.AvailableUnits = new List<ItemUnitDataModel>()
            {
                new ItemUnitDataModel
                {
                    Item=tmpItem,
                    Unit = tmpItem.DefaultUnit
                }
            };
            _unitOfWork.Setup(uow => uow.ItemRepository.GetByCode(code)).Returns(tmpItem);
            _unitOfWork.Setup(uow => uow.ItemRepository.GetPrices(code)).Returns<List<PriceListDataModel>>(null);
            return tmpItem;
        }

        private void SetupLoggerMockup()
        {
            _logger = new Mock<ILogger>();
        }
        #endregion MOCK SETUP

        [TestInitialize]
        public void Intialize()
        {
            _mappingService = new MappingService();
        }

        #region CREATE 

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CreatePurchasingOrder_NullRequest()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CreatePurchasingOrder(null);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CreatePurchasingOrder_NullModel()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CreatePurchasingOrder(null);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CreatePurchasingOrder_FailToGenerateSerial()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerial_ThrowExceptionMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CreatePurchasingOrder(null);
        }

        [TestMethod]
        public void CreatePurchasingOrder_InvalidCustomerData()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel
            {
                Customer = BuildCustomerModel("", ""),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid()
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void CreatePurchasingOrder_InvalidWarehouseData()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Test Customer", "01234567890"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.Empty,
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void CreatePurchasingOrder_InvaliSalesmanData()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Test Customer", "01234567890"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.Empty,
                WarehouseId = Guid.NewGuid()
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void CreatePurchasingOrder_InvalidMachineData()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Test Customer", "01234567890"),
                MachineName = "",
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid()
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CreatePurchasingOrder_RepositoryThrowException()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            SetupWriteOrderRepository_SaveWithExceptionMock();
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.AreEqual(model.Customer.Id, response.Result.Customer.Id);
            Assert.AreEqual(model.MachineName, response.Result.MachineName);
            Assert.AreEqual(model.SalesmanId, response.Result.SalesmanId);
            Assert.AreEqual(model.WarehouseId, response.Result.WarehouseId);
        }

        [TestMethod]
        public void CreatePurchasingOrder_NormalSecnario()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.AreEqual(model.Customer.Id, response.Result.Customer.Id);
            Assert.AreEqual(model.MachineName, response.Result.MachineName);
            Assert.AreEqual(model.SalesmanId, response.Result.SalesmanId);
            Assert.AreEqual(model.WarehouseId, response.Result.WarehouseId);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CreatePurchasingOrder_SavingFaild()
        {
            //Arrange 
            var model = new NewPurchasingOrderModel()
            {
                Customer = BuildCustomerModel("Customer", "01234567891"),
                MachineName = Guid.NewGuid().ToString(),
                SalesmanId = Guid.NewGuid(),
                WarehouseId = Guid.NewGuid(),
            };
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            SetupWriteOrderRepository_SaveFailMock(model);
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<NewPurchasingOrderModel>(model);
            var response = _purchasingManager.CreatePurchasingOrder(request);

            //Assert
            Assert.AreEqual(model.Customer.Id, response.Result.Customer.Id);
        }
        #endregion CREATE

        #region HOLD
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void HoldPurchasingOrder_NullRequest()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.HoldOrder(null);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void HoldPurchasingOrder_NullModel()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.HoldOrder(null);
        }

        [TestMethod]
        public void HoldPurchasingOrder_InvalidCustomerData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.Customer.Name = string.Empty;
            model.Customer.PhoneNumber = string.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.HoldOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void HoldPurchasingOrder_InvalidOrderHeaderData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.MachineName = string.Empty;
            model.SalesmanId = Guid.Empty;
            model.WarehouseId = Guid.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.HoldOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void HoldPurchasingOrder_RepositoryThrowException()
        {
            //Arrange
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();

            SetupLoggerMockup();
            SetupDomainService_GeneratOrderSerialMock();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupWriteOrderRepository_SaveWithExceptionMock();
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<PurchasingOrderModel>(model);
            _purchasingManager.HoldOrder(request);

            //Assert
        }

        [TestMethod]
        public void HoldPurchasingOrder_WithoutOrderLines()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.OrderLines = null;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.HoldOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void HoldPurchasingOrder_NormalScenario()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.HoldOrder(request);

            //Assert
            Assert.IsTrue(response.Result);
        }
        #endregion HOLD

        #region CANCEL
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CancelPurchasingOrder_NullRequest()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CancelOrder(null);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CancelPurchasingOrder_NullModel()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CancelOrder(null);
        }

        [TestMethod]
        public void CancelPurchasingOrder_InvalidCustomerData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.Customer.Name = string.Empty;
            model.Customer.PhoneNumber = string.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CancelOrder(request);

            //Assert
            Assert.IsTrue(response.Result);
        }

        [TestMethod]
        public void CancelPurchasingOrder_InvalidOrderHeaderData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.MachineName = string.Empty;
            model.SalesmanId = Guid.Empty;
            model.WarehouseId = Guid.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CancelOrder(request);

            //Assert
            Assert.IsTrue(response.Result);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void CancelPurchasingOrder_RepositoryThrowException()
        {
            //Arrange
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();

            SetupLoggerMockup();
            SetupDomainService_GeneratOrderSerialMock();
            SetupCustomerRepository_FindByDomainIdMock(model.Customer.Id);
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupWriteOrderRepository_SaveWithExceptionMock();
            InitializePurchasingManager();

            //Action
            var request = new ServiceRequest<PurchasingOrderModel>(model);
            _purchasingManager.CancelOrder(request);

            //Assert
        }

        [TestMethod]
        public void CancelPurchasingOrder_WithoutOrderLines()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.OrderLines = null;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CancelOrder(request);

            //Assert
            Assert.IsTrue(response.Result);
        }

        [TestMethod]
        public void CancelPurchasingOrder_NormalScenario()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CancelOrder(request);

            //Assert
            Assert.IsTrue(response.Result);
        }
        #endregion CANCEL

        #region CLOSE
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ClosePurchasingOrder_NullRequest()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CloseOrder(null);
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ClosePurchasingOrder_NullModel()
        {
            //Arrange 
            SetupDomainService_GeneratOrderSerialMock();
            SetupLoggerMockup();
            SetupWriteOrderRepository_EmptyMock();
            InitializePurchasingManager();

            //Action
            _purchasingManager.CloseOrder(null);
        }

        [TestMethod]
        public void ClosePurchasingOrder_InvalidCustomerData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithPayment();
            model.Customer.Name = string.Empty;
            model.Customer.PhoneNumber = string.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CloseOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void ClosePurchasingOrder_InvalidOrderData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithPayment();
            model.WarehouseId = Guid.Empty;
            model.MachineName = string.Empty;
            model.SalesmanId = Guid.Empty;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CloseOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void ClosePurchasingOrder_InvalidOrderLinesData()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithPayment();
            model.OrderLines = null;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CloseOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void ClosePurchasingOrder_NoPayments()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithoutPayment();
            model.OrderLines = null;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CloseOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }

        [TestMethod]
        public void ClosePurchasingOrder_PaymentNotEqualToNet()
        {
            //Arrange 
            var model = BuildPurchasingOrderModel_WithOrderLines_WithPayment();
            model.Net = 0;

            SetupDomainService_GeneratOrderSerialMock();
            SetupQueryOrderRepository_GetOrderById_EmptyOrderMock(model);
            SetupLoggerMockup();
            SetupWriteOrderRepository_SaveSuccessfullyMock(model);
            InitializePurchasingManager();

            var request = new ServiceRequest<PurchasingOrderModel>(model);

            //Action
            var response = _purchasingManager.CloseOrder(request);

            //Assert
            Assert.IsFalse(response.Result);
            Assert.IsTrue(response.ValidationErrors.Count > 0);
        }
        #endregion CLOSE
    }*/
}

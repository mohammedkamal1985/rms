﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RMS.Purchasing.ApplicationService;
using RMS.Purchasing.ApplicationService.Infrastructure;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using RMS.Purchasing.ApplicationService.Mapping;
using RMS.Purchasing.Repository.Infrastucture;
using RMS.Purchasing.Repository.Infrastucture.DataModel;
using Logging.Infrastructure.Interfaces;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;

namespace ApplicationServiceTest
{
    [TestClass]
    public class CustomerManagerTest
    {
        /*private IMappingService _mappingService;
        private ICustomerManager _customerManager;
        private Mock<IPurchasingUnitOfWork> _mockUoW;
        private Mock<ILogger> _mockLogger;

        private void SetupMockRepositoryFindByNameReturnListCustomer(string name)
        {
            _mockLogger = new Mock<ILogger>();
            _mockUoW = new Mock<IPurchasingUnitOfWork>();
            _mockUoW.Setup(u => u.CustomerRepository.GetByName(name)).Returns(new List<CustomerDataModel>()
            {
                new CustomerDataModel
                {
                    DomainId=Guid.NewGuid(),
                    Id=1,
                    Name="Customer 1",
                    PhoneNumber="01098880614",
                    DeliveryAddresses=new List<AddressDataModel>()
                    {
                        new AddressDataModel()
                        {
                            BlockNo=1,
                            Id=1,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        },
                        new AddressDataModel()
                        {
                            BlockNo=566,
                            Id=2,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        }
                    }
                },
                new CustomerDataModel
                {
                    DomainId=Guid.NewGuid(),
                    Id=2,
                    Name="Customer 2",
                    PhoneNumber="01005660878",
                    DeliveryAddresses=new List<AddressDataModel>()
                    {
                        new AddressDataModel()
                        {
                            BlockNo=9,
                            Id=3,
                            Street="Horyah School St.",
                            Region="Hehia",
                            State="Sharqyah",
                            Country="Egypt"
                        }
                    }
                },
                new CustomerDataModel
                {
                    DomainId=Guid.NewGuid(),
                    Id=3,
                    Name="Customer 3",
                    PhoneNumber="01111331118",
                    DeliveryAddresses=new List<AddressDataModel>()
                    {
                        new AddressDataModel()
                        {
                            BlockNo=12,
                            Id=1,
                            Street="Panorama El-Shrouq Compound",
                            Region="Shrouq City",
                            State="Cairo",
                            Country="Egypt"
                        }
                    }
                }
            });
        }

        private void SetupMockRepositoryFindByNameReturnEmptyList()
        {
            _mockLogger = new Mock<ILogger>();
            _mockUoW = new Mock<IPurchasingUnitOfWork>();
            _mockUoW.Setup(u => u.CustomerRepository.GetByName(string.Empty)).Returns<List<CustomerDataModel>>(null);
        }

        private void SetupMockRepositoryFindByPhoneReturnCustomer(string phone)
        {
            _mockUoW = new Mock<IPurchasingUnitOfWork>();
            _mockLogger = new Mock<ILogger>();
            _mockUoW.Setup(u => u.CustomerRepository.GetByPhoneNumber(phone)).Returns(new CustomerDataModel
            {
                DomainId = Guid.NewGuid(),
                Id = 1,
                Name = "Customer 1",
                PhoneNumber = "01098880614",
                DeliveryAddresses = new List<AddressDataModel>()
                    {
                        new AddressDataModel()
                        {
                            BlockNo=1,
                            Id=1,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        },
                        new AddressDataModel()
                        {
                            BlockNo=566,
                            Id=2,
                            Street="Plastine St.",
                            Region="Maadi",
                            State="Cairo",
                            Country="Egypt"
                        }
                    }
            });
        }

        private void SetupMockRepositoryFindByPhoneReturnNull()
        {
            _mockLogger = new Mock<ILogger>();
            _mockUoW = new Mock<IPurchasingUnitOfWork>();
            _mockUoW.Setup(u => u.CustomerRepository.GetByPhoneNumber(string.Empty)).Returns<CustomerDataModel>(null);
        }

        private void InitializeCustomerService()
        {
            _customerManager = new CustomerManager(_mockUoW.Object, _mappingService, _mockLogger.Object);
        }

        [TestInitialize]
        public void Initialize()
        {
            _mappingService = new MappingService();
        }

        [TestMethod]
        public void GetCustomersByNameNormalScenario()
        {
            //Arrange 
            var customerName = "customer";
            SetupMockRepositoryFindByNameReturnListCustomer(customerName);
            InitializeCustomerService();
            var request = new ServiceRequest<string>(customerName);

            //Action 
            var response = _customerManager.GetByName(request);

            Assert.AreEqual(3, response.Result.Count);
            Assert.IsInstanceOfType(response.Result, typeof(List<CustomerModel>));
            foreach (var c in response.Result)
            {
                Assert.IsTrue(c.Name.ToLower().Contains(customerName));
            }
        }

        [TestMethod]
        public void GetCustomersByNameEmptyNameString()
        {
            //Arrange 
            SetupMockRepositoryFindByNameReturnEmptyList();
            InitializeCustomerService();
            var request = new ServiceRequest<string>(string.Empty);

            //Action 
            var response = _customerManager.GetByName(request);

            Assert.IsNull(response.Result);
        }

        [TestMethod]
        public void GetCustomerByPhoneNumberNormalSecnario()
        {
            //Arrange
            var phone = "01098880614";
            SetupMockRepositoryFindByPhoneReturnCustomer(phone);
            InitializeCustomerService();
            var request = new ServiceRequest<string>(phone);
            
            //Action 
            var response= _customerManager.GetByPhoneNumber(request);

            //Assert
            Assert.IsInstanceOfType(response.Result, typeof(CustomerModel));
            Assert.AreEqual(response.Result.PhoneNumber, phone);
        }

        [TestMethod]
        public void GetCustomerByPhoneNumberEmptyPhoneNumber()
        {
            //Arrange
            var phone = "";
            SetupMockRepositoryFindByPhoneReturnNull();
            InitializeCustomerService();
            var request = new ServiceRequest<string>(phone );

            //Action 
            var response = _customerManager.GetByPhoneNumber(request);

            //Assert
            Assert.IsNull(response.Result);
        }

        [TestMethod]
        public void GetCustomerByPhoneNumberNoResultFound()
        {
            //Arrange
            var phone = "0123456789";
            SetupMockRepositoryFindByPhoneReturnNull();
            InitializeCustomerService();
            var request = new ServiceRequest<string>(phone);

            //Action 
            var response = _customerManager.GetByPhoneNumber(request);

            //Assert
            Assert.IsNull(response.Result);
        }*/
    }
}

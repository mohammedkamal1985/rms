﻿using System;
using System.Collections.Generic;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.ApplicationService.Infrastructure
{
    public interface ICustomerManager
    {
        ServiceReadResponse<List<CustomerModel>> GetByName(ServiceRequest<string> request);
        ServiceReadResponse<CustomerModel> GetByPhoneNumber(ServiceRequest<string> request);
        ServiceReadResponse<CustomerModel> GetDefaultCustomer();
    }
}

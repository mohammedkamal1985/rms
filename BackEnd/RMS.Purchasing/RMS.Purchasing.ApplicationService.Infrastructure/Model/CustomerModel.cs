﻿using System;
using System.Collections.Generic;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class CustomerModel
    {
        public CustomerModel()
        {
            Addresses = new List<CustomerAddressModel>();
        }
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string PhoneNumber { get; set; }
        public List<CustomerAddressModel> Addresses { get; set; }
        
        public bool IsDefault { get; set; }
    }
}

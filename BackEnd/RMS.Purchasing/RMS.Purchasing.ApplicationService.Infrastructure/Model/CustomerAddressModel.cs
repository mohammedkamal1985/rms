﻿namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class CustomerAddressModel
    {
        public int BlockNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Landmark { get; set; }
        
        public string PostCode { get; set; }
    }
}

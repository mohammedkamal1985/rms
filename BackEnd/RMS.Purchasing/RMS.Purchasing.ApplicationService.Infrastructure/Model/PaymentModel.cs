﻿using System;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class PaymentModel
    {
        public Guid OrderId { get; set; }
        public decimal Value { get; set; }
        public string AccountId { get; set; }
        public int AccountType { get; set; }
        public int Type { get; set; }
    }
}

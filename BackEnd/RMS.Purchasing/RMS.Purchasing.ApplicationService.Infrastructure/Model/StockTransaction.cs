﻿using System;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class StockTransactionModel
    {
        public ItemModel Item { get; set; }
        public decimal Quantity { get; set; }
        public UnitModel Unit { get; set; }
        
        public DateTime TimeStamp { get; set; }
        public int Type { get; set; }
        
        public Guid LocationId { get; set; }
    }
}

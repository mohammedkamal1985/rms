﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class UnitModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

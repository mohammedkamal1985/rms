﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class DeliveryOrderModel : PurchasingOrderModel
    {
        public DeliveryOrderModel()
        {
            OrderLines = new List<OrderLineModel>();
        }
        public CustomerAddressModel DeliveryAddress { get; set; }
        
        public DateTime DeliveryDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class NewOrderLineModel
    {
        public Guid OrderId { get; set; }
        public ItemModel Item { get; set; }
        public decimal Quantity { get; set; }
        public UnitModel Unit { get; set; }
    }
}

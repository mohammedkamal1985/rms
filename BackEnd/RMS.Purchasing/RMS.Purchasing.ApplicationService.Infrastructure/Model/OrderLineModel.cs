﻿namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class OrderLineModel:NewOrderLineModel
    {
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class PurchasingOrderModel : NewPurchasingOrderModel
    {
        public string Id { get; set; }
        public PurchasingOrderModel()
        {
            OrderLines = new List<OrderLineModel>();
            Payments = new List<PaymentModel>();
            Type = 0;
        }
        public List<OrderLineModel> OrderLines { get; set; }
        public List<PaymentModel> Payments { get; set; }
        public DateTime CreateDate { get;set;}
        public decimal Total { get; set; }
        public decimal Paid { get; set; }
        public decimal Net { get; set; }
        public decimal Change { get; set; }
        public string Notes { get; set; }
        public string Serial { get; set; }
        public int Type { get; set; }

    }
}

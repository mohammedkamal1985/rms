﻿namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class StockModel
    {
        public ItemModel Item { get; set; }
        public decimal Available { get; set; }
        public decimal Reserved { get; set; }
        public decimal Quantity { get; set; }
    }
}

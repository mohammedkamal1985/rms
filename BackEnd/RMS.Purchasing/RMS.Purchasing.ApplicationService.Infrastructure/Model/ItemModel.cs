﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class ItemModel
    {
        public ItemModel()
        {
            AvailableUnits = new List<UnitModel>();
        }
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public UnitModel DefaultUnit { get; set; }
        public List<UnitModel> AvailableUnits { get; set; }
        public decimal DefaultPrice { get; set; }
    }
}

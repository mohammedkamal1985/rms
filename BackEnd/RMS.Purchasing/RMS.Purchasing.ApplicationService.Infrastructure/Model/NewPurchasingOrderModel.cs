﻿using System;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Model
{
    public class NewPurchasingOrderModel
    {
        public Guid SalesmanId { get; set; }
        public string MachineName { get; set; }
        public Guid WarehouseId { get; set; }
        public CustomerModel Customer { get; set; }
        
        
    }
}

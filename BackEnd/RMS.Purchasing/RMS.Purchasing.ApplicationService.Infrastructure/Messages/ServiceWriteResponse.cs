﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RMS.Purchasing.ApplicationService.Infrastructure.Messages
{
    public class ServiceWriteResponse<T> 
    {
        public ServiceWriteResponse()
        {
            ValidationErrors = new List<string>();
        }
        public T Result { get; set; }
        public List<string> ValidationErrors { get; set; }
    }
}

﻿namespace RMS.Purchasing.ApplicationService.Infrastructure.Messages
{
    public class ServiceReadResponse<T>
    {
        public ServiceReadResponse(T result)
        {
            Result = result;
        }

        public T Result { get; }
    }
}

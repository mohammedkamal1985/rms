﻿namespace RMS.Purchasing.ApplicationService.Infrastructure.Messages
{
    public class ServiceRequest<T> 
    {
        public ServiceRequest(T data)
        {
            Data = data;
        }

        public T Data { get; }
    }
}

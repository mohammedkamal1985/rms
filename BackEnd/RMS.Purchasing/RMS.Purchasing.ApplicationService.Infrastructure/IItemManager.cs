﻿using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;
using System.Collections.Generic;

namespace RMS.Purchasing.ApplicationService.Infrastructure
{
    public interface IItemManager
    {
        ServiceReadResponse<List<ItemModel>> GetByName(ServiceRequest<string> itemName);
        ServiceReadResponse<ItemModel> GetByCode(ServiceRequest<string> code);
    }
}

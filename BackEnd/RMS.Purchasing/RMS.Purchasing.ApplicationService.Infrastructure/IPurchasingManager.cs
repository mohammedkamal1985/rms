﻿using System;
using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.ApplicationService.Infrastructure
{
    public interface IPurchasingManager
    {
        ServiceWriteResponse<PurchasingOrderModel> CreatePurchasingOrder(ServiceRequest<NewPurchasingOrderModel> newOrder);
        ServiceReadResponse<string> GetNewOrderSerial(); 
        
        //ServiceWriteResponse<DeliveryOrderModel> CreateDeliveryOrder(ServiceRequest<NewPurchasingOrderModel> newOrder);
        ServiceWriteResponse<PurchasingOrderModel> AddOrderLine(ServiceRequest<OrderLineModel> orderLine);
        
        //PurchasingOrderModel UpdateOrderLine(Guid orderId, OrderLineModel orderLine);
        ServiceWriteResponse<bool> CancelOrder(ServiceRequest<PurchasingOrderModel> order);
        ServiceWriteResponse<bool> HoldOrder(ServiceRequest<PurchasingOrderModel> order);
        ServiceWriteResponse<bool> CloseOrder(ServiceRequest<PurchasingOrderModel> order);
    }
}
﻿namespace RMS.Purchasing.ApplicationService.Infrastructure
{
    public interface IMappingService
    {
        Destination Map<Source, Destination>(Source sourceObject);
    }
}

﻿using RMS.Purchasing.ApplicationService.Infrastructure.Messages;
using RMS.Purchasing.ApplicationService.Infrastructure.Model;

namespace RMS.Purchasing.ApplicationService.Infrastructure
{
    public interface IStockManager
    {
        ServiceWriteResponse<bool> ReserveStock(ServiceRequest<StockTransactionModel> request);
        ServiceWriteResponse<bool> ReserveStock(ServiceRequest<string> request);
        ServiceReadResponse<StockModel> GetStockByItemCode(ServiceRequest<string> request);
    }
}
